package com.aait.alphacareprovider.Uitls;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionUtils {

    public static boolean isPermissionGranted(Activity activity, String permissionName, int requestCode) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(permissionName)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Tag", "Permission is granted");
                return true;
            } else {
                Log.v("tag", "Permission is revoked");
                ActivityCompat.requestPermissions(activity,
                        new String[]{permissionName}, requestCode);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("tag", "Permission is granted");
            return true;
        }
    }

    public static final String[] IMAGE_PERMISSIONS =
            {Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static final String[] DOWNLOAD_PERMISSIONS =
            {Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static final String[] GPS_PERMISSION =
            {Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION};

    public static final String[] CALL_PHONE =
            {Manifest.permission.CALL_PHONE};

    public static final int IMAGE_PERMISSION_RESPONSE = 1;

    public static final int DOWNLOAD_PERMISSION_RESPONSE = 2;

    /**
     * @return whether all the required permission for taking and picking an image are granted or not
     */
    public static boolean areImagePermissionsGranted(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        for (String permission : IMAGE_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllPermissionGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    Log.e("SSSs", permission.toString());
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean canMakeSmores(int BuildVersion) {
        return (Build.VERSION.SDK_INT > BuildVersion);

      // Build.VERSION_CODES.LOLLIPOP_MR1;
    }
}
