package com.aait.alphacareprovider.UI.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.aait.alphacareprovider.App.Constant;
import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Gps.GPSTracker;
import com.aait.alphacareprovider.Gps.GpsTrakerListener;
import com.aait.alphacareprovider.Models.AddressesModel;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.OnClick;

public class AddressLocationActivity extends ParentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GpsTrakerListener {
    @BindView(R.id.map)
    MapView mapView;



    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;

//    public String mLang, mLat;
//    String id;

    private AlertDialog mAlertDialog;

    boolean startTracker = false;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView act_title;
    AddressesModel addressesModel;

    @Override
    protected void initializeComponents() {

      addressesModel = (AddressesModel) getIntent().getSerializableExtra("data");
        act_title.setText(addressesModel.getName());
        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_address_location;
    }



    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + addressesModel.getLat() + "  Lng:" + addressesModel.getLng());
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        putMapMarker(Double.parseDouble(addressesModel.getLat()),Double.parseDouble(addressesModel.getLng()));
    }
    public void getLocationWithPermission() {
        gps = new GPSTracker(this, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(AddressLocationActivity.this, PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(AddressLocationActivity.this,
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());

                googleMap.clear();
                putMapMarker(Double.parseDouble(addressesModel.getLat()), Double.parseDouble(addressesModel.getLng()));
            }
        }
    }
    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.marker_red));

        myMarker = googleMap.addMarker(marker);
    }

}
