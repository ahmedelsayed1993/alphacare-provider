package com.aait.alphacareprovider.UI.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareprovider.Base.BaseFragment;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Listeners.PaginationScrollListener;
import com.aait.alphacareprovider.Models.OrderModel;
import com.aait.alphacareprovider.Models.OrdersResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.CurrentAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCurrentOrders extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<OrderModel> orderModels = new ArrayList<>();
    CurrentAdapter ordersAdapter;
    String newToken = null;
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    public static FragmentCurrentOrders newInstance(String newToken) {
        Bundle args = new Bundle();
        FragmentCurrentOrders fragment = new FragmentCurrentOrders();
        args.putString("token",newToken);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.app_recycle;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        ordersAdapter = new CurrentAdapter(mContext,orderModels,R.layout.recycler_order);
        ordersAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(ordersAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                        getQuestions(newToken);


            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                orderModels.clear();
                isLastPage = false;
                isLoading = false;

                        getQuestions(newToken);

            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        orderModels.clear();
        isLastPage = false;
        isLoading = false;

                getQuestions(newToken);

    }

    private void getQuestions(String token){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getCurrent(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id(),token,currentPage).enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Log.e("status",new Gson().toJson(response.body().getData().getData()));
                        TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getData().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_orders);
                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getData().isEmpty()) {
                                ordersAdapter.updateAll( response.body().getData().getData());
                            } else {
                                ordersAdapter.InsertAll( response.body().getData().getData());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getData().isEmpty()) {
                                ordersAdapter.updateAll( response.body().getData().getData());
                                isLastPage = true;
                            } else {
                                ordersAdapter.InsertAll( response.body().getData().getData());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
