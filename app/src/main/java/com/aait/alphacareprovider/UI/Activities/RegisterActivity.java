package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.ListModel;
import com.aait.alphacareprovider.Models.ListResponse;
import com.aait.alphacareprovider.Models.Singltone;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;

import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Views.ListDialog;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.aait.alphacareprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class RegisterActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks, OnItemClickListener {
    String ImageBasePath = null;
    String ImageBasePath1 = null;

    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;
    @BindView(R.id.all)
    RadioButton all;
    @BindView(R.id.city)
    TextView city;
    ListDialog listDialog;
    int gender = 1;
    String newToken= null;
    Singltone singltone;
    ListModel cities ;
    ArrayList<ListModel> listModels = new ArrayList<>();
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");

    @Override
    protected void initializeComponents() {
        singltone = Singltone.getInstance();
        SetData();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( RegisterActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });

    }
    void SetData(){
        List<View> views = new ArrayList<>();
        views.add(profile_image);
        views.add(name);
        views.add(phone);
        views.add(email);
        views.add(city);
        ImageBasePath = singltone.registerModel.getAvatar();
        Glide.with(mContext).load(singltone.registerModel.getAvatar()).apply(new RequestOptions().error(R.mipmap.upload
        )).into(profile_image);
        name.setText(singltone.registerModel.getName());
        phone.setText(singltone.registerModel.getPhone());
        email.setText(singltone.registerModel.getEmail());
        gender = singltone.registerModel.getGender();
        if (gender==1){
            male.setChecked(true);
        }else if(gender == 2) {
            female.setChecked(true);
        }else if (gender == 3){
            all.setChecked(true);
        }
        cities = new ListModel(singltone.registerModel.getCity_id(),singltone.registerModel.getCity_name());
        city.setText(singltone.registerModel.getCity_name());
        if (singltone.registerModel.getAvatar().equals("")){
            profile_image.requestFocus();
        }else if (singltone.registerModel.getName().equals("")){
            name.requestFocus();
        }else if (singltone.registerModel.getPhone().equals("")){
            phone.requestFocus();
        }else if (singltone.registerModel.getEmail().equals("")){
            email.requestFocus();
        }else if (singltone.registerModel.getCity_id().equals("")){
            city.requestFocus();
        }

    }
    @OnClick(R.id.city)
    void onCity(){
        getCities();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @OnClick(R.id.male)
    void onMale(){
        gender = 1;
    }
    @OnClick(R.id.female)
    void onFemale(){
        gender = 2;
    }
    @OnClick(R.id.all)
    void onAll(){
        gender = 3;
    }
    @OnClick(R.id.login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
    }
    @OnClick(R.id.profile_image)
    void onProfile(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }

@OnClick(R.id.register)
void onRegister(){
Log.e("ava",singltone.registerModel.getAvatar());
        if (!singltone.registerModel.getAvatar().equals("")){

                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(city,getString(R.string.city))){
                    return;
                }else {
                    singltone.registerModel.setAvatar(ImageBasePath);
                    singltone.registerModel.setName(name.getText().toString());
                    singltone.registerModel.setPhone(phone.getText().toString());
                    singltone.registerModel.setEmail(email.getText().toString());
                    singltone.registerModel.setCity_id(cities.getId());
                    singltone.registerModel.setCity_name(cities.getTitle());
                    singltone.registerModel.setGender(gender);
                    finish();
                    startActivity(new Intent(mContext,CompleteRegisterOneActivity.class));
                }

        }else {
            CommonUtil.makeToast(mContext,getString(R.string.profile_image));
        }

}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                 returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath = returnValue.get(0);
//                MultipartBody.Part filePart = null;
//                File ImageFile = new File(ImageBasePath);
//                ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
//                filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
                singltone.registerModel.setAvatar(ImageBasePath);
                Glide.with(mContext).load(singltone.registerModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.upload)).into(profile_image);
                //profile_image.setImageURI(Uri.parse(ImageBasePath));
            }
        }
    }



    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        listModels = response.body().getData();
                        listDialog = new ListDialog(mContext,RegisterActivity.this,listModels,getString(R.string.city));
                        listDialog.show();

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        if (view.getId()==R.id.tv_row_title){
            cities=listModels.get(position);
            city.setText(cities.getTitle());
            singltone.registerModel.setCity_name(cities.getTitle());
            singltone.registerModel.setCity_id(cities.getId());
        }

    }
}
