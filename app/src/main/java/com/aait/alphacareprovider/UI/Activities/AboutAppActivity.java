package com.aait.alphacareprovider.UI.Activities;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.SiteInfoResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutAppActivity extends ParentActivity {
    @BindView(R.id.terms)

    WebView terms;


    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;

    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.about_alph));
        WebSettings webSettings = terms.getSettings();
        webSettings.setJavaScriptEnabled(true);
        getInfo();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_terms;
    }
    private void getInfo(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getInfo(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<SiteInfoResponse>() {
            @Override
            public void onResponse(Call<SiteInfoResponse> call, Response<SiteInfoResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        terms.loadData(response.body().getData().getSite_about(), "text/html", "utf-8");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SiteInfoResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


}
