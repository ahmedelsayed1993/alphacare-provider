package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Models.AddressesModel;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.CommentsModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogReportComment extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    String token;
    CommentsModel addressesModel;
    @BindView(R.id.reason)
    EditText reason;
    private ProgressDialog mProgressDialog;
    public DialogReportComment(@NonNull Context context, String token, CommentsModel addressesModel) {
        super(context);
        this.mContext = context;
        this.token = token;
        this.addressesModel = addressesModel;

    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_report_comment);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {


    }
    private void report(){
        mProgressDialog = DialogUtil.showProgressDialog(mContext, mContext.getString(R.string.please_wait), false);
        RetroWeb.getClient().create(ServiceApi.class).report(languagePrefManager.getAppLanguage(),1,sharedPreferences.getUserData().getToken_id(),token,addressesModel.getId(),reason.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        DialogReportComment.this.dismiss();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        DialogReportComment.this.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                mProgressDialog.dismiss();
                DialogReportComment.this.dismiss();

            }
        });
    }
    @OnClick(R.id.cancel)
    void onCancel(){
        DialogReportComment.this.dismiss();
    }
    @OnClick(R.id.save)
    void onSave(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,reason,mContext.getResources().getString(R.string.report_reason))){
            return;
        }else {
            report();
        }
    }

}
