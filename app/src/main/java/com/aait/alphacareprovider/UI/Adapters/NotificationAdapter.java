package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.NotificationModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;

import com.aait.alphacareprovider.UI.Activities.CurrentOrderActivity;
import com.aait.alphacareprovider.UI.Activities.FinishedOrderActivity;
import com.aait.alphacareprovider.UI.Activities.OrderDetailsActivity;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationAdapter extends ParentRecyclerAdapter<NotificationModel> {
    String lang;
    String token;
    public NotificationAdapter(Context context, List<NotificationModel> data, int layoutId, String lang, String token) {
        super(context, data, layoutId);
        this.lang = lang;
        this.token = token;
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        NotificationAdapter.ViewHolder holder = new NotificationAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final NotificationAdapter.ViewHolder viewHolder = (NotificationAdapter.ViewHolder) holder;
        final NotificationModel addressesModel = data.get(position);
        viewHolder.message.setText(addressesModel.getMessage());
        if (!addressesModel.getRoom_id().equals("")&&!addressesModel.getOrder_id().equals("")){
            viewHolder.contact.setVisibility(View.VISIBLE);
        }else {
            viewHolder.contact.setVisibility(View.GONE);
        }
        viewHolder.contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
//                Intent intent =new Intent(mcontext, OrderDetailsActivity.class);
//                intent.putExtra("id",addressesModel.getOrder_id()+"");
//                mcontext.startActivity(intent);
            }
        });
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, position);
                PopupMenu popupMenu = new PopupMenu(mcontext, viewHolder.delete);

                popupMenu.inflate(R.menu.notification);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.delete:
                                RetroWeb.getClient().create(ServiceApi.class).remove(0, lang, mSharedPrefManager.getUserData().getToken_id(), token, addressesModel.getId()).enqueue(new Callback<BaseResponse>() {
                                    @Override
                                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                        if (response.isSuccessful()) {
                                            if (response.body().getKey().equals("1")) {
                                                CommonUtil.makeToast(mcontext,response.body().getMsg());
                                                notifyItemChanged(position);
                                            } else {
                                                CommonUtil.makeToast(mcontext, response.body().getMsg());
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                                        CommonUtil.handleException(mcontext, t);
                                        t.printStackTrace();


                                    }
                                });
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.show();

            }

        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!addressesModel.getOrder_id().equals("")){
                    if (addressesModel.getOrder_status().equals("0")){
                        Intent intent =new Intent(mcontext, OrderDetailsActivity.class);
                        intent.putExtra("id",addressesModel.getOrder_id()+"");
                        mcontext.startActivity(intent);
                    }else if (addressesModel.getOrder_status().equals("1")){
                        Intent intent =new Intent(mcontext, CurrentOrderActivity.class);
                        intent.putExtra("id",addressesModel.getOrder_id()+"");
                        mcontext.startActivity(intent);
                    }else if (addressesModel.getOrder_status().equals("3")){
                        Intent intent =new Intent(mcontext, FinishedOrderActivity.class);
                        intent.putExtra("id",addressesModel.getOrder_id()+"");
                        mcontext.startActivity(intent);
                    }else if (addressesModel.getOrder_status().equals("2")){
                        CommonUtil.makeToast(mcontext,mcontext.getString(R.string.order_refused));
                    }
                }
            }
        });


    }

    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.contact)
        Button contact;
        @BindView(R.id.delete)
                ImageView delete;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
