package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.UserModel;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivateAccountActivity extends ParentActivity {
    @BindView(R.id.code)
    EditText code;
    @OnClick(R.id.resend)
    void onResend(){
        resend();
    }
    @OnClick(R.id.confirm)
    void onConfirm(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,code,getString(R.string.activation_code))){
            return;
        }else {
            activate();
        }
    }
    UserModel userModel;
    @Override
    protected void initializeComponents() {
        userModel = (UserModel) getIntent().getSerializableExtra("data");
        Log.e("user",new Gson().toJson(userModel));

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_activate;
    }


    private void activate(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).activate(mLanguagePrefManager.getAppLanguage(),userModel.getId(),code.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());

                        startActivity(new Intent(mContext,LoginActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void resend(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).resend(mLanguagePrefManager.getAppLanguage(),userModel.getId()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.body().getKey().equals("1")){
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
