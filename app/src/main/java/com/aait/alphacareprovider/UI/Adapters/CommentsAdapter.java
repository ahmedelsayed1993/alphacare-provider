package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.CommentsModel;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Views.DialogReportComment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fotoapparat.parameter.Flash;

public class CommentsAdapter extends ParentRecyclerAdapter<CommentsModel> {
    String token;
    public CommentsAdapter(Context context, List<CommentsModel> data, int layoutId,String token) {
        super(context, data, layoutId);
        this.token = token;
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        CommentsAdapter.ViewHolder holder = new CommentsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final CommentsAdapter.ViewHolder viewHolder = (CommentsAdapter.ViewHolder) holder;
        final CommentsModel addressesModel = data.get(position);
        viewHolder.name.setText(addressesModel.getUser_name());
        viewHolder.comment.setText(addressesModel.getComment());
        Glide.with(mcontext).load(addressesModel.getUser_image()).apply(new RequestOptions().placeholder(mcontext.getResources().getDrawable(R.mipmap.logo))).into(viewHolder.image);
        viewHolder.rating.setRating(Float.parseFloat(addressesModel.getRate()));
        viewHolder.date.setText(addressesModel.getDate());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
                PopupMenu popupMenu = new PopupMenu(mcontext,viewHolder.more);
                popupMenu.inflate(R.menu.comment);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.report:
                                new DialogReportComment(mcontext,token,addressesModel).show();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.comment)
        TextView comment;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.rating)
        RatingBar rating;
        @BindView(R.id.more)
        ImageView more;
        @BindView(R.id.date)
                TextView date;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
