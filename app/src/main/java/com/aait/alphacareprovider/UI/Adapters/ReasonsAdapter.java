package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.ReasonsModel;
import com.aait.alphacareprovider.Models.SiteReportsModel;
import com.aait.alphacareprovider.R;

import java.util.List;

import butterknife.BindView;

public class ReasonsAdapter extends ParentRecyclerAdapter<ReasonsModel> {
    int selectedPosition = 0;
    public ReasonsAdapter(Context context, List<ReasonsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ReasonsAdapter.ViewHolder holder = new ReasonsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ReasonsAdapter.ViewHolder viewHolder = (ReasonsAdapter.ViewHolder) holder;
        final ReasonsModel addressesModel = data.get(position);
        viewHolder.reason.setText(addressesModel.getTitle());
        viewHolder.reason.setChecked(selectedPosition == position);
        viewHolder.reason.setTag(position);
        viewHolder.reason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, position);
                selectedPosition =(Integer) viewHolder.reason.getTag();
                notifyDataSetChanged();
            }
        });

    }
    public class ViewHolder extends ParentRecyclerViewHolder {
        @BindView(R.id.reason)
        RadioButton reason;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
