package com.aait.alphacareprovider.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.viewpager.widget.ViewPager;

import com.aait.alphacareprovider.Base.BaseFragment;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.ProviderServicesResponse;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.MainActivity;
import com.aait.alphacareprovider.UI.Adapters.OrdersTapAdapter;
import com.aait.alphacareprovider.UI.Adapters.SubscribeTapAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment {
    @BindView(R.id.title)
    TextView title;
    private String newToken= null;
    @BindView(R.id.no_services)
    LinearLayout no_services;
    @BindView(R.id.order_lay)
    LinearLayout order_lay;
    @BindView(R.id.orders)
    TabLayout myOrdersTab;

    @BindView(R.id.ordersViewPager)
    ViewPager myOrdersViewPager;
    @BindView(R.id.online)
    SwitchCompat online;
    private OrdersTapAdapter mAdapter;
    public static HomeFragment newInstance(String newToken) {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        args.putString("token",newToken);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initializeComponents(View view) {
        online.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.home));
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        mAdapter = new OrdersTapAdapter(mContext,getChildFragmentManager(),newToken);
        myOrdersViewPager.setAdapter(mAdapter);
        myOrdersTab.setupWithViewPager(myOrdersViewPager);
        getProvider(newToken);
        if(mSharedPrefManager.getUserData().getIs_online().equals("1")){
            online.setChecked(true);
//            online.setActivated(true);
        }else {
            online.setChecked(false);
//            online.setActivated(false);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(mSharedPrefManager.getUserData().getIs_online().equals("1")){
            online.setChecked(true);
//            online.setActivated(true);
        }else {
            online.setChecked(false);
//            online.setActivated(false);
        }
    }

    @OnClick(R.id.add_services)
    void onAddServices(){
        getActivity().finish();
        Intent intent = new Intent(mContext,MainActivity.class);
        intent.putExtra("type","profile");
        startActivity(intent);


    }
    private void getProvider(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProvider(1,mSharedPrefManager.getUserData().getToken_id(),token,null,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ProviderServicesResponse>() {
            @Override
            public void onResponse(Call<ProviderServicesResponse> call, Response<ProviderServicesResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if(response.body().getData().getTab_data().isEmpty()){
                            no_services.setVisibility(View.VISIBLE);
                            order_lay.setVisibility(View.GONE);
                        }else {
                            no_services.setVisibility(View.GONE);
                            order_lay.setVisibility(View.VISIBLE);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderServicesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.online)
    void onLine(){
        if (mSharedPrefManager.getUserData().getIs_online().equals("1")){
            active("0");
        }else {
            active("1");
        }
    }
    private void active(String status){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).active(1,mSharedPrefManager.getUserData().getToken_id(),newToken,status,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        mSharedPrefManager.setUserData(response.body().getData());
                        Log.e("user",mSharedPrefManager.getUserData().getIs_online());


                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
