package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.ListModel;
import com.aait.alphacareprovider.Models.ListModle;
import com.aait.alphacareprovider.R;

import java.util.List;

import butterknife.BindView;

public class TypesAdapter extends ParentRecyclerAdapter<ListModle> {
    public TypesAdapter(Context context, List<ListModle> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        TypesAdapter.ViewHolder holder = new TypesAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final TypesAdapter.ViewHolder viewHolder = (TypesAdapter.ViewHolder) holder;
        final ListModle categoryModel = data.get(position);
        viewHolder.name.setText(categoryModel.getName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
