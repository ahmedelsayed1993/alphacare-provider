package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.SiteReportsModel;
import com.aait.alphacareprovider.R;

import java.util.List;

import butterknife.BindView;

public class ReportsAdapter extends ParentRecyclerAdapter<SiteReportsModel> {
    public ReportsAdapter(Context context, List<SiteReportsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ReportsAdapter.ViewHolder holder = new ReportsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ReportsAdapter.ViewHolder viewHolder = (ReportsAdapter.ViewHolder) holder;
        final SiteReportsModel addressesModel = data.get(position);
        viewHolder.report.setText(addressesModel.getTitle());
        viewHolder.report.setChecked(true);
    }
    protected class ViewHolder extends ParentRecyclerViewHolder {
        @BindView(R.id.report)
        CheckBox report;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
