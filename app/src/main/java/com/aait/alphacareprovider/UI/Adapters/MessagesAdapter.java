package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.OrderModel;
import com.aait.alphacareprovider.Models.RoomModel;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.ChatActivity;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessagesAdapter extends ParentRecyclerAdapter<RoomModel> {
    String token;
    public MessagesAdapter(Context context, List<RoomModel> data, int layoutId,String token) {
        super(context, data, layoutId);
        this.token = token;
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        MessagesAdapter.ViewHolder holder = new MessagesAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final MessagesAdapter.ViewHolder viewHolder = (MessagesAdapter.ViewHolder) holder;
        final RoomModel addressesModel = data.get(position);
        viewHolder.name.setText(addressesModel.getOther_name());
        viewHolder.message.setText(addressesModel.getLast_message_body());
        Typeface face = ResourcesCompat.getFont(mcontext, R.font.cairo_regular);

        Typeface face1 = ResourcesCompat.getFont(mcontext, R.font.cairo_bold);

        Glide.with(mcontext).load(addressesModel.getOther_image()).into(viewHolder.image);
        if(addressesModel.getCount_unseen()==0){
            viewHolder.count.setVisibility(View.GONE);
            viewHolder.message.setTypeface(face);
            viewHolder.name.setTypeface(face);
            viewHolder.image.setBorderColor(mcontext.getResources().getColor(R.color.colorGray));
        }else {
            viewHolder.count.setVisibility(View.VISIBLE);
            viewHolder.count.setText(addressesModel.getCount_unseen()+"");
            viewHolder.message.setTypeface(face1);
            viewHolder.name.setTypeface(face1);
            viewHolder.image.setBorderColor(mcontext.getResources().getColor(R.color.colorAccent));
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
                Intent intent = new Intent(mcontext, ChatActivity.class);
                intent.putExtra("room",addressesModel.getRoom_id()+"");
                intent.putExtra("token",token);
                intent.putExtra("name",addressesModel.getOther_name());
                mcontext.startActivity(intent);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.count)
        TextView count;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
