package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.App.Constant;
import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.ListModel;
import com.aait.alphacareprovider.Models.ListResponse;
import com.aait.alphacareprovider.Models.Singltone;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Views.ListDialog;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.gson.Gson;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.listener.OnErrorListener;
import gun0912.tedimagepicker.builder.listener.OnMultiSelectedListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class CompleteRegisterOneActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.technical)
    RadioButton technical;
    @BindView(R.id.Specialized_center)
    RadioButton specialized_center;
    @BindView(R.id.technical_lay)
    LinearLayout technical_lay;
    @BindView(R.id.specialized_lay)
    LinearLayout specialized_lay;
    @BindView(R.id.service)
    TextView service;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.address_tag)
    TextView address_tag;
    @BindView(R.id.id_number)
    EditText id_number;
    @BindView(R.id.id_image)
    ImageView id_image;
    @BindView(R.id.professional)
    ImageView professional;
    @BindView(R.id.specialized)
    ImageView specialized;
    @BindView(R.id.Identification_number_owner)
    EditText Identification_number_owner;
    @BindView(R.id.id_image_)
    ImageView id_image_;
    @BindView(R.id.facility)
    ImageView facility;
    @BindView(R.id.documents)
    ImageView documents;
    Singltone singltone;
    int count = 1;
    ListModel listModel ;
    ListDialog listDialog;
    ArrayList<ListModel> listModels = new ArrayList<>();
    private int FILE_REQUEST_CODE = 666;
    ArrayList<MediaFile> files = new ArrayList<>();
    String path1 = null ,path2 = null,path3 = null , path4 = null , path5 = null , path6 = null;
    @OnClick(R.id.one)
    void onOne(){
        startActivity(new Intent(mContext,RegisterActivity.class));
    }
    String mAdresse="", mLang=null, mLat = null, mAddress="",Address = "";
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)
            .setCount(1)
            .setFrontfacing(false)
            .setImageQuality(ImageQuality.HIGH)
            .setPreSelectedUrls(returnValue)
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
            .setPath("/pix/images");
    ArrayList<String> returnValue1 = new ArrayList<>();
    Options options1 = Options.init()
            .setRequestCode(200)
            .setCount(1)
            .setFrontfacing(false)
            .setImageQuality(ImageQuality.HIGH)
            .setPreSelectedUrls(returnValue1)
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
            .setPath("/pix/images");
    ArrayList<String> returnValue2 = new ArrayList<>();
    Options options2 = Options.init()
            .setRequestCode(300)
            .setCount(1)
            .setFrontfacing(false)
            .setImageQuality(ImageQuality.HIGH)
            .setPreSelectedUrls(returnValue2)
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
            .setPath("/pix/images");
    ArrayList<String> returnValue3 = new ArrayList<>();
    Options options3 = Options.init()
            .setRequestCode(400)
            .setCount(1)
            .setFrontfacing(false)
            .setImageQuality(ImageQuality.HIGH)
            .setPreSelectedUrls(returnValue3)
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
            .setPath("/pix/images");
    ArrayList<String> returnValue4 = new ArrayList<>();
    Options options4 = Options.init()
            .setRequestCode(550)
            .setCount(1)
            .setFrontfacing(false)
            .setImageQuality(ImageQuality.HIGH)
            .setPreSelectedUrls(returnValue4)
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
            .setPath("/pix/images");

    @Override
    protected void initializeComponents() {
        singltone =  Singltone.getInstance();
        Log.e("model",new Gson().toJson(singltone.registerModel));
         setData();
//        if (count==1){
//            technical.setChecked(true);
//            technical_lay.setVisibility(View.VISIBLE);
//            specialized_lay.setVisibility(View.GONE);
//            address.setVisibility(View.VISIBLE);
//            address_tag.setVisibility(View.VISIBLE);
//        }else {
//            specialized_center.setChecked(true);
//            specialized_lay.setVisibility(View.VISIBLE);
//            technical_lay.setVisibility(View.GONE);
//            address.setVisibility(View.VISIBLE);
//            address_tag.setVisibility(View.VISIBLE);
//        }

    }
    void setData(){
        List<View> views = new ArrayList<>();
        views.add(service);
        views.add(address);
        views.add(id_image);
        views.add(id_image_);
        views.add(professional);
        views.add(id_number);
        views.add(documents);
        views.add(facility);
        views.add(specialized);
        views.add(Identification_number_owner);
        listModel = new ListModel(singltone.registerModel.getSection_id(),singltone.registerModel.getSection_name());
        service.setText(singltone.registerModel.getSection_name());
        if (mLanguagePrefManager.equals("ar")){
            address.setText(singltone.registerModel.getLatlng_address_ar());
        }else {
            address.setText(singltone.registerModel.getLatlng_address_en());
        }
        mLat = singltone.registerModel.getLat();
        mLang = singltone.registerModel.getLng();
        mAddress = singltone.registerModel.getLatlng_address_ar();
        mAdresse = singltone.registerModel.getLatlng_address_en();
        path6 = singltone.registerModel.getPapers_pdf();
        path5 = singltone.registerModel.getLisence_copy();
        path4 = singltone.registerModel.getNational_copy();
        path3 = singltone.registerModel.getCard_copy();
        path2 = singltone.registerModel.getPractice_copy();
        path1 = singltone.registerModel.getNational_copy();
        if (singltone.registerModel.isTechnical()||singltone.registerModel.getProvider_type()==1){
            technical.setChecked(true);
            technical_lay.setVisibility(View.VISIBLE);
            specialized_lay.setVisibility(View.GONE);
            address.setVisibility(View.VISIBLE);
            address_tag.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(singltone.registerModel.getNational_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(id_image);
            id_number.setText(singltone.registerModel.getNational_id());
            Glide.with(mContext).load(singltone.registerModel.getPractice_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(professional);
            Glide.with(mContext).load(singltone.registerModel.getCard_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(specialized);
        }else if (!(singltone.registerModel.isTechnical())||singltone.registerModel.getProvider_type()==2){
            specialized_center.setChecked(true);
            technical_lay.setVisibility(View.GONE);
            specialized_lay.setVisibility(View.VISIBLE);
            address.setVisibility(View.VISIBLE);
            address_tag.setVisibility(View.VISIBLE);
            Identification_number_owner.setText(singltone.registerModel.getNational_id());
            Glide.with(mContext).load(singltone.registerModel.getNational_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(id_image_);
            Glide.with(mContext).load(singltone.registerModel.getLisence_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(facility);
            if (singltone.registerModel.getPapers_pdf().equals("")){
                Glide.with(mContext).load(singltone.registerModel.getPapers_pdf()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(documents);
            }else {
                Glide.with(mContext).load(singltone.registerModel.getPapers_pdf()).apply(new RequestOptions().error(R.mipmap.doone)).into(documents);
            }
        }

        if (singltone.registerModel.isTechnical()){
            if (singltone.registerModel.getSection_id().equals("")){
                service.requestFocus();
            }else if (singltone.registerModel.getLatlng_address_ar().equals("")){
                address.requestFocus();
            }else if (singltone.registerModel.getNational_id().equals("")){
                id_number.requestFocus();
            }else if (singltone.registerModel.getNational_copy().equals("")){
                id_image.requestFocus();
            }else if (singltone.registerModel.getPractice_copy().equals("")){
                professional.requestFocus();
            }else if (singltone.registerModel.getCard_copy().equals("")){
                specialized.requestFocus();
            }
        }else {
            if (singltone.registerModel.getSection_id().equals("")){
                service.requestFocus();
            }else if (singltone.registerModel.getLatlng_address_ar().equals("")){
                address.requestFocus();
            }else if (singltone.registerModel.getNational_id().equals("")){
                Identification_number_owner.requestFocus();
            }else if (singltone.registerModel.getNational_copy().equals("")){
                id_image_.requestFocus();
            }else if (singltone.registerModel.getLisence_copy().equals("")){
                facility.requestFocus();
            }else if (singltone.registerModel.getPapers_pdf().equals("")){
                documents.requestFocus();
            }

        }
    }
    @OnClick(R.id.technical)
    void onTechnical(){
        count = 1;
        singltone.registerModel.setProvider_type(1);
        singltone.registerModel.setTechnical(true);
        singltone.registerModel.setTechnical(true);
        technical_lay.setVisibility(View.VISIBLE);
        specialized_lay.setVisibility(View.GONE);
        address.setVisibility(View.VISIBLE);
        address_tag.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.Specialized_center)
    void onSpecialized(){
        count = 2;
        singltone.registerModel.setProvider_type(2);
        singltone.registerModel.setTechnical(false);
        singltone.registerModel.setTechnical(false);
        specialized_lay.setVisibility(View.VISIBLE);
        technical_lay.setVisibility(View.GONE);
        address.setVisibility(View.VISIBLE);
        address_tag.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.service)
    void onService(){
        getSections();
    }
    @OnClick(R.id.id_image)
    void onImage(){

            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                    CommonUtil.PrintLogE("Permission not granted");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                                REQUEST_IMAGES);
                    }
                } else {
                    Pix.start(this, options);
                    CommonUtil.PrintLogE("Permission is granted before");
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23");
                Pix.start(this, options);
            }

    }
    @OnClick(R.id.professional)
    void onProfessional(){

            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                    CommonUtil.PrintLogE("Permission not granted");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                                REQUEST_IMAGES);
                    }
                } else {
                    Pix.start(this, options1);
                    CommonUtil.PrintLogE("Permission is granted before");
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23");
                Pix.start(this, options1);
            }

    }
    @OnClick(R.id.specialized)
    void onSpecializ(){

        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options2);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options2);
        }

    }
    @OnClick(R.id.id_image_)
    void onIDImage(){

        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options3);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options3);
        }

    }
    @OnClick(R.id.facility)
    void onFacility(){

        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options4);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options4);
        }

    }
    @OnClick(R.id.documents)
    void add(){
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(
                FilePickerActivity.CONFIGS,  new Configurations.Builder()
                        .setShowFiles(true)
                        .setShowAudios(false)
                        .setShowVideos(false)
                        .setShowImages(false)
                        .setSuffixes("pdf")
                        .setSingleChoiceMode(true)
                        .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);

//        Intent intent = new Intent();
//        intent.setType("application/pdf");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select PDF"), 5);
    }
    @OnClick(R.id.address)
    void onAddress(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register_complete_one;
    }
    @OnClick(R.id.register)
    void onRegister(){
        if (CommonUtil.checkTextError(service,getString(R.string.services))||CommonUtil.checkTextError(address,getString(R.string.address))){
            return;
        }else {
            if (singltone.registerModel.getProvider_type()==1){
              if (
              CommonUtil.checkTextError((AppCompatActivity)mContext,id_number,getString(R.string.Identification_Number))||
              CommonUtil.cheID(id_number,getString(R.string.id_number_validation))){
                  return;
              }else {
                  if (path1!=null){
                      if (path2!=null){
                          if (path3!=null){
                              singltone.registerModel.setSection_name(listModel.getTitle());
                              singltone.registerModel.setSection_id(listModel.getId());
                              singltone.registerModel.setLat(mLat);
                              singltone.registerModel.setLng(mLang);
                              singltone.registerModel.setLatlng_address_ar(mAddress);
                              singltone.registerModel.setLatlng_address_en(mAdresse);
                              singltone.registerModel.setNational_id(id_number.getText().toString());
                              singltone.registerModel.setNational_copy(path1);
                              singltone.registerModel.setPractice_copy(path2);
                              singltone.registerModel.setCard_copy(path3);
                              singltone.registerModel.setTechnical(true);
                              singltone.registerModel.setProvider_type(1);
                              Log.e("user",new Gson().toJson(singltone));
                              finish();
                              startActivity(new Intent(mContext,CompleteRegisterTwoActivity.class));
                          }else {
                              CommonUtil.makeToast(mContext,getString(R.string.Specialization_Card_Authority));
                          }
                      }else {
                          CommonUtil.makeToast(mContext,getString(R.string.Professional_license));
                      }
                  }else {
                      CommonUtil.makeToast(mContext,getString(R.string.ID_photo));
                  }
              }
            }else if(singltone.registerModel.getProvider_type() == 2) {
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,Identification_number_owner,getString(R.string.Identification_number_owner))||
                CommonUtil.cheID(Identification_number_owner,getString(R.string.id_number_validation))){
                    return;
                }else  {

                        if (!singltone.registerModel.getLisence_copy().equals("")){
                            if (!singltone.registerModel.getPapers_pdf().equals("")){
                                singltone.registerModel.setSection_name(listModel.getTitle());
                                singltone.registerModel.setSection_id(listModel.getId());
                                singltone.registerModel.setLat(mLat);
                                singltone.registerModel.setLng(mLang);
                                singltone.registerModel.setLatlng_address_ar(mAddress);
                                singltone.registerModel.setLatlng_address_en(mAdresse);
                                singltone.registerModel.setNational_id(Identification_number_owner.getText().toString());
                                singltone.registerModel.setNational_copy(path4);
                                singltone.registerModel.setLisence_copy(path5);
                                singltone.registerModel.setPapers_pdf(path6);
                                singltone.registerModel.setTechnical(false);
                                singltone.registerModel.setProvider_type(2);
                                Log.e("user",new Gson().toJson(singltone));
                                finish();
                                startActivity(new Intent(mContext,CompleteRegisterTwoActivity.class));
                            }else {
                                CommonUtil.makeToast(mContext,getString(R.string.The_documents_necessary_for_practicing_specialization));
                            }
                        }else {
                            CommonUtil.makeToast(mContext,getString(R.string.The_work_permit_for_the_facility));
                        }

                }
            }
        }

    }
    private void getSections(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getSections(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        listModels = response.body().getData();
                        listDialog = new ListDialog(mContext,CompleteRegisterOneActivity.this,listModels,getString(R.string.services));
                        listDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        if (view.getId()==R.id.tv_row_title){
            listModel = listModels.get(position);
            singltone.registerModel.setSection_id(listModel.getId());
            singltone.registerModel.setSection_name(listModel.getTitle());
            service.setText(singltone.registerModel.getSection_name());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                Log.e("file",files.get(0).getName());
                path6 = files.get(0).getPath();


                singltone.registerModel.setPapers_pdf(files.get(0).getPath());
                Glide.with(mContext).load(path6).apply(new RequestOptions().error(R.mipmap.doone)).into(documents);
                Log.e("path", path6);
            }
        }
        else if (requestCode == Constant.RequestCode.GET_LOCATION) {
            if (resultCode == RESULT_OK) {
                mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                mAddress = data.getStringExtra("LOCATION");
                Address = data.getStringExtra("LOC");
                mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse + "  " + mAddress + "  " + address);
                if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                    address.setText(mAddress);
                } else if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                    address.setText(mAdresse);
                }
            }

        }
        else if (requestCode == 100){
            if (resultCode == RESULT_OK) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                path1 = returnValue.get(0);
                singltone.registerModel.setNational_copy(path1);
                Glide.with(mContext).load(singltone.registerModel.getNational_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(id_image);
            }
        }
        else if (requestCode == 200){
            if (resultCode == RESULT_OK) {
                returnValue1 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                path2 = returnValue1.get(0);
                singltone.registerModel.setPractice_copy(path2);
                Glide.with(mContext).load(singltone.registerModel.getPractice_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(professional);
            }
        }
        else if (requestCode == 300){
            if (resultCode == RESULT_OK) {
                returnValue2 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                path3 = returnValue2.get(0);
                singltone.registerModel.setCard_copy(path3);
                Glide.with(mContext).load(singltone.registerModel.getCard_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(specialized);
            }
        }
        else if (requestCode == 400){
            if (resultCode == RESULT_OK) {
                returnValue3 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                path4 = returnValue3.get(0);
                singltone.registerModel.setNational_copy(path4);
                Glide.with(mContext).load(singltone.registerModel.getNational_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(id_image_);
            }
        }
        else if (requestCode == 550){
            if (resultCode == RESULT_OK) {
                returnValue4 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                path5 = returnValue4.get(0);
                singltone.registerModel.setLisence_copy(path5);
                Glide.with(mContext).load(singltone.registerModel.getLisence_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(facility);
            }
        }
    }
    @OnClick(R.id.login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
    }
//    File file = new File(path);
//    final RequestBody requestBody = RequestBody.create(MediaType.parse("file/*"),file);
//    MultipartBody.Part filepart = MultipartBody.Part.createFormData("file",file.getName(),requestBody);
}
