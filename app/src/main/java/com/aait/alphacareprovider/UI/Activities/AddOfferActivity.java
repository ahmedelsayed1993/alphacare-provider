package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.aait.alphacareprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class AddOfferActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks{
    @BindView(R.id.name_ar)
    EditText name_ar;
    @BindView(R.id.name_en)
    EditText name_en;
    @BindView(R.id.price)
    EditText price;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.desc_ar)
    EditText desc_ar;
    @BindView(R.id.desc_en)
    EditText desc_en;
    @BindView(R.id.adv_ar)
    EditText adv_ar;
    @BindView(R.id.adv_en)
    EditText adv_en;
    String ImageBasePath = null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    String newToken = null;
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( AddOfferActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_offer;
    }
    @OnClick(R.id.image)
    void onProfile(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }
    @OnClick(R.id.add)
    void onAdd(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,name_ar,getString(R.string.service_name_ar))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,name_en,getString(R.string.service_name_en))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,price,getString(R.string.price))){
            return;
        }else {
            if (ImageBasePath==null){
                CommonUtil.makeToast(mContext,getString(R.string.add_image));
            }else {
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,desc_ar,getString(R.string.service_desc_ar))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,desc_en,getString(R.string.service_desc_en))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,adv_ar,getString(R.string.Features_ar))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,adv_en,getString(R.string.Features_en))){
                    return;
                }else {
                    addService(ImageBasePath);
                }
            }
        }
    }
    @OnClick(R.id.cancel)
    void onCancel(){
        onBackPressed();
    }
    private void addService(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, AddOfferActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).addService(1,mSharedPrefManager.getUserData().getToken_id(),newToken
                ,name_ar.getText().toString(),name_en.getText().toString(),price.getText().toString(),filePart,desc_ar.getText().toString(),
                desc_en.getText().toString(),mLanguagePrefManager.getAppLanguage(),1,adv_ar.getText().toString(),adv_en.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,MainActivity.class);
                        intent.putExtra("type","profile");
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }

            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath = returnValue.get(0);
//                MultipartBody.Part filePart = null;
//                File ImageFile = new File(ImageBasePath);
//                ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
//                filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);

                Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload)).into(image);
                //profile_image.setImageURI(Uri.parse(ImageBasePath));
            }
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
