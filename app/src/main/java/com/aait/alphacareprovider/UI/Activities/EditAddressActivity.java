package com.aait.alphacareprovider.UI.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.aait.alphacareprovider.App.Constant;
import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Gps.GPSTracker;
import com.aait.alphacareprovider.Gps.GpsTrakerListener;
import com.aait.alphacareprovider.Models.AddressesResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAddressActivity extends ParentActivity
        implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GpsTrakerListener {
    @BindView(R.id.map)
    MapView mapView;



    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;

    public String mLang, mLat;

    private AlertDialog mAlertDialog;

    boolean startTracker = false;

    String mResult;
    Geocoder geocoder;
    String mResult1;
    Geocoder geocoder1;
    String mResult2;
    Geocoder geocoder2;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    String name;
    String token;
    String id;


    @Override
    protected void initializeComponents() {
        name = getIntent().getStringExtra("name");
        id = getIntent().getStringExtra("id");
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( EditAddressActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();

                Log.e("newToken",token);

            }
        });
        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_map;
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMapClick(LatLng latLng) {
        Log.e("LatLng", latLng.toString());
        mLang = Double.toString(latLng.latitude);
        mLat = Double.toString(latLng.longitude);
        if (myMarker != null) {
            myMarker.remove();
            putMapMarker(latLng.latitude, latLng.longitude);
        } else {
            putMapMarker(latLng.latitude, latLng.longitude);
        }

        if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
            putMapMarker(latLng.latitude, latLng.longitude);
            mLat = String.valueOf(latLng.latitude);
            mLang = String.valueOf(latLng.longitude);
            List<Address> addresses;
            List<Address> addresses1;
            List<Address> addresses2;
            geocoder = new Geocoder(EditAddressActivity.this, Locale.ENGLISH);
            geocoder1 = new Geocoder(EditAddressActivity.this, new Locale("ar"));
            geocoder2 = new Geocoder(EditAddressActivity.this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                addresses1 = geocoder1.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                addresses2 = geocoder2.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                if (addresses.isEmpty()){
                    Toast.makeText(EditAddressActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                }
                else{mResult = addresses.get(0).getAddressLine(0);
                    // CommonUtil.makeToast(mContext,mResult);
                }
                if (addresses1.isEmpty()){
                    Toast.makeText(EditAddressActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                }
                else{mResult1 = addresses1.get(0).getAddressLine(0);
                    // CommonUtil.makeToast(mContext,mResult1);
                }
                if (addresses2.isEmpty()){
                    Toast.makeText(EditAddressActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                }
                else{mResult2 = addresses.get(0).getAddressLine(0);
                    //CommonUtil.makeToast(mContext,mResult2);
                }

            } catch (IOException e) {}
            googleMap.clear();
            putMapMarker(latLng.latitude, latLng.longitude);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        getLocationWithPermission();
    }
    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.marker_red));
        marker.title("موقعي");
        myMarker = googleMap.addMarker(marker);
    }
    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 800: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    Log.e("Permission", "All permission are granted");
                    getCurrentLocation();
                    for (int i = 0; i < grantResults.length; i++) {
                        Log.e("Permission", grantResults[0] + "");
                    }
                } else {
                    Log.e("Permission", "permission arn't granted");
                }
                return;
            }
        }
    }
    @OnClick(R.id.btn_finish_location)
    void onDetectedSuccess() {
        Log.e("Location", "Lat:" + mLat + " Lng:" + mLang +"address"+mResult+"  "+mResult1+"  "+mResult2);
        if (mLang != null && mLat != null ) {
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).editAddrress(mLanguagePrefManager.getAppLanguage(), 1, mSharedPrefManager.getUserData().getToken_id()
                    , token, name, mLat, mLang,Integer.parseInt(id),mResult1, mResult).enqueue(new Callback<AddressesResponse>() {
                @Override
                public void onResponse(Call<AddressesResponse> call, Response<AddressesResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                            startActivity(new Intent(mContext,AddressesActivity.class));
                            EditAddressActivity.this.finish();
                        } else {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddressesResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }
    }

    public void getLocationWithPermission() {
        gps = new GPSTracker(this, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(EditAddressActivity.this, PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(EditAddressActivity.this,
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
                List<Address> addresses;
                List<Address> addresses1;
                List<Address> addresses2;
                geocoder = new Geocoder(EditAddressActivity.this, Locale.ENGLISH);
                geocoder1 = new Geocoder(EditAddressActivity.this, new Locale("ar"));
                geocoder2 = new Geocoder(EditAddressActivity.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    addresses1 = geocoder1.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    addresses2 = geocoder2.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()) {
                        Toast.makeText(EditAddressActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult = addresses.get(0).getAddressLine(0);
                        //CommonUtil.makeToast(mContext, mResult);
                    }
                    if (addresses1.isEmpty()) {
                        Toast.makeText(EditAddressActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult1 = addresses1.get(0).getAddressLine(0);
                        //CommonUtil.makeToast(mContext, mResult1);
                    }
                    if (addresses2.isEmpty()) {
                        Toast.makeText(EditAddressActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult2 = addresses.get(0).getAddressLine(0);
                        // CommonUtil.makeToast(mContext, mResult2);
                    }

                } catch (IOException e) {
                }
                googleMap.clear();
                putMapMarker(Double.parseDouble(mLat), Double.parseDouble(mLang));
            }
        }
    }

}
