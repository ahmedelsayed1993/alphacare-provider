package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.PaginationScrollListener;
import com.aait.alphacareprovider.Models.CommissionModel;
import com.aait.alphacareprovider.Models.CommissionResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.CommissionAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletActivity extends ParentActivity {
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    String newToken = null;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.commission)
    TextView commission;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    CommissionAdapter commissionAdapter;
    ArrayList<CommissionModel> commissionModels = new ArrayList<>();
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.wallet));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        commissionAdapter = new CommissionAdapter(mContext,commissionModels,R.layout.recycler_commissions);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(commissionAdapter);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( WalletActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;

                        getCommission(newToken);


                    }

                    @Override
                    public int getTotalPageCount() {
                        return 0;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });

                swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
                swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        currentPage = 1;
                        commissionModels.clear();
                        isLastPage = false;
                        isLoading = false;

                        getCommission(newToken);

                    }
                });
                Log.e("newToken",newToken);

            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( WalletActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                currentPage = 1;
                commissionModels.clear();
                isLastPage = false;
                isLoading = false;

                getCommission(newToken);
            }
        });
    }
@OnClick(R.id.settlement)
void onSettlement(){
        startActivity(new Intent(mContext,SettlementActivity.class));
}

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_wallet;
    }
    @OnClick(R.id.shipping)
    void onShipping(){
        startActivity(new Intent(mContext,ShippingActivity.class));
    }
    private void getCommission(String token){
        showProgressDialog(getString(R.string.please_wait));
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getCommission(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id(),token,currentPage).enqueue(new Callback<CommissionResponse>() {
            @Override
            public void onResponse(Call<CommissionResponse> call, Response<CommissionResponse> response) {
                hideProgressDialog();
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        balance.setText(response.body().getData().getUser_balance()+" "+getString(R.string.sar));
                        commission.setText(getString(R.string.Discount_rate)+" "+response.body().getData().getSite_commission()+getString(R.string.On_every_completed_request));
                        TOTAL_PAGES = response.body().getData().getUser_commissions().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getUser_commissions().getData().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getUser_commissions().getData().isEmpty()) {
                                commissionAdapter.updateAll( response.body().getData().getUser_commissions().getData());
                            } else {
                                commissionAdapter.InsertAll( response.body().getData().getUser_commissions().getData());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getUser_commissions().getData().isEmpty()) {
                                commissionAdapter.updateAll( response.body().getData().getUser_commissions().getData());
                                isLastPage = true;
                            } else {
                                commissionAdapter.InsertAll( response.body().getData().getUser_commissions().getData());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommissionResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);


            }
        });
    }
}
