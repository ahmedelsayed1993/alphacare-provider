package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Models.UserModel;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;

import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.LoginActivity;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPasswordDailog extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    UserModel userModel;
    String phone;
    public NewPasswordDailog(@NonNull Context context, UserModel userModel, String phone) {
        super(context);
        this.mContext = context;
        this.userModel = userModel;
        this.phone = phone;
    }
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.new_password)
    EditText new_password;
    @BindView(R.id.confirm_new_password)
    EditText confirm_new_password;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_new_password);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {

    }
    @OnClick(R.id.done)
    void onDone(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,code,mContext.getString(R.string.enter_code))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,new_password,mContext.getString(R.string.new_password))||
        CommonUtil.checkLength(new_password,mContext.getString(R.string.password_length),6)||
        CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new_password,mContext.getString(R.string.confirm_new_password))){
            return;
        }else {
            if (new_password.getText().toString().equals(confirm_new_password.getText().toString())){
                 newPass();
            }else {
                CommonUtil.makeToast(mContext,mContext.getString(R.string.password_not_match));
            }
        }
    }
    private void newPass(){
        RetroWeb.getClient().create(ServiceApi.class).newPass(languagePrefManager.getAppLanguage(),code.getText().toString(),new_password.getText().toString(),phone).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        NewPasswordDailog.this.cancel();
                        mContext.startActivity(new Intent(mContext, LoginActivity.class));

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }
}
