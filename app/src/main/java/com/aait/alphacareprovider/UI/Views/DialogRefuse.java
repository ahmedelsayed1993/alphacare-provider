package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.OrderDetailsModdel;
import com.aait.alphacareprovider.Models.ReasonsModel;
import com.aait.alphacareprovider.Models.ReasonsResponse;
import com.aait.alphacareprovider.Models.ServicesModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.MainActivity;
import com.aait.alphacareprovider.UI.Activities.OrderDetailsActivity;
import com.aait.alphacareprovider.UI.Adapters.ReasonsAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogRefuse extends Dialog implements OnItemClickListener {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    String token;
    OrderDetailsModdel addressesModel;
    @BindView(R.id.order)
    TextView address;
    @BindView(R.id.reasons)
    RecyclerView reasons;
    ArrayList<ReasonsModel> reasonsModels = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    ReasonsAdapter reasonsAdapter;
    private ProgressDialog mProgressDialog;
    String  id="";
    @BindView(R.id.another_reason)
    EditText another_reason;
    public DialogRefuse(@NonNull Context context, String token, OrderDetailsModdel addressesModel) {
        super(context);
        this.mContext = context;
        this.token =token;
        this.addressesModel = addressesModel;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_refuse);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        reasonsAdapter = new ReasonsAdapter(mContext,reasonsModels,R.layout.recycler_reason);
        reasonsAdapter.setOnItemClickListener(this);
        reasons.setLayoutManager(linearLayoutManager);
        reasons.setAdapter(reasonsAdapter);
        getReassons();

        address.setText(mContext.getString(R.string.refuse_request)+addressesModel.getOrder_id()+"#");



    }
    private void getReassons(){
        mProgressDialog = DialogUtil.showProgressDialog(mContext, mContext.getString(R.string.please_wait), false);
        RetroWeb.getClient().create(ServiceApi.class).getReasons(languagePrefManager.getAppLanguage()).enqueue(new Callback<ReasonsResponse>() {
            @Override
            public void onResponse(Call<ReasonsResponse> call, Response<ReasonsResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){

                        }else {
                            id = response.body().getData().get(0).getId()+"";
                            reasonsAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ReasonsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                mProgressDialog.dismiss();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

        id = reasonsModels.get(position).getId()+"";
        Log.e("id",id);

    }
    private void answerOrder(int status,String reason){
        mProgressDialog = DialogUtil.showProgressDialog(mContext, mContext.getString(R.string.please_wait), false);
        RetroWeb.getClient().create(ServiceApi.class).answerOrder(languagePrefManager.getAppLanguage(),1,sharedPreferences.getUserData().getToken_id(),token,addressesModel.getOrder_id()+"",status,reason).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext, MainActivity.class);
                        intent.putExtra("type","normal");
                        mContext.startActivity(intent);
                        DialogRefuse.this.dismiss();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                mProgressDialog.dismiss();

            }
        });
    }
    @OnClick(R.id.add)
    void onAdd(){
        if (another_reason.getText().toString().equals("")){
            answerOrder(2,id);
        }else {
            answerOrder(2,another_reason.getText().toString());
        }

    }
    @OnClick(R.id.cancel)
    void onCancel(){
        DialogRefuse.this.dismiss();
    }
}
