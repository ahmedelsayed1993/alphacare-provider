package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareprovider.Models.MessageModel;
import com.aait.alphacareprovider.R;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    Context context;
    List<MessageModel> messageModels;
    int id;
    LinearLayout.LayoutParams paramsMsg;
    public ChatAdapter(Context context, List<MessageModel> messageModels, int id) {
        this.context = context;
        this.messageModels = messageModels;
        this.id = id;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == 0) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_chat_send, parent, false));
        } else
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_chat_receive, parent, false));
    }

    @Override
    public int getItemViewType(int position) {

        if (messageModels.get(position).getIs_sender().equals("1")) {

            return 0;
        } else {

            return 1;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.msg.setText(messageModels.get(position).getMessage_body());


    }

    @Override
    public int getItemCount() {
        return messageModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView msg;



        public ViewHolder(View itemView) {
            super(itemView);
            msg = itemView.findViewById(R.id.tv_name);


        }
    }
}
