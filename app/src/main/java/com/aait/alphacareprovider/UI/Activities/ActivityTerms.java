package com.aait.alphacareprovider.UI.Activities;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.SiteInfoResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTerms extends ParentActivity  {

    @BindView(R.id.terms)
    WebView terms;


    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;

    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.terms_conditions));
        WebSettings webSettings = terms.getSettings();
        webSettings.setJavaScriptEnabled(true);
        getInfo();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_terms;
    }
    private void getInfo(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getInfo(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<SiteInfoResponse>() {
            @Override
            public void onResponse(Call<SiteInfoResponse> call, Response<SiteInfoResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        terms.loadData(response.body().getData().getSite_condition(), "text/html", "utf-8");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SiteInfoResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }



//    @Override
//    public Drawable getDrawable(String s) {
//        LevelListDrawable d = new LevelListDrawable();
//        Drawable empty = getResources().getDrawable(R.drawable.ic_launcher_background);
//        d.addLevel(0, 0, empty);
//        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());
//
//        new LoadImage().execute(source, d);
//
//        return d;
//    }

//    @SuppressLint("StaticFieldLeak")
//    class LoadImage extends AsyncTask<Object, Void, Bitmap> {
//
//        private LevelListDrawable mDrawable;
//
//        @Override
//        protected Bitmap doInBackground(Object... params) {
//            String source = (String) params[0];
//            mDrawable = (LevelListDrawable) params[1];
//            Log.d(TAG, "doInBackground " + source);
//            try {
//                InputStream is = new URL(source).openStream();
//                return BitmapFactory.decodeStream(is);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap bitmap) {
//            Log.d(TAG, "onPostExecute drawable " + mDrawable);
//            Log.d(TAG, "onPostExecute bitmap " + bitmap);
//            if (bitmap != null) {
//                BitmapDrawable d = new BitmapDrawable(bitmap);
//                mDrawable.addLevel(1, 1, d);
//                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
//                mDrawable.setLevel(1);
//                // i don't know yet a better way to refresh TextView
//                // mTv.invalidate() doesn't work as expected
//                CharSequence t = terms.getText();
//                terms.setText(t);
//            }
//        }
//    }
}
