package com.aait.alphacareprovider.UI.Activities;

import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Listeners.PaginationScrollListener;
import com.aait.alphacareprovider.Models.NotificationModel;
import com.aait.alphacareprovider.Models.NotificationResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.NotificationAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<NotificationModel> orderModels = new ArrayList<>();
    NotificationAdapter ordersAdapter;
    String newToken = null;
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.notifications));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( NotificationActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                ordersAdapter = new NotificationAdapter(mContext,orderModels,R.layout.recycler_notification,mLanguagePrefManager.getAppLanguage(),newToken);
                ordersAdapter.setOnItemClickListener(NotificationActivity.this);
                rvRecycle.setLayoutManager(linearLayoutManager);
                rvRecycle.setAdapter(ordersAdapter);
                rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;
                                getQuestions(newToken);
                                Log.e("newToken",newToken);



                    }

                    @Override
                    public int getTotalPageCount() {
                        return 0;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });

                swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
                swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        currentPage = 1;
                        orderModels.clear();
                        isLastPage = false;
                        isLoading = false;
                        getQuestions(newToken);

                    }
                });
                Log.e("newToken",newToken);

            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_notification;
    }

    @Override
    public void onItemClick(View view, int position) {

    }
    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        orderModels.clear();
        isLastPage = false;
        isLoading = false;
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( NotificationActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                getQuestions(newToken);
                Log.e("newToken",newToken);

            }
        });
    }

    private void getQuestions(String token){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getNotification(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id(),token,currentPage).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getData().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getData().isEmpty()) {
                                ordersAdapter.updateAll( response.body().getData().getData());
                            } else {
                                ordersAdapter.InsertAll( response.body().getData().getData());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getData().isEmpty()) {
                                ordersAdapter.updateAll( response.body().getData().getData());
                                isLastPage = true;
                            } else {
                                ordersAdapter.InsertAll( response.body().getData().getData());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }
}
