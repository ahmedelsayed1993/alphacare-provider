package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.Singltone;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Views.ReportsDialog;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.aait.alphacareprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class CompleteRegisterTwoActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    int count = 1;
    @BindView(R.id.technical_lay)
    LinearLayout technical_lay;
    @BindView(R.id.specialized_lay)
    LinearLayout specialized_lay;
    Singltone singltone;
    @OnClick(R.id.one)
    void onOne(){
        startActivity(new Intent(mContext,RegisterActivity.class));
    }
    @OnClick(R.id.two)
    void onTwo(){
        startActivity(new Intent(mContext,CompleteRegisterOneActivity.class));
    }
    @BindView(R.id.id_image)
    ImageView id_image;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.description_en)
    EditText description_en;
    @BindView(R.id.junior)
    RadioButton junior;
    @BindView(R.id.avarage)
    RadioButton avarage;
    @BindView(R.id.experienced)
    RadioButton experienced;
    @BindView(R.id.senior)
    RadioButton senior;
    @BindView(R.id.IBAN_Number)
    EditText IBAN_Number;
    @BindView(R.id.IBAN_Image)
    ImageView IBAN_Image;
    @BindView(R.id.provider_description)
    EditText provider_description;
    @BindView(R.id.provider_description_en)
    EditText provider_description_en;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm_pass)
    EditText confirm_pass;
    String path=null , path1 = null;
    String newToken= null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)
            .setCount(1)
            .setFrontfacing(false)
            .setImageQuality(ImageQuality.HIGH)
            .setPreSelectedUrls(returnValue)
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
            .setPath("/pix/images");
    ArrayList<String> returnValue1 = new ArrayList<>();
    Options options1 = Options.init()
            .setRequestCode(200)
            .setCount(1)
            .setFrontfacing(false)
            .setImageQuality(ImageQuality.HIGH)
            .setPreSelectedUrls(returnValue1)
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
            .setPath("/pix/images");
    @Override
    protected void initializeComponents() {
        singltone = Singltone.getInstance();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( CompleteRegisterTwoActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
        Log.e("user",new Gson().toJson(singltone.registerModel));
          setData();
        if (singltone.registerModel.isTechnical()){
            technical_lay.setVisibility(View.VISIBLE);
            specialized_lay.setVisibility(View.GONE);
        }else {
            technical_lay.setVisibility(View.GONE);
            specialized_lay.setVisibility(View.VISIBLE);
        }

    }
    @OnClick(R.id.junior)
    void onJunior(){
        count = 1;
        junior.setChecked(true);
    }
    @OnClick(R.id.avarage)
    void onAvarade(){
        count = 2;
        avarage.setChecked(true);
    }
    @OnClick(R.id.experienced)
    void onExperienced(){
        count = 3;
        experienced.setChecked(true);
    }
    @OnClick(R.id.senior)
    void onSenior(){
        count = 4;
        senior.setChecked(true);
    }
    @OnClick(R.id.id_image)
    void onImage(){

        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }

    }
    @OnClick(R.id.IBAN_Image)
    void onProfessional(){

        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options1);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options1);
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_complete_register_two;
    }
    void setData(){
        List<View> views = new ArrayList<>();
        views.add(id_image);
        views.add(description);
        views.add(description_en);
        views.add(IBAN_Number);
        views.add(IBAN_Image);
        views.add(provider_description);
        views.add(provider_description_en);
        views.add(password);
        if (singltone.registerModel.isTechnical()){
            technical_lay.setVisibility(View.VISIBLE);
            specialized_lay.setVisibility(View.GONE);
            if (singltone.registerModel.getExperience()==1){
                junior.setChecked(true);
            }else if (singltone.registerModel.getExperience()==2){
                avarage.setChecked(true);
            }else if (singltone.registerModel.getExperience()==3){
                experienced.setChecked(true);
            }else if (singltone.registerModel.getExperience()==4){
                senior.setChecked(true);
            }
            path = singltone.registerModel.getDegree_copy();
            path1 = singltone.registerModel.getIban_copy();
            Glide.with(mContext).load(singltone.registerModel.getDegree_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(id_image);
            description.setText(singltone.registerModel.getDesc_ar());
            description_en.setText(singltone.registerModel.getDesc_en());
        }else {
            technical_lay.setVisibility(View.GONE);
            specialized_lay.setVisibility(View.VISIBLE);
            IBAN_Number.setText(singltone.registerModel.getBank_iban());
            Glide.with(mContext).load(singltone.registerModel.getIban_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(IBAN_Image);
            provider_description.setText(singltone.registerModel.getDesc_ar());
            provider_description_en.setText(singltone.registerModel.getDesc_en());
        }
        password.setText(singltone.registerModel.getPassword());
        confirm_pass.setText(singltone.registerModel.getPassword());


        if (singltone.registerModel.isTechnical()){
            if (singltone.registerModel.getDegree_copy().equals("")){
                id_image.requestFocus();
            }else if (singltone.registerModel.getDesc_ar().equals("")){
                description.requestFocus();
            }else if (singltone.registerModel.getDesc_en().equals("")){
                description_en.requestFocus();
            }else if (singltone.registerModel.getPassword().equals("")){
                password.requestFocus();
            }
        }else {
            if (singltone.registerModel.getBank_iban().equals("")){
                IBAN_Number.requestFocus();
            }else if (singltone.registerModel.getIban_copy().equals("")){
                IBAN_Image.requestFocus();
            }else if (singltone.registerModel.getDesc_ar().equals("")){
                provider_description.requestFocus();
            }else if (singltone.registerModel.getDesc_en().equals("")){
                provider_description_en.requestFocus();
            }else if (singltone.registerModel.getPassword().equals("")){
                password.requestFocus();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == 100){
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                path = returnValue.get(0);
                singltone.registerModel.setDegree_copy(path);
                Glide.with(mContext).load(singltone.registerModel.getDegree_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(id_image);
            }else if (requestCode == 200){
                returnValue1 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                path1 = returnValue1.get(0);
                singltone.registerModel.setIban_copy(path1);
                Glide.with(mContext).load(singltone.registerModel.getIban_copy()).apply(new RequestOptions().error(R.mipmap.uploaddd)).into(IBAN_Image);
            }
        }
    }
    @OnClick(R.id.register)
    void onRegister(){
        if(singltone.registerModel.isTechnical()){
            if (!singltone.registerModel.getDegree_copy().equals("")){
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,description,getString(R.string.description_provider))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,description_en,getString(R.string.description_provider_en))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_pass,getString(R.string.confirm_password))){
                    return;
                }else {
                    if (!password.getText().toString().equals(confirm_pass.getText().toString())){
                        CommonUtil.makeToast(mContext,getString(R.string.password_not_match));
                    }else {
                        singltone.registerModel.setDegree_copy(path);
                        singltone.registerModel.setDesc_ar(description.getText().toString());
                        singltone.registerModel.setDesc_en(description_en.getText().toString());
                        singltone.registerModel.setPassword(password.getText().toString());
                        singltone.registerModel.setExperience(count);
                        registerTechnical();
                    }
                }
            }else {
                CommonUtil.makeToast(mContext,getString(R.string.Certificate_of_scientific_qualification));
            }
        }else {


                    if (CommonUtil.checkTextError((AppCompatActivity)mContext,provider_description,getString(R.string.description_provider))||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,provider_description_en,getString(R.string.description_provider_en))||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.password))||
                            CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_pass,getString(R.string.confirm_password))){
                        return;
                    }else {
                        if (!password.getText().toString().equals(confirm_pass.getText().toString())){
                            CommonUtil.makeToast(mContext,getString(R.string.password_not_match));
                        }else {
                            singltone.registerModel.setBank_iban(IBAN_Number.getText().toString());
                            if (path1==null){
                                singltone.registerModel.setIban_copy("");
                            }else {
                                singltone.registerModel.setIban_copy(path1);
                            }
                            singltone.registerModel.setDesc_ar(provider_description.getText().toString());
                            singltone.registerModel.setDesc_en(provider_description_en.getText().toString());
                            singltone.registerModel.setPassword(password.getText().toString());
                            Log.e("ueer",new Gson().toJson(singltone.registerModel));
                            if (singltone.registerModel.getNational_copy().equals("")){
                                if (singltone.registerModel.getIban_copy().equals("")){
                                    if (singltone.registerModel.getBank_iban().equals("")){
                                        registerLabwithout();
                                    }else {
                                        registerLab();
                                    }
                                }else {
                                    registerLabwithiban();
                                }
                            }else {
                                registerLabwithcopy();
                            }
//

                        }
                    }


        }

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    private void registerTechnical(){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(singltone.registerModel.getAvatar());
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, CompleteRegisterTwoActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        MultipartBody.Part filePart1 = null;
        File ImageFile1 = new File(singltone.registerModel.getNational_copy());
        ProgressRequestBody fileBody1 = new ProgressRequestBody(ImageFile1, CompleteRegisterTwoActivity.this);
        filePart1 = MultipartBody.Part.createFormData("national_copy", ImageFile1.getName(), fileBody1);
        MultipartBody.Part filePart2 = null;
        File ImageFile2 = new File(singltone.registerModel.getPractice_copy());
        ProgressRequestBody fileBody2 = new ProgressRequestBody(ImageFile1, CompleteRegisterTwoActivity.this);
        filePart2 = MultipartBody.Part.createFormData("practice_copy", ImageFile2.getName(), fileBody2);
        MultipartBody.Part filePart3 = null;
        File ImageFile3 = new File(singltone.registerModel.getCard_copy());
        ProgressRequestBody fileBody3 = new ProgressRequestBody(ImageFile3, CompleteRegisterTwoActivity.this);
        filePart3 = MultipartBody.Part.createFormData("card_copy", ImageFile3.getName(), fileBody3);
        MultipartBody.Part filePart4 = null;
        File ImageFile4= new File(singltone.registerModel.getDegree_copy());
        ProgressRequestBody fileBody4 = new ProgressRequestBody(ImageFile4, CompleteRegisterTwoActivity.this);
        filePart4 = MultipartBody.Part.createFormData("degree_copy", ImageFile4.getName(), fileBody4);
        RetroWeb.getClient().create(ServiceApi.class).registerTechnical(mLanguagePrefManager.getAppLanguage(),filePart,singltone.registerModel.getName(),singltone.registerModel.getPhone(),singltone.registerModel.getEmail(),
                singltone.registerModel.getCity_id(),singltone.registerModel.getGender(),singltone.registerModel.getSection_id(),singltone.registerModel.getProvider_type()
        ,singltone.registerModel.getNational_id(),filePart1,singltone.registerModel.getLat(),singltone.registerModel.getLng(),singltone.registerModel.getLatlng_address_ar(),singltone.registerModel.getLatlng_address_en()
        ,filePart2,filePart3,filePart4,singltone.registerModel.getExperience(),singltone.registerModel.getDesc_ar(),singltone.registerModel.getDesc_en(),singltone.registerModel.getPassword()
        ,1,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        new ReportsDialog(mContext,response.body().getData()).show();
                        Singltone.setSingletonInstance(null);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    private void registerLab(){
        MultipartBody.Part filePart = null;
        File ImageFile = new File(singltone.registerModel.getAvatar());
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, CompleteRegisterTwoActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        MultipartBody.Part filePart1 = null;
        File ImageFile1 = new File(singltone.registerModel.getNational_copy());
        ProgressRequestBody fileBody1 = new ProgressRequestBody(ImageFile1, CompleteRegisterTwoActivity.this);
        filePart1 = MultipartBody.Part.createFormData("national_copy", ImageFile1.getName(), fileBody1);
        MultipartBody.Part filePart2 = null;
        File ImageFile2 = new File(singltone.registerModel.getLisence_copy());
        ProgressRequestBody fileBody2 = new ProgressRequestBody(ImageFile2, CompleteRegisterTwoActivity.this);
        filePart2 = MultipartBody.Part.createFormData("lisence_copy", ImageFile2.getName(), fileBody2);
        MultipartBody.Part filePart3 = null;
        File ImageFile3 = new File(singltone.registerModel.getIban_copy());
        ProgressRequestBody fileBody3 = new ProgressRequestBody(ImageFile3, CompleteRegisterTwoActivity.this);
        filePart3 = MultipartBody.Part.createFormData("iban_copy", ImageFile3.getName(), fileBody3);
        MultipartBody.Part filepart = null;
        File file = new File(singltone.registerModel.getPapers_pdf());
       RequestBody requestBody =  RequestBody.create(MediaType.parse("*/*"),file);
        filepart = MultipartBody.Part.createFormData("papers_pdf",file.getName(),requestBody);
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).registerLab(mLanguagePrefManager.getAppLanguage(),filePart,singltone.registerModel.getName()
        ,singltone.registerModel.getPhone(),singltone.registerModel.getEmail(),singltone.registerModel.getCity_id(),singltone.registerModel.getGender(),singltone.registerModel.getSection_id(),singltone.registerModel.getProvider_type()
        ,singltone.registerModel.getNational_id(),filePart1,filePart2,filepart,singltone.registerModel.getBank_iban(),filePart3
        ,singltone.registerModel.getDesc_ar(),singltone.registerModel.getDesc_en(),singltone.registerModel.getPassword(),1,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        new ReportsDialog(mContext,response.body().getData()).show();
                        Singltone.setSingletonInstance(null);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void registerLabwithiban(){
        MultipartBody.Part filePart = null;
        File ImageFile = new File(singltone.registerModel.getAvatar());
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, CompleteRegisterTwoActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);

        MultipartBody.Part filePart2 = null;
        File ImageFile2 = new File(singltone.registerModel.getLisence_copy());
        ProgressRequestBody fileBody2 = new ProgressRequestBody(ImageFile2, CompleteRegisterTwoActivity.this);
        filePart2 = MultipartBody.Part.createFormData("lisence_copy", ImageFile2.getName(), fileBody2);
        MultipartBody.Part filePart3 = null;
        File ImageFile3 = new File(singltone.registerModel.getIban_copy());
        ProgressRequestBody fileBody3 = new ProgressRequestBody(ImageFile3, CompleteRegisterTwoActivity.this);
        filePart3 = MultipartBody.Part.createFormData("iban_copy", ImageFile3.getName(), fileBody3);
        MultipartBody.Part filepart = null;
        File file = new File(singltone.registerModel.getPapers_pdf());
        RequestBody requestBody =  RequestBody.create(MediaType.parse("*/*"),file);
        filepart = MultipartBody.Part.createFormData("papers_pdf",file.getName(),requestBody);
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).registerLabwithiban(mLanguagePrefManager.getAppLanguage(),filePart,singltone.registerModel.getName()
                ,singltone.registerModel.getPhone(),singltone.registerModel.getEmail(),singltone.registerModel.getCity_id(),singltone.registerModel.getGender(),singltone.registerModel.getSection_id(),singltone.registerModel.getProvider_type()
                ,singltone.registerModel.getNational_id(),filePart2,filepart,singltone.registerModel.getBank_iban(),filePart3
                ,singltone.registerModel.getDesc_ar(),singltone.registerModel.getDesc_en(),singltone.registerModel.getPassword(),1,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        new ReportsDialog(mContext,response.body().getData()).show();
                        Singltone.setSingletonInstance(null);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    private void registerLabwithcopy(){
        MultipartBody.Part filePart = null;
        File ImageFile = new File(singltone.registerModel.getAvatar());
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, CompleteRegisterTwoActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        MultipartBody.Part filePart1 = null;
        File ImageFile1 = new File(singltone.registerModel.getNational_copy());
        ProgressRequestBody fileBody1 = new ProgressRequestBody(ImageFile1, CompleteRegisterTwoActivity.this);
        filePart1 = MultipartBody.Part.createFormData("national_copy", ImageFile1.getName(), fileBody1);
        MultipartBody.Part filePart2 = null;
        File ImageFile2 = new File(singltone.registerModel.getLisence_copy());
        ProgressRequestBody fileBody2 = new ProgressRequestBody(ImageFile2, CompleteRegisterTwoActivity.this);
        filePart2 = MultipartBody.Part.createFormData("lisence_copy", ImageFile2.getName(), fileBody2);

        MultipartBody.Part filepart = null;
        File file = new File(singltone.registerModel.getPapers_pdf());
        RequestBody requestBody =  RequestBody.create(MediaType.parse("*/*"),file);
        filepart = MultipartBody.Part.createFormData("papers_pdf",file.getName(),requestBody);
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).registerLabwithcopy(mLanguagePrefManager.getAppLanguage(),filePart,singltone.registerModel.getName()
                ,singltone.registerModel.getPhone(),singltone.registerModel.getEmail(),singltone.registerModel.getCity_id(),singltone.registerModel.getGender(),singltone.registerModel.getSection_id(),singltone.registerModel.getProvider_type()
                ,singltone.registerModel.getNational_id(),filePart1,filePart2,filepart,singltone.registerModel.getBank_iban()
                ,singltone.registerModel.getDesc_ar(),singltone.registerModel.getDesc_en(),singltone.registerModel.getPassword(),1,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        new ReportsDialog(mContext,response.body().getData()).show();
                        Singltone.setSingletonInstance(null);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void registerLabwithout(){
        MultipartBody.Part filePart = null;
        File ImageFile = new File(singltone.registerModel.getAvatar());
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, CompleteRegisterTwoActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);

        MultipartBody.Part filePart2 = null;
        File ImageFile2 = new File(singltone.registerModel.getLisence_copy());
        ProgressRequestBody fileBody2 = new ProgressRequestBody(ImageFile2, CompleteRegisterTwoActivity.this);
        filePart2 = MultipartBody.Part.createFormData("lisence_copy", ImageFile2.getName(), fileBody2);

        MultipartBody.Part filepart = null;
        File file = new File(singltone.registerModel.getPapers_pdf());
        RequestBody requestBody =  RequestBody.create(MediaType.parse("*/*"),file);
        filepart = MultipartBody.Part.createFormData("papers_pdf",file.getName(),requestBody);
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).registerLabwitout(mLanguagePrefManager.getAppLanguage(),filePart,singltone.registerModel.getName()
                ,singltone.registerModel.getPhone(),singltone.registerModel.getEmail(),singltone.registerModel.getCity_id(),singltone.registerModel.getGender(),singltone.registerModel.getSection_id(),singltone.registerModel.getProvider_type()
                ,singltone.registerModel.getNational_id(),filePart2,filepart,singltone.registerModel.getBank_iban()
                ,singltone.registerModel.getDesc_ar(),singltone.registerModel.getDesc_en(),singltone.registerModel.getPassword(),1,newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        new ReportsDialog(mContext,response.body().getData()).show();
                        Singltone.setSingletonInstance(null);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    @OnClick(R.id.login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
    }
}
