package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.ServiceModel;
import com.aait.alphacareprovider.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;

public class ServicesAdapter extends ParentRecyclerAdapter<ServiceModel> {
    public ServicesAdapter(Context context, List<ServiceModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ServicesAdapter.ViewHolder holder = new ServicesAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ServicesAdapter.ViewHolder viewHolder = (ServicesAdapter.ViewHolder) holder;
        final ServiceModel addressesModel = data.get(position);
        viewHolder.service_name.setText(addressesModel.getTitle());
        viewHolder.price.setText(addressesModel.getPrice()+mcontext.getResources().getString(R.string.sar));
        Glide.with(mcontext).load(addressesModel.getImage()).apply(new RequestOptions().placeholder(mcontext.getResources().getDrawable(R.mipmap.logo))).into(viewHolder.image);

        viewHolder.service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });

    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.service_name)
        TextView service_name;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.service)
        LinearLayout service;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
