package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareprovider.App.Constant;
import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.AddressesModel;
import com.aait.alphacareprovider.Models.AddressesResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.AddressesAdapter;
import com.aait.alphacareprovider.UI.Views.DailogDeleteAddress;
import com.aait.alphacareprovider.UI.Views.DialogAddAddress;
import com.aait.alphacareprovider.UI.Views.DialogEditAddress;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.core.provider.FontsContractCompat.FontRequestCallback.RESULT_OK;

public class AddressesActivity extends ParentActivity implements OnItemClickListener {
    @OnClick(R.id.back)
    void onBack(){
        Intent intent = new Intent(mContext,MainActivity.class);
        intent.putExtra("type","normal");
        startActivity(intent);
        AddressesActivity.this.finish();
    }
    @BindView(R.id.title)
    TextView act_title;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<AddressesModel> addressesModels = new ArrayList<>();
    AddressesAdapter addressesAdapter;
    String newToken = null;
    DialogAddAddress dialogAddAddress;
    DialogEditAddress dialogEditAddress;
    DailogDeleteAddress dailogDeleteAddress;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.my_addresses));

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( AddressesActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        newToken = instanceIdResult.getToken();
                        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                        addressesAdapter = new AddressesAdapter(mContext,addressesModels,R.layout.recycler_address,newToken);
                        addressesAdapter.setOnItemClickListener(AddressesActivity.this);
                        rvRecycle.setLayoutManager(linearLayoutManager);
                        rvRecycle.setAdapter(addressesAdapter);
                        getAddress(newToken);
                        Log.e("newToken",newToken);

                    }
                });
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( AddressesActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                addressesAdapter = new AddressesAdapter(mContext,addressesModels,R.layout.recycler_address,newToken);
                addressesAdapter.setOnItemClickListener(AddressesActivity.this);
                rvRecycle.setLayoutManager(linearLayoutManager);
                rvRecycle.setAdapter(addressesAdapter);
                getAddress(newToken);
                Log.e("newToken",newToken);

            }
        });


    }
    @OnClick(R.id.add_address)
    void onAdd(){
        dialogAddAddress = new DialogAddAddress(mContext,newToken);
        dialogAddAddress.show();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_addresses;
    }
    private void getAddress(String token){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getAddress(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getToken_id(),token,1).enqueue(new Callback<AddressesResponse>() {
            @Override
            public void onResponse(Call<AddressesResponse> call, Response<AddressesResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            addressesAdapter.updateAll(response.body().getData());
                        }
                    }
                    else if (response.body().getKey().equals("2")&&response.body().getUser_status().equals("non-active")){

                        Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                        intent.putExtra("data",mSharedPrefManager.getUserData());
                        startActivity(intent);

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    dialogAddAddress.onActivityResult(requestCode, resultCode, data);
                    dialogAddAddress.mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    dialogAddAddress.mAddress = data.getStringExtra("LOCATION");
                    dialogAddAddress.address = data.getStringExtra("LOC");
                    dialogAddAddress.mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    dialogAddAddress.mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + dialogAddAddress.mLat + " Lng : " + dialogAddAddress.mLang + " Address : " + dialogAddAddress.mAdresse + "  " + dialogAddAddress.mAddress + "  " + dialogAddAddress.address);
                    if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                        dialogAddAddress.addresses.setText(dialogAddAddress.mAddress);
                    } else if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                        dialogAddAddress.addresses.setText(dialogAddAddress.mAdresse);
                    }
                }
            } else if (requestCode == Constant.RequestCode.GET_LOCATIONONMAP){
                if (resultCode == RESULT_OK) {
                    dialogEditAddress.onActivityResult(requestCode, resultCode, data);
                    dialogEditAddress.mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    dialogEditAddress.mAddress = data.getStringExtra("LOCATION");
                    dialogEditAddress.address = data.getStringExtra("LOC");
                    dialogEditAddress.mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    dialogEditAddress.mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    Log.e("Loc","Lat : " + dialogEditAddress.mLat + " Lng : " + dialogEditAddress.mLang + " Address : " + dialogEditAddress.mAdresse + "  " + dialogEditAddress.mAddress + "  " + dialogEditAddress.address);
                    if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                        dialogEditAddress.addresses.setText(dialogEditAddress.mAddress);
                    } else if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                        dialogEditAddress.addresses.setText(dialogEditAddress.mAdresse);
                    }

//                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
//                    mAddress = data.getStringExtra("LOCATION");
//                    address = data.getStringExtra("LOC");
//                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
//                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
//                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse + "  " + mAddress + "  " + address);
//                    if (languagePrefManager.getAppLanguage().equals("ar")) {
//                        addresses.setText(mAddress);
//                    } else if (languagePrefManager.getAppLanguage().equals("en")) {
//                        addresses.setText(mAdresse);
//                    }

                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext,MainActivity.class));
        this.finish();
    }
}
