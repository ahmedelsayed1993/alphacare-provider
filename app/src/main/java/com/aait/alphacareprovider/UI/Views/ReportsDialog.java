package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareprovider.Models.SiteReportsModel;
import com.aait.alphacareprovider.Models.SiteReportsResponse;
import com.aait.alphacareprovider.Models.UserModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.ActivateAccountActivity;
import com.aait.alphacareprovider.UI.Adapters.ReportsAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportsDialog extends Dialog {
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    Context mContext;
    LinearLayoutManager linearLayoutManager;
    ArrayList<SiteReportsModel> siteReportsModels = new ArrayList<>();
    ReportsAdapter reportsAdapter;
    @BindView(R.id.reports)
    RecyclerView reports;
    UserModel userModel;
    @BindView(R.id.done)
    Button done;
    public ReportsDialog(@NonNull Context context, UserModel userModel) {
        super(context);
        this.mContext = context;
        this.userModel = userModel;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_reports);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(false);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        reportsAdapter = new ReportsAdapter(mContext,siteReportsModels,R.layout.recycler_reports);
        reports.setLayoutManager(linearLayoutManager);
        reports.setAdapter(reportsAdapter);
        getReports();

    }
    void getReports(){
        RetroWeb.getClient().create(ServiceApi.class).getReports(languagePrefManager.getAppLanguage()).enqueue(new Callback<SiteReportsResponse>() {
            @Override
            public void onResponse(Call<SiteReportsResponse> call, Response<SiteReportsResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        reportsAdapter.updateAll(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SiteReportsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();

            }
        });
    }
    @OnClick(R.id.done)
    void onDone(){
        Intent intent = new Intent(mContext, ActivateAccountActivity.class);
        intent.putExtra("data",userModel);
        mContext.startActivity(intent);
    }
}
