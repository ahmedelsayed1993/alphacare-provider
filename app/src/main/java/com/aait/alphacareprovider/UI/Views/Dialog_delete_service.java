package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.AddressesModel;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.ServiceModel;
import com.aait.alphacareprovider.Models.ServicesModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dialog_delete_service extends Dialog implements OnItemClickListener {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    String token;
    ServicesModel addressesModel;
    @BindView(R.id.address)
    TextView address;
    private ProgressDialog mProgressDialog;
    public Dialog_delete_service(@NonNull Context context, String token, ServicesModel addressesModel) {
        super(context);
        this.mContext = context;
        this.token = token;
        this.addressesModel = addressesModel;

    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_delet_service);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        if (languagePrefManager.getAppLanguage().equals("ar")) {
            address.setText(mContext.getString(R.string.are_you_sure_service) + ("'" + addressesModel.getTitle() + "'") + "؟");
        }else {
            address.setText(mContext.getString(R.string.are_you_sure_service) + ("'" + addressesModel.getTitle() + "'") + "?");
        }


    }

    private void delete(){
        mProgressDialog = DialogUtil.showProgressDialog(mContext, mContext.getString(R.string.please_wait), false);
        RetroWeb.getClient().create(ServiceApi.class).deleteProduct(1,sharedPreferences.getUserData().getToken_id(),token,addressesModel.getId(),languagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Dialog_delete_service.this.dismiss();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Dialog_delete_service.this.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                mProgressDialog.dismiss();

            }
        });
    }
    @OnClick(R.id.save)
    void onSave(){
        delete();
    }

    @OnClick(R.id.cancel)
    void onCancel(){
        Dialog_delete_service.this.cancel();
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
