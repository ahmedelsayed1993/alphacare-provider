package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.CommissionModel;
import com.aait.alphacareprovider.R;

import java.util.List;

import butterknife.BindView;

public class CommissionsAdapter extends ParentRecyclerAdapter<CommissionModel> {
    public CommissionsAdapter(Context context, List<CommissionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        CommissionsAdapter.ViewHolder holder = new CommissionsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final CommissionsAdapter.ViewHolder viewHolder = (CommissionsAdapter.ViewHolder) holder;
        final CommissionModel addressesModel = data.get(position);
        viewHolder.process.setText("#"+addressesModel.getOrder_id());
        viewHolder.date.setText(addressesModel.getDate());
        viewHolder.value.setText(addressesModel.getDept()+"-");
        if (addressesModel.isSelected()){
            viewHolder.check.setChecked(true);
        }else {
            viewHolder.check.setChecked(false);
        }
        if (addressesModel.getPaid()==1){
            viewHolder.check.setVisibility(View.INVISIBLE);
            viewHolder.value.setTextColor(mcontext.getResources().getColor(R.color.colorGreen));
            viewHolder.process.setTextColor(mcontext.getResources().getColor(R.color.colorGreen));
        }else {

            viewHolder.check.setVisibility(View.VISIBLE);
            viewHolder.value.setTextColor(mcontext.getResources().getColor(R.color.colorRed));
            viewHolder.process.setTextColor(mcontext.getResources().getColor(R.color.colorRed));
        }
        viewHolder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {
        @BindView(R.id.process)
        TextView process;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.value)
        TextView value;
        @BindView(R.id.check)
        CheckBox check;
        @BindView(R.id.lay)
        LinearLayout lay;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
