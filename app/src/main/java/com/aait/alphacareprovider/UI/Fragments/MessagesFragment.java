package com.aait.alphacareprovider.UI.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareprovider.Base.BaseFragment;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Listeners.PaginationScrollListener;
import com.aait.alphacareprovider.Models.MessagesResponse;
import com.aait.alphacareprovider.Models.NotificationResponse;
import com.aait.alphacareprovider.Models.RoomModel;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.NotificationActivity;
import com.aait.alphacareprovider.UI.Adapters.MessagesAdapter;
import com.aait.alphacareprovider.UI.Adapters.NotificationAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessagesFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    String newToken = null;
    MessagesAdapter messagesAdapter;
    ArrayList<RoomModel> roomModels = new ArrayList<>();
    @BindView(R.id.online)
    SwitchCompat online;
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    LinearLayoutManager linearLayoutManager;
    public static MessagesFragment newInstance(String newToken) {
        Bundle args = new Bundle();
        MessagesFragment fragment = new MessagesFragment();
        args.putString("token",newToken);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_messages;
    }

    @Override
    protected void initializeComponents(View view) {
        online.setVisibility(View.VISIBLE);
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        title.setText(getString(R.string.messages));
        if(mSharedPrefManager.getUserData().getIs_online().equals("1")){
            online.setChecked(true);
//            online.setActivated(true);
        }else {
            online.setChecked(false);
//            online.setActivated(false);
        }
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);

                messagesAdapter = new MessagesAdapter(mContext,roomModels,R.layout.recycler_chats,newToken);
                messagesAdapter.setOnItemClickListener(this);
                rvRecycle.setLayoutManager(linearLayoutManager);
                rvRecycle.setAdapter(messagesAdapter);
                rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;
                        getQuestions(newToken);
                        Log.e("newToken",newToken);



                    }

                    @Override
                    public int getTotalPageCount() {
                        return 0;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });

                swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
                swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        currentPage = 1;
                        roomModels.clear();
                        isLastPage = false;
                        isLoading = false;
                        getQuestions(newToken);

                    }
                });


    }
    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        roomModels.clear();
        isLastPage = false;
        isLoading = false;

        getQuestions(newToken);
        Log.e("newToken",newToken);

        if(mSharedPrefManager.getUserData().getIs_online().equals("1")){
            online.setChecked(true);
//            online.setActivated(true);
        }else {
            online.setChecked(false);
//            online.setActivated(false);
        }



    }
    private void getQuestions(String token){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getMessages(1,mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getToken_id(),token,currentPage).enqueue(new Callback<MessagesResponse>() {
            @Override
            public void onResponse(Call<MessagesResponse> call, Response<MessagesResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                Log.e("res",new Gson().toJson(response.body().getData()));
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getData().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);

                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getData().isEmpty()) {
                                messagesAdapter.updateAll( response.body().getData().getData());
                            } else {
                                messagesAdapter.InsertAll( response.body().getData().getData());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getData().isEmpty()) {
                                messagesAdapter.updateAll( response.body().getData().getData());
                                isLastPage = true;
                            } else {
                                messagesAdapter.InsertAll( response.body().getData().getData());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<MessagesResponse> call, Throwable t) {
                Log.e("err",new Gson().toJson(t));
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }
    @OnClick(R.id.online)
    void onLine(){
        if (mSharedPrefManager.getUserData().getIs_online().equals("1")){
            active("0");
        }else {
            active("1");
        }
    }
    private void active(String status){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).active(1,mSharedPrefManager.getUserData().getToken_id(),newToken,status,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        mSharedPrefManager.setUserData(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
