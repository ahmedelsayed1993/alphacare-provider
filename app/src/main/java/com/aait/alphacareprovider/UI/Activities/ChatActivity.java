package com.aait.alphacareprovider.UI.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.PaginationScrollListener;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.ChatResponse;
import com.aait.alphacareprovider.Models.MessageModel;
import com.aait.alphacareprovider.Models.MessageResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.ChatAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends ParentActivity {
    public static boolean mActive=false;
    SharedPreferences sharedPreferences;
    BroadcastReceiver MsgReciever;
    String room;
    String token;
    String name;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.et_chat)
    EditText etChat;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    RecyclerView.Adapter adapter;
    List<MessageModel> messageModels;
    public static int reciver=0;
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @Override
    protected void initializeComponents() {
        sharedPreferences = getSharedPreferences("home", MODE_PRIVATE);
        room = getIntent().getStringExtra("room");
        token = getIntent().getStringExtra("token");
        name = getIntent().getStringExtra("name");
        title.setText(name);
        rvRecycle.hasFixedSize();
        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        linearLayout.setStackFromEnd(true);
        rvRecycle.setLayoutManager(linearLayout);
        messageModels=new ArrayList<>();
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayout) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                getChats();
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                messageModels.clear();
                isLastPage = false;
                isLoading = false;
                getChats();
            }
        });


        MsgReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //  Toast.makeText(ChatActivity.this, "broad", Toast.LENGTH_SHORT).show();
                final MessageModel  chatModel1 = new MessageModel();
                chatModel1.setMessage_body("");
                chatModel1.setCreated_at("");
                chatModel1.setIs_sender("");
                chatModel1.setBody_type("");
                chatModel1.setFlagged("");
                if (intent.getAction().equalsIgnoreCase("new_message")){
                    //  Toast.makeText(context, "broad2", Toast.LENGTH_SHORT).show();


                    chatModel1.setMessage_body(intent.getStringExtra("msg"));

                    chatModel1.setIs_sender(intent.getStringExtra("id"));
                    chatModel1.setRoom_id(Integer.parseInt(intent.getStringExtra("sender_id")));
                    chatModel1.setCreated_at(intent.getStringExtra("time"));
                    chatModel1.setType(intent.getStringExtra("type"));
                    chatModel1.setUser_id(Integer.parseInt(intent.getStringExtra("reciver_id")));
                    chatModel1.setId(Integer.parseInt(intent.getStringExtra("message")));

//                    Log.e("chat",chatModel1.getMessage_body());
//                    Log.e("chat",chatModel1.getIs_sender());
//                    Log.e("chat",chatModel1.getId()+"");
//                    Log.e("chat",chatModel1.getUser_id()+"");
//                    Log.e("chat",chatModel1.getType()+"");
//
//                    Log.e("chat",new Gson().toJson(chatModel1));

                    messageModels.add(chatModel1);
                    adapter = new ChatAdapter(mContext,messageModels,mSharedPrefManager.getUserData().getId());
                    rvRecycle.setAdapter(adapter);
                    see(intent.getStringExtra("message"));
                    // friend_id = intent.getIntExtra("user",0);





                }

            }
        };


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_chat;
    }

    private void see(String message){
        RetroWeb.getClient().create(ServiceApi.class).see(1,mSharedPrefManager.getUserData().getToken_id(),token,message,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){

                    }else {

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }

    private void getChats() {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
                RetroWeb.getClient().create(ServiceApi.class).getChat(1,mSharedPrefManager.getUserData().getToken_id(),token,room,mLanguagePrefManager.getAppLanguage(),currentPage).enqueue(new Callback<ChatResponse>() {
                    @Override
                    public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getKey().equals("1")) {
                                TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                                if (currentPage == 1 &&response.body().getData().getData().isEmpty()){
                                    layNoItem.setVisibility(View.VISIBLE);
                                    layNoInternet.setVisibility(View.GONE);
                                    tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                                }
                                if (currentPage < TOTAL_PAGES) {
                                    if (response.body().getData().getData().isEmpty()) {
                                       // reciver = response.body().getData().getData().get(0).getOther_users().get(0);

                                        adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
                                        rvRecycle.setAdapter(adapter);
                                    } else {
                                        for (int i=0;i<response.body().getData().getData().size();i++){
                                            messageModels.add(response.body().getData().getData().get(i));
                                        }
                                        Log.e("messages",new Gson().toJson(messageModels));
                                        //reciver = response.body().getData().getData().get(0).getOther_users().get(0);

                                        adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
                                        rvRecycle.setAdapter(adapter);
                                    }
                                    isLoading = false;
                                }else if (currentPage == TOTAL_PAGES){
                                    if (response.body().getData().getData().isEmpty()) {

                                       // reciver = response.body().getData().getData().get(0).getOther_users().get(0);

                                        adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
                                        rvRecycle.setAdapter(adapter);
                                        isLastPage = true;
                                    } else {
                                        for (int i=0;i<response.body().getData().getData().size();i++){
                                            messageModels.add(response.body().getData().getData().get(i));
                                        }
                                        //reciver = response.body().getData().getData().get(0).getOther_users().get(0);

                                        adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
                                        rvRecycle.setAdapter(adapter);
                                    }
                                }
                                else {
                                    isLastPage = true;
                                }


                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ChatResponse> call, Throwable t) {
                        Log.i("exception", "exception");
                        Log.e("errpr", String.valueOf(t));
                        CommonUtil.handleException(ChatActivity.this, t);
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });

    }
    @Override
    protected void onResume() {
        super.onResume();
        currentPage = 1;
        messageModels.clear();
        isLastPage = false;
        isLoading = false;
        getChats();
        SharedPreferences.Editor editor = getSharedPreferences("home", MODE_PRIVATE).edit();

        editor.putString("sender_id", room);

        editor.apply();
        LocalBroadcastManager.getInstance(this).registerReceiver(MsgReciever,new IntentFilter("new_message"));}


    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = getSharedPreferences("home", MODE_PRIVATE).edit();
        editor.putString("sender_id", "0");
        editor.apply();


    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @OnClick(R.id.send)
    void send() {
        final String msg = etChat.getText().toString();
        if (!msg.equals("")) {


            RetroWeb.getClient().create(ServiceApi.class).Send(1, mSharedPrefManager.getUserData().getToken_id(), token, room, msg).enqueue(new Callback<MessageResponse>() {
                @Override
                public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                   if (response.isSuccessful()) {
                       if (response.body().getKey().equals("1")) {
                           MessageModel model = new MessageModel();
                           model.setMessage_body(msg);
                           model.setBody_type(response.body().getData().getBody_type());
                           model.setCreated_at(response.body().getData().getCreated_at());
                           model.setUser_id(response.body().getData().getUser_id());
                           model.setIs_sender(response.body().getData().getIs_sender());


                           messageModels.add(model);
                           adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
                           rvRecycle.setAdapter(adapter);
                           adapter.notifyDataSetChanged();
                           etChat.setText("");
                       }else {
                           CommonUtil.makeToast(mContext,response.body().getMsg());
                       }
                   }
                }

                @Override
                public void onFailure(Call<MessageResponse> call, Throwable t) {
                    Log.i("exception", "exception");
                    hideProgressDialog();
                    Log.e("errpr", String.valueOf(t));
                    CommonUtil.handleException(ChatActivity.this, t);
                    t.printStackTrace();
                }
            });
        }
    }
}
