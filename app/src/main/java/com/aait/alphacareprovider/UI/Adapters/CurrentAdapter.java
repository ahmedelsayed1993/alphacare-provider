package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.OrderModel;
import com.aait.alphacareprovider.R;

import com.aait.alphacareprovider.UI.Activities.CurrentOrderActivity;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CurrentAdapter extends ParentRecyclerAdapter<OrderModel> {
    public CurrentAdapter(Context context, List<OrderModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        CurrentAdapter.ViewHolder holder = new CurrentAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final CurrentAdapter.ViewHolder viewHolder = (CurrentAdapter.ViewHolder) holder;
        final OrderModel addressesModel = data.get(position);
        viewHolder.order.setText(mcontext.getString(R.string.order)+"  "+addressesModel.getOrder_id()+" # ");
        viewHolder.price.setText(addressesModel.getOrder_total_price()+mcontext.getResources().getString(R.string.sar));
        viewHolder.address.setText(addressesModel.getOrder_latlng_address());
        viewHolder.name.setText(addressesModel.getProvider_name());
        Glide.with(mcontext).load(addressesModel.getProvider_avatar()).into(viewHolder.image);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
                Intent intent =new Intent(mcontext, CurrentOrderActivity.class);
                intent.putExtra("id",addressesModel.getOrder_id()+"");
                mcontext.startActivity(intent);
            }
        });

    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.time)
        TextView order;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.address)
        TextView address;

        @BindView(R.id.name)
        TextView name;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

