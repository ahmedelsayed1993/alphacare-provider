package com.aait.alphacareprovider.UI.Fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareprovider.Base.BaseFragment;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.CommentsModel;
import com.aait.alphacareprovider.Models.ProviderCommentsResponse;
import com.aait.alphacareprovider.Models.ProviderServicesResponse;
import com.aait.alphacareprovider.Models.ServiceModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.CommentsAdapter;
import com.aait.alphacareprovider.UI.Adapters.ServicesAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.comments)
    RecyclerView comments;
    @BindView(R.id.no_comment)
    TextView no_comment;
    LinearLayoutManager gridLayoutManager;
    ArrayList<CommentsModel> commentsModels = new ArrayList<>();
    CommentsAdapter commentsAdapter;
    String token;
    public static CommentsFragment newInstance(String newToken) {
        Bundle args = new Bundle();
        CommentsFragment fragment = new CommentsFragment();
        args.putString("token",newToken);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_comments;
    }

    @Override
    protected void initializeComponents(View view) {
        comments.setVisibility(View.GONE);
        Bundle bundle = this.getArguments();
        token = bundle.getString("token");
        gridLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        commentsAdapter = new CommentsAdapter(mContext,commentsModels,R.layout.recycler_comment,token);
        commentsAdapter.setOnItemClickListener(this);
        comments.setLayoutManager(gridLayoutManager);
        comments.setAdapter(commentsAdapter);
        getProvider(token);
    }
    private void getProvider(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getComments(1,mSharedPrefManager.getUserData().getToken_id(),token,"3",mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ProviderCommentsResponse>() {
            @Override
            public void onResponse(Call<ProviderCommentsResponse> call, Response<ProviderCommentsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().getTab_data().isEmpty()){
                            no_comment.setVisibility(View.VISIBLE);
                            comments.setVisibility(View.GONE);

                        }else {
                            no_comment.setVisibility(View.GONE);

                            comments.setVisibility(View.VISIBLE);
                            commentsAdapter.updateAll(response.body().getData().getTab_data());
                        }

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderCommentsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
