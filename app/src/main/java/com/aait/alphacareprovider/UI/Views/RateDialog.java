package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.OrderDetailsModdel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.FinishOrderActivity;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateDialog extends Dialog {
    private ProgressDialog mProgressDialog;
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    String token;
    OrderDetailsModdel id;
    @BindView(R.id.comment)
    EditText comment;
    @BindView(R.id.rating)
    RatingBar rating;

    public RateDialog(@NonNull Context context, String token, OrderDetailsModdel id) {
        super(context);
        this.mContext = context;
        this.token = token;
        this.id = id;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_rate);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {

    }
    @OnClick(R.id.done)
    void onDone(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,comment,mContext.getString(R.string.your_comment))){
            return;
        }else {
            rate(id.getOrder_id()+"");
        }
    }
    private void rate(String order){
        mProgressDialog = DialogUtil.showProgressDialog(mContext, mContext.getString(R.string.please_wait), false);
        RetroWeb.getClient().create(ServiceApi.class).rate(languagePrefManager.getAppLanguage(),1,sharedPreferences.getUserData().getToken_id(),token,(int) rating.getRating(),comment.getText().toString(),id.getUser_id(),order).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext, FinishOrderActivity.class);
                        intent.putExtra("order",id);
                        mContext.startActivity(intent);

                        RateDialog.this.dismiss();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                mProgressDialog.dismiss();

            }
        });
    }
}
