package com.aait.alphacareprovider.UI.Activities;

import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Fragments.HomeFragment;
import com.aait.alphacareprovider.UI.Fragments.MessagesFragment;
import com.aait.alphacareprovider.UI.Fragments.MoreFragment;
import com.aait.alphacareprovider.UI.Fragments.ProfileFragment;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Stack;

import butterknife.BindView;

public class MainActivity extends ParentActivity implements BottomNavigationView.OnNavigationItemSelectedListener,BottomNavigationView.OnNavigationItemReselectedListener{
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
     protected int selectedTab = 0;
    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;
    HomeFragment mHomeFragment;
    MoreFragment mMoreFragment;
    MessagesFragment mMessagesFragment;
    ProfileFragment mProfileFragment;
    String newToken= null;
    String type;
    @Override
    protected void initializeComponents() {
        type = getIntent().getStringExtra("type");
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( MainActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                mHomeFragment = HomeFragment.newInstance(newToken);
                mMoreFragment = MoreFragment.newInstance(newToken);
                mMessagesFragment= MessagesFragment.newInstance(newToken);
                mProfileFragment = ProfileFragment.newInstance(newToken);
                fragmentManager = getSupportFragmentManager();
                transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.home_fragment_container, mHomeFragment);
                transaction.add(R.id.home_fragment_container,mMessagesFragment);
                transaction.add(R.id.home_fragment_container,mProfileFragment);
                transaction.add(R.id.home_fragment_container,mMoreFragment);
                transaction.commit();
                bottomNavigation.setOnNavigationItemSelectedListener(MainActivity.this);
                bottomNavigation.setOnNavigationItemReselectedListener(MainActivity.this);
                if (type.equals("normal")) {
                    bottomNavigation.setSelectedItemId(R.id.home);
                    showHome(true);
                }else if (type.equals("profile")){
                    bottomNavigation.setSelectedItemId(R.id.profile);
                    showFavourite(true);
                }
                Log.e("newToken",newToken);

            }
        });




    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                showHome(true);
                break;
            case R.id.messages:
                showOrders(true);
                break;
            case R.id.profile:
                showFavourite(true);
                break;
            case R.id.more:
                showMenu(true);
                break;
        }
        return true;
    }
    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mMoreFragment);
//        transaction.hide(mOrdersFragment);
//        transaction.hide(mFavouriteFragment);
        transaction.replace(R.id.home_fragment_container,mHomeFragment);
        transaction.commit();
        selectedTab = R.id.home;



    }
    private void showMenu(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mOrdersFragment);
//        transaction.hide(mFavouriteFragment);
        transaction.replace(R.id.home_fragment_container,mMoreFragment);
        transaction.commit();
        selectedTab = R.id.more;



    }
    private void showOrders(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mMoreFragment);
//        transaction.hide(mFavouriteFragment);
        transaction.replace(R.id.home_fragment_container,mMessagesFragment);
        transaction.commit();
        selectedTab = R.id.messages;



    }
    public void showFavourite(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mMoreFragment);
//        transaction.hide(mOrdersFragment);
        transaction.replace(R.id.home_fragment_container,mProfileFragment);
        transaction.commit();
        selectedTab = R.id.profile;

    }
}
