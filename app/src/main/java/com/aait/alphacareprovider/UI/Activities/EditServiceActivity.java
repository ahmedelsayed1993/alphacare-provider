package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.ServiceResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.aait.alphacareprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class EditServiceActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks{
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.price)
            EditText price;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.desc)
            EditText desc;
    String id;
    String newToken = null;
    String ImageBasePath = null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( EditServiceActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
        getService();

    }
    @OnClick(R.id.image)
    void onProfile(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_service;
    }
    private void getService(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getService(mLanguagePrefManager.getAppLanguage(),id).enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){

                        name.setText(response.body().getData().getTitle());
                        price.setText(response.body().getData().getPrice());
                        desc.setText(response.body().getData().getDesc());

                        Glide.with(mContext).load(response.body().getData().getImage()).apply(new RequestOptions().error(R.mipmap.logo)).into(image);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.cancel)
    void onCancel(){
        EditServiceActivity.this.finish();
    }
    private void editwithImageAr(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, EditServiceActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).editServicewithImageAr(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id()
        ,newToken,id,name.getText().toString(),desc.getText().toString(),price.getText().toString(),null,filePart).enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                hideProgressDialog();
                if(response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        EditServiceActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    private void editwithImageEn(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, EditServiceActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).editServicewithImageEn(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id()
                ,newToken,id,name.getText().toString(),desc.getText().toString(),price.getText().toString(),null,filePart).enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                hideProgressDialog();
                if(response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        EditServiceActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void editwithoutImageAr(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).editServicewithoutImageAr(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id()
                ,newToken,id,name.getText().toString(),desc.getText().toString(),price.getText().toString(),null).enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                hideProgressDialog();
                if(response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        EditServiceActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void editwithoutImageEn(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).editServicewithoutImageEn(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id()
                ,newToken,id,name.getText().toString(),desc.getText().toString(),price.getText().toString(),null).enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                hideProgressDialog();
                if(response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        EditServiceActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    @OnClick(R.id.add)
    void onAdd(){
        if (mLanguagePrefManager.getAppLanguage().equals("ar")){
            if (ImageBasePath!=null){
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,price,getString(R.string.price))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,desc,getString(R.string.service_desc))){
                    return;
                }else {
                    editwithImageAr(ImageBasePath);
                }
            }else {
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,price,getString(R.string.price))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,desc,getString(R.string.service_desc))){
                    return;
                }else {
                    editwithoutImageAr();
                }
            }
        }else if (mLanguagePrefManager.getAppLanguage().equals("en")){
            if (ImageBasePath!=null){
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,price,getString(R.string.price))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,desc,getString(R.string.service_desc))){
                    return;
                }else {
                    editwithImageEn(ImageBasePath);
                }
            }else {
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,price,getString(R.string.price))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,desc,getString(R.string.service_desc))){
                    return;
                }else {
                    editwithoutImageEn();
                }
            }
        }
    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath = returnValue.get(0);
//                MultipartBody.Part filePart = null;
//                File ImageFile = new File(ImageBasePath);
//                ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
//                filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);

                Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload)).into(image);
                //profile_image.setImageURI(Uri.parse(ImageBasePath));
            }
        }
    }
}
