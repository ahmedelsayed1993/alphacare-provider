package com.aait.alphacareprovider.UI.Activities;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Listeners.PaginationScrollListener;
import com.aait.alphacareprovider.Models.QuestionModel;
import com.aait.alphacareprovider.Models.QuestionsResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.QuestionsAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionsActivity extends ParentActivity implements OnItemClickListener {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView act_title;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<QuestionModel> questionModels = new ArrayList<>();
    QuestionsAdapter questionsAdapter;

    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.repeated_questions));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        questionsAdapter = new QuestionsAdapter(mContext,questionModels,R.layout.recycler_questions);
        questionsAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(questionsAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                getQuestions();
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                questionModels.clear();
                isLastPage = false;
                isLoading = false;
                getQuestions();
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        currentPage = 1;
        questionModels.clear();
        isLastPage = false;
        isLoading = false;
        getQuestions();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_questions;
    }
    private void getQuestions(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getQuestions(mLanguagePrefManager.getAppLanguage(),currentPage).enqueue(new Callback<QuestionsResponse>() {
            @Override
            public void onResponse(Call<QuestionsResponse> call, Response<QuestionsResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getData().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getData().isEmpty()) {
                                questionsAdapter.updateAll( response.body().getData().getData());
                            } else {
                                questionsAdapter.InsertAll( response.body().getData().getData());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getData().isEmpty()) {
                                questionsAdapter.updateAll( response.body().getData().getData());
                                isLastPage = true;
                            } else {
                                questionsAdapter.InsertAll( response.body().getData().getData());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
