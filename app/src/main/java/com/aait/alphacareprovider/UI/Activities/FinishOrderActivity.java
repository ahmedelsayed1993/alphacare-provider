package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.widget.TextView;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.OrderDetailsModdel;
import com.aait.alphacareprovider.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class FinishOrderActivity extends ParentActivity {
    OrderDetailsModdel orderDetailsModdel;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.order)
    TextView order;
    @Override
    protected void initializeComponents() {
        orderDetailsModdel = (OrderDetailsModdel)getIntent().getSerializableExtra("order");
        name.setText(orderDetailsModdel.getUser_name());
        order.setText(getString(R.string.Congratulations)+orderDetailsModdel.getOrder_id());
        address.setText(orderDetailsModdel.getOrder_latlng_address());
        Glide.with(mContext).load(orderDetailsModdel.getUser_avatar()).apply(new RequestOptions().error(R.mipmap.logo)).into(image);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_finish_order;
    }
    @OnClick(R.id.done)
    void onDone(){
        Intent intent = new Intent(mContext,MainActivity.class);
        intent.putExtra("type","normal");
        startActivity(intent);
        FinishOrderActivity.this.finish();
    }
}
