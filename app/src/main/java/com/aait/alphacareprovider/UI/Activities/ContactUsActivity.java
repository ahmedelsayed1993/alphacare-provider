package com.aait.alphacareprovider.UI.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareprovider.App.Constant;
import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.ListModel;
import com.aait.alphacareprovider.Models.ListModle;
import com.aait.alphacareprovider.Models.ListResonse;
import com.aait.alphacareprovider.Models.SiteInfoResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.TypesAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.email)
    TextView email;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.types)
    RecyclerView types;
    @BindView(R.id.contact_type)
    TextView contact_type;
    LinearLayoutManager linearLayoutManager;
    ArrayList<ListModle> listModels = new ArrayList<>();
    TypesAdapter typesAdapter;
    ListModle listModel;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone_num)
    EditText phone_num;
    @BindView(R.id.message)
    EditText message;
    String newToken= null;
    String number = "";
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.contact_us));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        typesAdapter = new TypesAdapter(mContext,listModels,R.layout.list_row);
        typesAdapter.setOnItemClickListener(this);
        types.setLayoutManager(linearLayoutManager);
        types.setAdapter(typesAdapter);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( ContactUsActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
        getTypes();
         getInfo();
    }
    @OnClick(R.id.contact_type)
    void onCotact(){
        types.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,phone_num,getString(R.string.phone))||
                CommonUtil.checkLength(phone_num,getString(R.string.phone_length),9)||
        CommonUtil.checkTextError(contact_type,getString(R.string.contact_type))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,message,getString(R.string.your_message))){
            return;
        }else {
            sendMessage();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact_us;
    }
    private void getInfo(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getInfo(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<SiteInfoResponse>() {
            @Override
            public void onResponse(Call<SiteInfoResponse> call, Response<SiteInfoResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Glide.with(mContext).load(response.body().getData().getContact_image()).apply(new RequestOptions().placeholder(R.mipmap.logo)).into(image);
                        email.setText(response.body().getData().getSite_email());
                        phone.setText(response.body().getData().getSite_phone());
                        number = response.body().getData().getSite_phone();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SiteInfoResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getTypes(){
        RetroWeb.getClient().create(ServiceApi.class).getTypes(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResonse>() {
            @Override
            public void onResponse(Call<ListResonse> call, Response<ListResonse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()!=0){
                            typesAdapter.updateAll(response.body().getData());
                        }else {

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResonse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
            }
        });
    }
    private void sendMessage(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).sendMessage(mLanguagePrefManager.getAppLanguage(),0,mSharedPrefManager.getUserData().getToken_id()
        ,newToken,name.getText().toString(),phone_num.getText().toString(),listModel.getId(),message.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,MainActivity.class);
                        intent.putExtra("type","normal");
                        startActivity(intent);
                        ContactUsActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                 CommonUtil.handleException(mContext,t);
                 t.printStackTrace();
                 hideProgressDialog();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {


            Log.e("type",new Gson().toJson(listModels.get(position)));
            listModel = listModels.get(position);
            contact_type.setText(listModels.get(position).getName());
            types.setVisibility(View.GONE);


    }

    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        mContext.startActivity(intent);
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions((Activity)mContext,PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }
    @OnClick(R.id.phone)
    void onCall(){

        if (number.equals("")) {
        }else {
            getLocationWithPermission(number);
        }

    }
}
