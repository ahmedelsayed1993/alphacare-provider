package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.AddressesModel;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Views.DailogDeleteAddress;
import com.aait.alphacareprovider.UI.Views.DialogEditAddress;

import java.util.List;

import butterknife.BindView;

public class AddressesAdapter extends ParentRecyclerAdapter<AddressesModel> {
    String token;
    public AddressesAdapter(Context context, List<AddressesModel> data, int layoutId, String token) {
        super(context, data, layoutId);
        this.token = token;
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        AddressesAdapter.ViewHolder holder = new AddressesAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final AddressesAdapter.ViewHolder viewHolder = (AddressesAdapter.ViewHolder) holder;
        final AddressesModel addressesModel = data.get(position);
        viewHolder.title.setText(addressesModel.getName());
        viewHolder.address.setText(addressesModel.getLatlng_address());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemClickListener.onItemClick(view,position);
                PopupMenu popupMenu = new PopupMenu(mcontext, viewHolder.more);

                popupMenu.inflate(R.menu.address);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.edit:
                              DialogEditAddress  dialogEditAddress = new DialogEditAddress(mcontext,token,addressesModel);
                                dialogEditAddress.show();
                                return true;
                            case R.id.delete:
                               DailogDeleteAddress dailogDeleteAddress = new DailogDeleteAddress(mcontext,token,addressesModel);
                                dailogDeleteAddress.show();
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.show();



            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.more)
        ImageView more;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
