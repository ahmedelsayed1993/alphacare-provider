package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Fragments.CommentsFragment;

import com.aait.alphacareprovider.UI.Fragments.OffersFragment;
import com.aait.alphacareprovider.UI.Fragments.ServicesFrgment;


public class SubscribeTapAdapter extends FragmentPagerAdapter {

    private Context context;
    private String token;

    public SubscribeTapAdapter(Context context , FragmentManager fm,String token) {
        super(fm);
        this.context = context;
        this.token = token;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new ServicesFrgment().newInstance(token);
        }else if (position == 1){
            return new OffersFragment().newInstance(token);
        }else {
            return new CommentsFragment().newInstance(token);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return context.getString(R.string.services);
        }else if (position == 1){
            return context.getString(R.string.offers);
        }else {
            return context.getString(R.string.comments);
        }
    }
}
