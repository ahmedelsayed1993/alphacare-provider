package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeetingsActivity extends ParentActivity {
    @OnClick(R.id.back)
            void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    String newToken= null;
    @BindView(R.id.notify)
    SwitchCompat notify;
    @BindView(R.id.arabic)
    RadioButton arabic;
    @BindView(R.id.english)
    RadioButton english;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.settings));
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SeetingsActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
        if (mSharedPrefManager.getUserData().getNotification_status().equals("1")){
            notify.setChecked(true);
        }else {
            notify.setChecked(false);
        }
        if(mLanguagePrefManager.getAppLanguage().equals("ar")){
            arabic.setChecked(true);
        }else {
            english.setChecked(true);
        }
    }

    @OnClick(R.id.notify)
    void onNotify(){
        if (mSharedPrefManager.getUserData().getNotification_status().equals("1")){
            changenotify(0);
        }else {
            changenotify(1);
        }
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }
    @OnClick(R.id.edit_data)
    void onEdit(){
        startActivity(new Intent(mContext,ProfileActivity.class));
    }

    @OnClick(R.id.english)
    void onEnglish(){
        changeLang("en");
    }
    @OnClick(R.id.arabic)
    void onArabic(){
        changeLang("ar");
    }
    @OnClick(R.id.logout)
    void onLogout(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).LogOut(1,mSharedPrefManager.getUserData().getToken_id(),newToken).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        mSharedPrefManager.Logout();
                        mSharedPrefManager.setLoginStatus(false);
                        startActivity(new Intent(mContext,SplashActivity.class));
                        SeetingsActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void changeLang(String lang){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeLang(1,mSharedPrefManager.getUserData().getToken_id(),newToken,lang).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        mLanguagePrefManager.setAppLanguage(lang);
                        startActivity(new Intent(mContext,SplashActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    private void changenotify(int notif){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).status(1,mSharedPrefManager.getUserData().getToken_id(),newToken,notif,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        mSharedPrefManager.setUserData(response.body().getData());
                        if (mSharedPrefManager.getUserData().getNotification_status().equals("1")){
                            notify.setChecked(true);
                        }else {
                            notify.setChecked(false);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
