package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Models.AddressesModel;
import com.aait.alphacareprovider.Models.AddressesResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailogDeleteAddress extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    String token;
    AddressesModel addressesModel;
    public DailogDeleteAddress(@NonNull Context context, String token, AddressesModel addressesModel) {
        super(context);
        this.mContext = context;
        this.token = token;
        this.addressesModel = addressesModel;

    }

    @BindView(R.id.address)
    TextView address;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_delete_address);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        address.setText(mContext.getString(R.string.are_you_sure)+("'"+addressesModel.getName()+"'")+addressesModel.getLatlng_address());


    }

    @OnClick(R.id.save)
    void onSave(){

            RetroWeb.getClient().create(ServiceApi.class).deleteAddress(languagePrefManager.getAppLanguage(), 1, sharedPreferences.getUserData().getToken_id()
                    , token, addressesModel.getId()).enqueue(new Callback<AddressesResponse>() {
                @Override
                public void onResponse(Call<AddressesResponse> call, Response<AddressesResponse> response) {
                    DailogDeleteAddress.this.cancel();
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        } else {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddressesResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    DailogDeleteAddress.this.cancel();

                }
            });

    }
    @OnClick(R.id.cancel)
    void onCancel(){
        DailogDeleteAddress.this.cancel();
    }
}
