package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.ListModel;
import com.aait.alphacareprovider.Models.ListResponse;
import com.aait.alphacareprovider.Models.UserModel;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Views.ListDialog;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.aait.alphacareprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ProfileActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks , OnItemClickListener {
    String newToken = null;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.avatar)
    CircleImageView avatar;
    @BindView(R.id.personal_info)
    TextView personal_info;
    @BindView(R.id.personal_lay)
    LinearLayout personal_lay;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.id_number)
    EditText id_number;
    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;
    @BindView(R.id.edit_password)
    TextView edit_password;
    @BindView(R.id.change_pass)
    LinearLayout change_pass;
    @BindView(R.id.Professional_information)
    TextView Professional_information;
    @BindView(R.id.professional_lay)
    LinearLayout professional_lay;
    @BindView(R.id.lay1)
    LinearLayout lay1;
    @BindView(R.id.lay2)
    LinearLayout lay2;
    int gender = 1;
    @BindView(R.id.service)
    TextView service;
    @BindView(R.id.facility)
    ImageView facility;
    ListModel listModel ;
    ListDialog listDialog;
    @BindView(R.id.description)
            EditText description;
    @BindView(R.id.description_en)
            EditText description_en;
    @BindView(R.id.professional)
            ImageView professional;
    @BindView(R.id.old_password)
            EditText old_password;
    @BindView(R.id.new_password)
            EditText new_password;
    @BindView(R.id.confirm_new_password)
            EditText confirm_new_password;

    UserModel userModel;
    ArrayList<ListModel> listModels = new ArrayList<>();
    private String ImageBasePath = null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    private String ImageBasePath1 = null;
    ArrayList<String> returnValue1 = new ArrayList<>();
    Options options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    private String ImageBasePath2 = null;
    ArrayList<String> returnValue2 = new ArrayList<>();
    Options options2 = Options.init()
            .setRequestCode(300)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue2)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.edit_data));
        personal_lay.setVisibility(View.GONE);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( ProfileActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                getProfile();
                Log.e("newToken",newToken);

            }
        });
    }
    @OnClick(R.id.personal_info)
    void onPersonal(){
        if (personal_lay.getVisibility()==View.VISIBLE){
            personal_lay.setVisibility(View.GONE);
        }else if (personal_lay.getVisibility()==View.GONE){
            personal_lay.setVisibility(View.VISIBLE);
        }
    }
    @OnClick(R.id.Professional_information)
    void onProfessional(){
        if (professional_lay.getVisibility()==View.VISIBLE){
            professional_lay.setVisibility(View.GONE);
        }else if (professional_lay.getVisibility()==View.GONE){
            professional_lay.setVisibility(View.VISIBLE);
        }
    }
    @OnClick(R.id.edit_password)
    void onEditPass(){
        if (change_pass.getVisibility()==View.VISIBLE){
            change_pass.setVisibility(View.GONE);
        }else if (change_pass.getVisibility()==View.GONE){
            change_pass.setVisibility(View.VISIBLE);
        }
    }
    @OnClick(R.id.male)
    void onMale(){
        gender = 1;
    }
    @OnClick(R.id.female)
    void onFemale(){
        gender = 2;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }
    void setData(UserModel userModel){
        Glide.with(mContext).load(userModel.getAvatar()).apply(new RequestOptions().error(R.mipmap.logo)).into(avatar);
        name.setText(userModel.getName());
        phone.setText(userModel.getPhone());
        email.setText(userModel.getEmail());
        id_number.setText(userModel.getNational_id());
        gender = Integer.parseInt(userModel.getGender());
        if (userModel.getGender().equals("1")){
            male.setChecked(true);
        }else {
            female.setChecked(true);
        }
        if (userModel.getProvider_type().equals("1")){
            lay1.setVisibility(View.GONE);
            lay2.setVisibility(View.VISIBLE);
        }else if (userModel.getProvider_type().equals("2")){
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.GONE);
        }
    }
    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getToken_id(),newToken,1).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        setData(response.body().getData());
                        userModel = response.body().getData();
                        Log.e("user",new Gson().toJson(response.body().getData()));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void updatePersionalInfo(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateWithoutPassAvatar(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id()
        ,newToken,name.getText().toString(),phone.getText().toString(),email.getText().toString(),gender,id_number.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        setData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void updateProfessional(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("lisence_copy", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).updateProfessional_lab(mLanguagePrefManager.getAppLanguage(),1
        ,mSharedPrefManager.getUserData().getToken_id(),newToken,name.getText().toString(),phone.getText().toString(),email.getText().toString()
        ,gender,id_number.getText().toString(),listModel.getId(),description.getText().toString(),description_en.getText().toString(),filePart).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.body().getKey().equals("1")){
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                    setData(response.body().getData());
                    mSharedPrefManager.setUserData(response.body().getData());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void updateProfessional_lab(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("practice_copy", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).updateProfessional_pro(mLanguagePrefManager.getAppLanguage(),1
                ,mSharedPrefManager.getUserData().getToken_id(),newToken,name.getText().toString(),phone.getText().toString(),email.getText().toString()
                ,gender,id_number.getText().toString(),listModel.getId(),description.getText().toString(),description_en.getText().toString(),filePart).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.body().getKey().equals("1")){
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                    setData(response.body().getData());
                    mSharedPrefManager.setUserData(response.body().getData());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    private void updateProfessional_with(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("lisence_copy", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).updateProfessional_lab_with(mLanguagePrefManager.getAppLanguage(),1
                ,mSharedPrefManager.getUserData().getToken_id(),newToken,name.getText().toString(),phone.getText().toString(),email.getText().toString()
                ,gender,id_number.getText().toString(),listModel.getId(),description.getText().toString(),description_en.getText().toString(),filePart
                ,old_password.getText().toString(),new_password.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.body().getKey().equals("1")){
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                    setData(response.body().getData());
                    mSharedPrefManager.setUserData(response.body().getData());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void update_with(){
        showProgressDialog(getString(R.string.please_wait));

        RetroWeb.getClient().create(ServiceApi.class).update_with(mLanguagePrefManager.getAppLanguage(),1
                ,mSharedPrefManager.getUserData().getToken_id(),newToken,name.getText().toString(),phone.getText().toString(),email.getText().toString()
                ,gender,id_number.getText().toString()
                ,old_password.getText().toString(),new_password.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.body().getKey().equals("1")){
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                    setData(response.body().getData());
                    mSharedPrefManager.setUserData(response.body().getData());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void updateProfessional_lab_with(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("practice_copy", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).updateProfessional_pro_with(mLanguagePrefManager.getAppLanguage(),1
                ,mSharedPrefManager.getUserData().getToken_id(),newToken,name.getText().toString(),phone.getText().toString(),email.getText().toString()
                ,gender,id_number.getText().toString(),listModel.getId(),description.getText().toString(),description_en.getText().toString(),filePart
        ,old_password.getText().toString(),new_password.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.body().getKey().equals("1")){
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                    setData(response.body().getData());
                    mSharedPrefManager.setUserData(response.body().getData());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }
    public void getPickImageWithPermission1() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options1);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options1);
        }
    }
    public void getPickImageWithPermission2() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options2);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options2);
        }
    }
    @OnClick(R.id.professional)
    void onProfession(){
        getPickImageWithPermission2();
    }
    @OnClick(R.id.facility)
    void onFacility(){
        getPickImageWithPermission1();
    }

    @OnClick(R.id.avatar)
    void onAvatar(){
        getPickImageWithPermission();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath = returnValue.get(0);
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                avatar.setImageURI(Uri.parse(ImageBasePath));
                if (ImageBasePath!=null) {
                    updateImage(ImageBasePath);
                }
            }else if (requestCode == 200){
                returnValue1 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath1 = returnValue1.get(0);
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                facility.setImageURI(Uri.parse(ImageBasePath1));
            }else if (requestCode == 300){
                returnValue2 = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath2 = returnValue2.get(0);
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                professional.setImageURI(Uri.parse(ImageBasePath2));
            }
        }
    }

    @OnClick(R.id.service)
    void onService(){
        getSections();
    }
    private void getSections(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getSections(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        listModels = response.body().getData();
                        listDialog = new ListDialog(mContext,ProfileActivity.this,listModels,getString(R.string.services));
                        listDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void updateImage(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).update(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id(),newToken,filePart).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        setData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.add)
    void onAdd(){
        if (professional_lay.getVisibility()==View.GONE&&change_pass.getVisibility()==View.GONE){
            if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
            CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
            CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
            CommonUtil.checkTextError((AppCompatActivity)mContext,id_number,getString(R.string.id_number))||
            CommonUtil.cheID(id_number,getString(R.string.id_number_validation))){
                return;
            }else {
                updatePersionalInfo();
            }
        }
        else if (professional_lay.getVisibility()==View.VISIBLE&&change_pass.getVisibility()==View.GONE){
            if (userModel.getProvider_type().equals("2")) {
                if (ImageBasePath1 != null) {

                    if (CommonUtil.checkTextError((AppCompatActivity) mContext, name, getString(R.string.name)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, phone, getString(R.string.phone)) ||
                            CommonUtil.checkLength(phone, getString(R.string.phone_length), 9) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, email, getString(R.string.email)) ||
                            !CommonUtil.isEmailValid(email, getString(R.string.correct_email)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, id_number, getString(R.string.id_number)) ||
                            CommonUtil.cheID(id_number, getString(R.string.id_number_validation)) ||
                            CommonUtil.checkTextError(service, getString(R.string.service)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description, getString(R.string.description_provider)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description_en, getString(R.string.description_provider_en))) {
                        return;
                    } else {

                        updateProfessional(ImageBasePath1);
                    }
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.The_work_permit_for_the_facility));
                }
            }else if (userModel.getProvider_type().equals("1")){
                if (ImageBasePath2 != null) {

                    if (CommonUtil.checkTextError((AppCompatActivity) mContext, name, getString(R.string.name)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, phone, getString(R.string.phone)) ||
                            CommonUtil.checkLength(phone, getString(R.string.phone_length), 9) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, email, getString(R.string.email)) ||
                            !CommonUtil.isEmailValid(email, getString(R.string.correct_email)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, id_number, getString(R.string.id_number)) ||
                            CommonUtil.cheID(id_number, getString(R.string.id_number_validation)) ||
                            CommonUtil.checkTextError(service, getString(R.string.service)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description, getString(R.string.description_provider)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description_en, getString(R.string.description_provider_en))) {
                        return;
                    } else {

                        updateProfessional_lab(ImageBasePath2);
                    }
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.Professional_license));
                }
            }
        }
        else if (professional_lay.getVisibility()==View.VISIBLE&&change_pass.getVisibility()==View.VISIBLE){
            if (userModel.getProvider_type().equals("2")) {
                if (ImageBasePath1 != null) {

                    if (CommonUtil.checkTextError((AppCompatActivity) mContext, name, getString(R.string.name)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, phone, getString(R.string.phone)) ||
                            CommonUtil.checkLength(phone, getString(R.string.phone_length), 9) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, email, getString(R.string.email)) ||
                            !CommonUtil.isEmailValid(email, getString(R.string.correct_email)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, id_number, getString(R.string.id_number)) ||
                            CommonUtil.cheID(id_number, getString(R.string.id_number_validation)) ||
                            CommonUtil.checkTextError(service, getString(R.string.service)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description, getString(R.string.description_provider)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description_en, getString(R.string.description_provider_en))||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,old_password,mContext.getString(R.string.old_password))||
                            CommonUtil.checkLength(old_password,mContext.getString(R.string.password_length),6)||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,new_password,mContext.getString(R.string.new_password))||
                            CommonUtil.checkLength(new_password,mContext.getString(R.string.password_length),6)||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new_password,mContext.getString(R.string.confirm_new_password))) {
                        return;
                    } else {

                        if (new_password.getText().toString().equals(confirm_new_password.getText().toString())){
                            updateProfessional_with(ImageBasePath1);
                        }else {
                            CommonUtil.makeToast(mContext,mContext.getString(R.string.password_not_match));
                        }
                    }
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.The_work_permit_for_the_facility));
                }
            }else if (userModel.getProvider_type().equals("1")){
                if (ImageBasePath2 != null) {

                    if (CommonUtil.checkTextError((AppCompatActivity) mContext, name, getString(R.string.name)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, phone, getString(R.string.phone)) ||
                            CommonUtil.checkLength(phone, getString(R.string.phone_length), 9) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, email, getString(R.string.email)) ||
                            !CommonUtil.isEmailValid(email, getString(R.string.correct_email)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, id_number, getString(R.string.id_number)) ||
                            CommonUtil.cheID(id_number, getString(R.string.id_number_validation)) ||
                            CommonUtil.checkTextError(service, getString(R.string.service)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description, getString(R.string.description_provider)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, description_en, getString(R.string.description_provider_en))||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,old_password,mContext.getString(R.string.old_password))||
                            CommonUtil.checkLength(old_password,mContext.getString(R.string.password_length),6)||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,new_password,mContext.getString(R.string.new_password))||
                            CommonUtil.checkLength(new_password,mContext.getString(R.string.password_length),6)||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new_password,mContext.getString(R.string.confirm_new_password))) {
                        return;
                    } else {
                        if (new_password.getText().toString().equals(confirm_new_password.getText().toString())){
                            updateProfessional_lab_with(ImageBasePath2);
                        }else {
                            CommonUtil.makeToast(mContext,mContext.getString(R.string.password_not_match));
                        }


                    }
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.Professional_license));
                }
            }
        }
        else if (professional_lay.getVisibility()==View.GONE&&change_pass.getVisibility()==View.VISIBLE){


                    if (CommonUtil.checkTextError((AppCompatActivity) mContext, name, getString(R.string.name)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, phone, getString(R.string.phone)) ||
                            CommonUtil.checkLength(phone, getString(R.string.phone_length), 9) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, email, getString(R.string.email)) ||
                            !CommonUtil.isEmailValid(email, getString(R.string.correct_email)) ||
                            CommonUtil.checkTextError((AppCompatActivity) mContext, id_number, getString(R.string.id_number)) ||
                            CommonUtil.cheID(id_number, getString(R.string.id_number_validation)) ||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,old_password,mContext.getString(R.string.old_password))||
                            CommonUtil.checkLength(old_password,mContext.getString(R.string.password_length),6)||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,new_password,mContext.getString(R.string.new_password))||
                            CommonUtil.checkLength(new_password,mContext.getString(R.string.password_length),6)||
                            CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new_password,mContext.getString(R.string.confirm_new_password))) {
                        return;
                    } else {

                        if (new_password.getText().toString().equals(confirm_new_password.getText().toString())){
                            update_with();
                        }else {
                            CommonUtil.makeToast(mContext,mContext.getString(R.string.password_not_match));
                        }
                    }


        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        if (view.getId()==R.id.tv_row_title){
            listModel = listModels.get(position);

            service.setText(listModel.getTitle());
        }
    }
    @OnClick(R.id.cancel)
    void onCancel(){
        onBackPressed();
    }
}
