package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Fragments.FragmentCurrentOrders;
import com.aait.alphacareprovider.UI.Fragments.FragmentFinished;
import com.aait.alphacareprovider.UI.Fragments.FragmentNewOrders;


public class OrdersTapAdapter extends FragmentPagerAdapter {

    private Context context;
    private String token;

    public OrdersTapAdapter(Context context , FragmentManager fm,String token) {
        super(fm);
        this.context = context;
        this.token = token;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new FragmentNewOrders().newInstance(token);
        }else if (position == 1){
            return new FragmentCurrentOrders().newInstance(token);
        }else {
            return new FragmentFinished().newInstance(token);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return context.getString(R.string.pending_approval);
        }else if (position == 1){
            return context.getString(R.string.underway);
        }else {
            return context.getString(R.string.finished);
        }
    }
}
