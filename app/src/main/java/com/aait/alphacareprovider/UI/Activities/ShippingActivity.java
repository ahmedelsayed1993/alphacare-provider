package com.aait.alphacareprovider.UI.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.AccountResponse;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;
import com.aait.alphacareprovider.Uitls.PermissionUtils;
import com.aait.alphacareprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.alphacareprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ShippingActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.account_name)
    EditText account_name;
    @BindView(R.id.account_number)
    EditText account_number;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.bank_name)
            TextView bank_name;
    @BindView(R.id.number)
            TextView number;
    @BindView(R.id.iban)
            TextView iban;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    String ImageBasePath = null;
    String newToken = null;
    Double total;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.Wallet_shipping));
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( ShippingActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);
                getAccount();

            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_shipping_wallet;
    }
    @OnClick(R.id.image)
    void onProfile(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    private void getAccount(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Accounts(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id(),newToken).enqueue(new Callback<AccountResponse>() {
            @Override
            public void onResponse(Call<AccountResponse> call, Response<AccountResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        bank_name.setText(response.body().getAccount().getBank_name());
                        number.setText(response.body().getAccount().getAccount_number());
                        iban.setText(response.body().getAccount().getIban_number());
                        total = response.body().getAccount().getTotal();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AccountResponse> call, Throwable t) {

                CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    private void transfer(String path)
    {
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ShippingActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).Transfer(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id(),newToken,account_name.getText().toString()
        ,account_number.getText().toString(),amount.getText().toString(),filePart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        onBackPressed();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.add)
    void onAdd(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,account_name,getString(R.string.account_name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,account_number,getString(R.string.account_number))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,amount,getString(R.string.amount))){
            return;
        }else {
            if (ImageBasePath!=null){
                if (total > Double.parseDouble(amount.getText().toString()) ){
                    Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_wallet);
                    dialog.getWindow().setGravity(Gravity.CENTER);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    Button cancel;
                    Button save;

                    cancel = dialog.findViewById(R.id.cancel);
                    save = dialog.findViewById(R.id.save);

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                          dialog.dismiss();
                        }
                    });
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            transfer(ImageBasePath);
                        }
                    });


                    dialog.show();
                }else {
                    transfer(ImageBasePath);
                }
            }else {
                CommonUtil.makeToast(mContext,getString(R.string.state_image));
            }
        }
    }
    @OnClick(R.id.cancel)
    void onCancel(){
        onBackPressed();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath = returnValue.get(0);
//                MultipartBody.Part filePart = null;
//                File ImageFile = new File(ImageBasePath);
//                ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
//                filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);

                Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload)).into(image);
                //profile_image.setImageURI(Uri.parse(ImageBasePath));
            }
        }
    }
}
