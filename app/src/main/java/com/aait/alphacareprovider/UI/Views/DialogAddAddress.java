package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.alphacareprovider.App.Constant;
import com.aait.alphacareprovider.Models.AddressesResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.MapDetectLocationActivity;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.core.provider.FontsContractCompat.FontRequestCallback.RESULT_OK;

public class DialogAddAddress extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
   public String mAdresse="", mLang=null, mLat = null, mAddress="",address = "";
   String token;
    public DialogAddAddress(@NonNull Context context, String token) {
        super(context);
        this.mContext = context;
        this.token = token;

    }
    public TextView addresses;
    @BindView(R.id.name)
     EditText name;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_add_address);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        addresses = findViewById(R.id.address);

    }
    @OnClick(R.id.address)
    void onLocation(){

        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    @OnClick(R.id.save)
    void onSave(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,mContext.getString(R.string.name))||
        CommonUtil.checkTextError(addresses,mContext.getString(R.string.address))){
            return;
        }else {
            RetroWeb.getClient().create(ServiceApi.class).addAddrress(languagePrefManager.getAppLanguage(), 1, sharedPreferences.getUserData().getToken_id()
                    , token, name.getText().toString(), mLat, mLang, mAddress, mAdresse).enqueue(new Callback<AddressesResponse>() {
                @Override
                public void onResponse(Call<AddressesResponse> call, Response<AddressesResponse> response) {
                    DialogAddAddress.this.cancel();
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        } else {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddressesResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    DialogAddAddress.this.cancel();

                }
            });
        }
    }
    @OnClick(R.id.cancel)
    void onCancel(){
       DialogAddAddress.this.cancel();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mAddress = data.getStringExtra("LOCATION");
                    address = data.getStringExtra("LOC");
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse+"  "+mAddress+"  "+address);
                    if (languagePrefManager.getAppLanguage().equals("ar")) {
                        addresses.setText(mAddress);
                    }else if (languagePrefManager.getAppLanguage().equals("en")){
                        addresses.setText(mAdresse);
                    }

                }
            }
        }
    }
}
