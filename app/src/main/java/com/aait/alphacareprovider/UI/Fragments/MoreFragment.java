package com.aait.alphacareprovider.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.aait.alphacareprovider.Base.BaseFragment;
import com.aait.alphacareprovider.Models.NotificationCountRespons;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.AboutAppActivity;
import com.aait.alphacareprovider.UI.Activities.ActivateAccountActivity;
import com.aait.alphacareprovider.UI.Activities.ActivityTerms;
import com.aait.alphacareprovider.UI.Activities.AddressesActivity;
import com.aait.alphacareprovider.UI.Activities.ContactUsActivity;
import com.aait.alphacareprovider.UI.Activities.NotificationActivity;
import com.aait.alphacareprovider.UI.Activities.QuestionsActivity;
import com.aait.alphacareprovider.UI.Activities.SeetingsActivity;
import com.aait.alphacareprovider.UI.Activities.WalletActivity;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreFragment extends BaseFragment {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.count)
    TextView count;
    private String newToken= null;
    @BindView(R.id.online)
    SwitchCompat online;
    public static MoreFragment newInstance(String newToken) {
        Bundle args = new Bundle();
        MoreFragment fragment = new MoreFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_more;
    }

    @Override
    protected void initializeComponents(View view) {
         title.setText(getString(R.string.menu));
         online.setVisibility(View.GONE);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                getCount(newToken);
                Log.e("newToken",newToken);

            }
        });
    }
    @OnClick(R.id.wallet)
    void onWallet(){
        startActivity(new Intent(mContext, WalletActivity.class));
    }
    @OnClick(R.id.addresses)
    void onMyAddress(){
        startActivity(new Intent(mContext, AddressesActivity.class));
    }
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext, NotificationActivity.class));
    }
    @OnClick(R.id.about_app)
    void onAboutApp(){
        startActivity(new Intent(mContext, AboutAppActivity.class));
    }
    @OnClick(R.id.repeated_questions)
    void onReoeated(){
        startActivity(new Intent(mContext, QuestionsActivity.class));
    }
    @OnClick(R.id.terms_conditions)
    void onTerms(){
        startActivity(new Intent(mContext, ActivityTerms.class));
    }
    @OnClick(R.id.contact_us)
    void onContact(){
        startActivity(new Intent(mContext, ContactUsActivity.class));
    }
    @OnClick(R.id.share_app)
    void onShare(){
        CommonUtil.ShareApp(mContext);
    }
    @OnClick(R.id.settings)
    void onSettings(){
        startActivity(new Intent(mContext, SeetingsActivity.class));
    }

    private void getCount(String token){
        //  showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCount(mLanguagePrefManager.getAppLanguage(),1,mSharedPrefManager.getUserData().getToken_id(),token).enqueue(new Callback<NotificationCountRespons>() {
            @Override
            public void onResponse(Call<NotificationCountRespons> call, Response<NotificationCountRespons> response) {
                // hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        count.setText(response.body().getData()+"");
                    }else if (response.body().getKey().equals("2")&&response.body().getUser_status().equals("non-active")){

                        Intent intent = new Intent(mContext, ActivateAccountActivity.class);
                        intent.putExtra("data",mSharedPrefManager.getUserData());
                        startActivity(intent);

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationCountRespons> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                // hideProgressDialog();

            }
        });
    }

}
