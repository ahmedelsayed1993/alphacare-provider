package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.CommentsModel;
import com.aait.alphacareprovider.Models.CommissionModel;
import com.aait.alphacareprovider.R;

import java.util.List;

import butterknife.BindView;

public class CommissionAdapter extends ParentRecyclerAdapter<CommissionModel> {
    public CommissionAdapter(Context context, List<CommissionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        CommissionAdapter.ViewHolder holder = new CommissionAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final CommissionAdapter.ViewHolder viewHolder = (CommissionAdapter.ViewHolder) holder;
        final CommissionModel addressesModel = data.get(position);
        viewHolder.process.setText("#"+addressesModel.getOrder_id());
        viewHolder.date.setText(addressesModel.getDate());
        viewHolder.value.setText(addressesModel.getDept()+"-");
    }
    public class ViewHolder extends ParentRecyclerViewHolder {
        @BindView(R.id.process)
        TextView process;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.value)
        TextView value;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
