package com.aait.alphacareprovider.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.alphacareprovider.Base.BaseFragment;
import com.aait.alphacareprovider.Listeners.OnItemClickListener;
import com.aait.alphacareprovider.Models.ProviderServicesResponse;
import com.aait.alphacareprovider.Models.ServiceModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.AddOfferActivity;
import com.aait.alphacareprovider.UI.Activities.AddServiceActivity;
import com.aait.alphacareprovider.UI.Activities.OfferDetailsActivity;
import com.aait.alphacareprovider.UI.Activities.ServiceDetailsActivity;
import com.aait.alphacareprovider.UI.Adapters.OffersAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersFragment extends BaseFragment implements OnItemClickListener {
    String newToken = null;
    @BindView(R.id.lay_no)
    LinearLayout lay_no;
    @BindView(R.id.add)
    ImageView add;
    @BindView(R.id.services)
    RecyclerView services;
    ArrayList<ServiceModel> serviceModels = new ArrayList<>();
    GridLayoutManager linearLayoutManager;
    OffersAdapter offersAdapter;
    public static OffersFragment newInstance(String newToken) {
        Bundle args = new Bundle();
        OffersFragment fragment = new OffersFragment();
        args.putString("token",newToken);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_offers;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        linearLayoutManager = new GridLayoutManager(mContext,4);
        offersAdapter = new OffersAdapter(mContext,serviceModels,R.layout.recycler_offers);
        offersAdapter.setOnItemClickListener(this);
        services.setLayoutManager(linearLayoutManager);
        services.setAdapter(offersAdapter);
        getProvider(newToken);

    }

    @OnClick(R.id.add)
    void onAddService(){
        startActivity(new Intent(mContext, AddOfferActivity.class));
    }
    @OnClick(R.id.add_offer)
    void onAdd(){
        startActivity(new Intent(mContext, AddOfferActivity.class));
    }
    private void getProvider(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProvider(1,mSharedPrefManager.getUserData().getToken_id(),token,"2",mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ProviderServicesResponse>() {
            @Override
            public void onResponse(Call<ProviderServicesResponse> call, Response<ProviderServicesResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().getTab_data().isEmpty()){
                            lay_no.setVisibility(View.VISIBLE);
                            add.setVisibility(View.GONE);
                            services.setVisibility(View.GONE);
                        }else {
                            lay_no.setVisibility(View.GONE);
                            add.setVisibility(View.VISIBLE);
                            services.setVisibility(View.VISIBLE);
                           offersAdapter.updateAll(response.body().getData().getTab_data());
                        }

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderServicesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(mContext, OfferDetailsActivity.class);
        intent.putExtra("id",serviceModels.get(position).getId()+"");
        startActivity(intent);
    }
}
