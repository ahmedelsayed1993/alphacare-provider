package com.aait.alphacareprovider.UI.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.viewpager.widget.ViewPager;

import com.aait.alphacareprovider.Base.BaseFragment;
import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.ProviderServicesModel;
import com.aait.alphacareprovider.Models.ProviderServicesResponse;
import com.aait.alphacareprovider.Models.UserResponse;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Adapters.RecyclerPoupupAds;
import com.aait.alphacareprovider.UI.Adapters.ReportsAdapter;
import com.aait.alphacareprovider.UI.Adapters.SubscribeTapAdapter;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.aait.alphacareprovider.Uitls.DialogUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment {
    @BindView(R.id.title)
    TextView title;
    private String newToken= null;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.titl)
    TextView title1;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.open)
    ImageView open;
    @BindView(R.id.reviews)
    TextView reviews;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RecyclerPoupupAds recyclerPoupupAds;
    ArrayList<String> banners = new ArrayList<>();
    @BindView(R.id.orders)
    TabLayout myOrdersTab;

    @BindView(R.id.ordersViewPager)
    ViewPager myOrdersViewPager;
    private SubscribeTapAdapter mAdapter;
    @BindView(R.id.online)
    SwitchCompat online;
    public static ProfileFragment newInstance(String newToken) {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        args.putString("token",newToken);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile;
    }
    @Override
    public void onResume() {
        super.onResume();
        if(mSharedPrefManager.getUserData().getIs_online().equals("1")){
            online.setChecked(true);
//            online.setActivated(true);
        }else {
            online.setChecked(false);
//            online.setActivated(false);
        }
    }
    @Override
    protected void initializeComponents(View view) {
        online.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.profile));
        Bundle bundle = this.getArguments();
        newToken = bundle.getString("token");
        mAdapter = new SubscribeTapAdapter(mContext,getChildFragmentManager(),newToken);
        myOrdersViewPager.setAdapter(mAdapter);
        myOrdersTab.setupWithViewPager(myOrdersViewPager);
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(Objects.requireNonNull(getActivity()),  new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                newToken = instanceIdResult.getToken();
                getProvider(newToken);
                Log.e("newToken",newToken);

//            }
//        });
        if(mSharedPrefManager.getUserData().getIs_online().equals("1")){
            online.setChecked(true);
//            online.setActivated(true);
        }else {
            online.setChecked(false);
//            online.setActivated(false);
        }

    }
    void setData(ProviderServicesModel providerServicesModel){
        banners = providerServicesModel.getProvider().getImages();
        recyclerPoupupAds = new RecyclerPoupupAds(mContext,banners);
        viewPager.setAdapter(recyclerPoupupAds);

//                        indicator.setViewPager(viewPager);


        NUM_PAGES = recyclerPoupupAds.getCount();
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == NUM_PAGES){ currentPage = 0; }
                viewPager.setCurrentItem(currentPage,true);
                currentPage++;

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);


            }
        }, 2500,2500);


        name.setText(providerServicesModel.getProvider().getName());
        rating.setRating(Float.parseFloat(providerServicesModel.getProvider().getFinal_rate()));
        title1.setText(providerServicesModel.getProvider().getSection());
        description.setText(providerServicesModel.getProvider().getDesc());
        reviews.setText(mContext.getResources().getString(R.string.number_of_reviews)+" "+providerServicesModel.getProvider().getVisitors());
        if (providerServicesModel.getProvider().getIs_online().equals("0")){
            open.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.red));
        }else {
            open.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.green));
        }
    }
    private void getProvider(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProvider(1,mSharedPrefManager.getUserData().getToken_id(),token,null,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ProviderServicesResponse>() {
            @Override
            public void onResponse(Call<ProviderServicesResponse> call, Response<ProviderServicesResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        setData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderServicesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.online)
    void onLine(){
        if (mSharedPrefManager.getUserData().getIs_online().equals("1")){
            active("0");
        }else {
            active("1");
        }
    }
    private void active(String status){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).active(1,mSharedPrefManager.getUserData().getToken_id(),newToken,status,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        mSharedPrefManager.setUserData(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}
