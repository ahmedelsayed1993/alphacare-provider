package com.aait.alphacareprovider.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Activities.CurrentOrderActivity;
import com.aait.alphacareprovider.UI.Activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FollowDialog extends Dialog {
    Context mContext;
    int order_id;
    @BindView(R.id.order)
    TextView order;
    public FollowDialog(@NonNull Context context,int order_id) {
        super(context);
        this.mContext=context;
        this.order_id = order_id;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_follow_order);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);

        initializeComponents();
    }
    private void initializeComponents() {
        order.setText(mContext.getString(R.string.accept_order)+" "+order_id+" #");


    }
    @OnClick(R.id.later)
    void onLater(){
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.putExtra("type","normal");
        mContext.startActivity(intent);
        FollowDialog.this.dismiss();
    }
    @OnClick(R.id.follow)
    void onFollow(){
        Intent intent =new Intent(mContext, CurrentOrderActivity.class);
        intent.putExtra("id",order_id+"");
        mContext.startActivity(intent);
        FollowDialog.this.dismiss();
    }
}
