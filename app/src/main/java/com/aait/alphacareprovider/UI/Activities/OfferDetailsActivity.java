package com.aait.alphacareprovider.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.PopupMenu;

import com.aait.alphacareprovider.Base.ParentActivity;
import com.aait.alphacareprovider.Models.ServiceResponse;
import com.aait.alphacareprovider.Models.ServicesModel;
import com.aait.alphacareprovider.Network.RetroWeb;
import com.aait.alphacareprovider.Network.ServiceApi;
import com.aait.alphacareprovider.R;
import com.aait.alphacareprovider.UI.Views.DialogDeleteOffer;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferDetailsActivity extends ParentActivity {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.advantages)
    TextView advantages;
    @BindView(R.id.more)
    ImageView more;
    String id;
    ServicesModel servicesModel;
    String newToken = null;
    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( OfferDetailsActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
        getService();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_offer_details;
    }
    @OnClick(R.id.more)
    void onMore(){

        PopupMenu popupMenu = new PopupMenu(mContext, more);

        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.service, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.edit:
                        Intent intent  = new Intent(mContext,EditOfferActivity.class);
                        intent.putExtra("id",id);
                        startActivity(intent);
                        return true;
                    case R.id.delete:
                        new DialogDeleteOffer(mContext,newToken,servicesModel).show();
                        return true;
                }
                return false;
            }
        });
        popupMenu.show();
    }
    private void getService(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getService(mLanguagePrefManager.getAppLanguage(),id).enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        servicesModel = response.body().getData();
                        name.setText(response.body().getData().getTitle());
                        price.setText(response.body().getData().getPrice()+mContext.getResources().getString(R.string.sar));
                        details.setText(response.body().getData().getDesc());
                        advantages.setText(response.body().getData().getAdv());
                        Glide.with(mContext).load(response.body().getData().getImage()).apply(new RequestOptions().error(R.mipmap.logo)).into(image);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
