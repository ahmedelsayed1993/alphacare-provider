package com.aait.alphacareprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.alphacareprovider.Base.ParentRecyclerAdapter;
import com.aait.alphacareprovider.Base.ParentRecyclerViewHolder;
import com.aait.alphacareprovider.Models.QuestionModel;
import com.aait.alphacareprovider.R;

import java.util.List;

import butterknife.BindView;

public class QuestionsAdapter extends ParentRecyclerAdapter<QuestionModel> {
    int count = 0;
    public QuestionsAdapter(Context context, List<QuestionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        QuestionsAdapter.ViewHolder holder = new QuestionsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final QuestionsAdapter.ViewHolder viewHolder = (QuestionsAdapter.ViewHolder) holder;
        final QuestionModel questionModel = data.get(position);
        viewHolder.ask.setText(questionModel.getAsk());
        viewHolder.answer.setText(questionModel.getAnswer());
        viewHolder.show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                itemClickListener.onItemClick(view,position);
//                PopupMenu popupMenu = new PopupMenu(mcontext,viewHolder.show);
//
//                popupMenu.inflate(R.menu.address);
//
//                popupMenu.show();
                count++;
                itemClickListener.onItemClick(view,position);
                if (count%2==1){
                    viewHolder.answer.setVisibility(View.VISIBLE);


                }else {
                    viewHolder.answer.setVisibility(View.GONE);

                }

            }
        });
    }

    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.ask)
        TextView ask;
        @BindView(R.id.answer)
        TextView answer;
        @BindView(R.id.show)
        ImageView show;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
