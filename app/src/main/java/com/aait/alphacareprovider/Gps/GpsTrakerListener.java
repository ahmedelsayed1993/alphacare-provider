package com.aait.alphacareprovider.Gps;

public interface GpsTrakerListener {
    void onTrackerSuccess(Double lat, Double log);

    void onStartTracker();
}
