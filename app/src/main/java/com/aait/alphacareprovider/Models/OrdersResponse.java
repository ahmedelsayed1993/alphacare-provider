package com.aait.alphacareprovider.Models;

public class OrdersResponse extends BaseResponse {
    private OrderResponse data;

    public OrderResponse getData() {
        return data;
    }

    public void setData(OrderResponse data) {
        this.data = data;
    }
}
