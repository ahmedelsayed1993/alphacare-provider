package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationsModel implements Serializable {
    private ArrayList<NotificationModel> data;
    private PaginationModel pagination;

    public ArrayList<NotificationModel> getData() {
        return data;
    }

    public void setData(ArrayList<NotificationModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
