package com.aait.alphacareprovider.Models;

public class SettlementResponse extends BaseResponse {
    private SettlementModel data;

    public SettlementModel getData() {
        return data;
    }

    public void setData(SettlementModel data) {
        this.data = data;
    }
}
