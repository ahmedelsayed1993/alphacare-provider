package com.aait.alphacareprovider.Models;

public class MessageResponse extends BaseResponse {
    private MessageModel data;

    public MessageModel getData() {
        return data;
    }

    public void setData(MessageModel data) {
        this.data = data;
    }
}
