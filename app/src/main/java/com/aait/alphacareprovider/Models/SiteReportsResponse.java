package com.aait.alphacareprovider.Models;

import java.util.ArrayList;

public class SiteReportsResponse extends BaseResponse{
   private ArrayList<SiteReportsModel> data;

    public ArrayList<SiteReportsModel> getData() {
        return data;
    }

    public void setData(ArrayList<SiteReportsModel> data) {
        this.data = data;
    }
}
