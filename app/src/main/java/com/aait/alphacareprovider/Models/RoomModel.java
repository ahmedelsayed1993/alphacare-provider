package com.aait.alphacareprovider.Models;

import java.io.Serializable;

public class RoomModel implements Serializable {
    private int room_id;
    private String  other_id;
    private String other_name;
    private String other_image;
    private String  last_message_id;
    private String last_message_body;
    private String last_message_type;
    private String last_message_seen;
    private int count_unseen;
    private int total_pages;

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public String getOther_id() {
        return other_id;
    }

    public void setOther_id(String other_id) {
        this.other_id = other_id;
    }

    public String getOther_name() {
        return other_name;
    }

    public void setOther_name(String other_name) {
        this.other_name = other_name;
    }

    public String getOther_image() {
        return other_image;
    }

    public void setOther_image(String other_image) {
        this.other_image = other_image;
    }

    public String getLast_message_id() {
        return last_message_id;
    }

    public void setLast_message_id(String  last_message_id) {
        this.last_message_id = last_message_id;
    }

    public String getLast_message_body() {
        return last_message_body;
    }

    public void setLast_message_body(String last_message_body) {
        this.last_message_body = last_message_body;
    }

    public String getLast_message_type() {
        return last_message_type;
    }

    public void setLast_message_type(String last_message_type) {
        this.last_message_type = last_message_type;
    }

    public String getLast_message_seen() {
        return last_message_seen;
    }

    public void setLast_message_seen(String last_message_seen) {
        this.last_message_seen = last_message_seen;
    }

    public int getCount_unseen() {
        return count_unseen;
    }

    public void setCount_unseen(int count_unseen) {
        this.count_unseen = count_unseen;
    }
}
