package com.aait.alphacareprovider.Models;

public class ChatResponse extends BaseResponse {
    private ChatModel data;

    public ChatModel getData() {
        return data;
    }

    public void setData(ChatModel data) {
        this.data = data;
    }
}
