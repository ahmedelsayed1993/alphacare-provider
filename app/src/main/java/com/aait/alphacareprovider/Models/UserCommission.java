package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

 public class UserCommission implements Serializable {
    private ArrayList<CommissionModel> data;
    private PaginationModel pagination;

    public ArrayList<CommissionModel> getData() {
        return data;
    }

    public void setData(ArrayList<CommissionModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
