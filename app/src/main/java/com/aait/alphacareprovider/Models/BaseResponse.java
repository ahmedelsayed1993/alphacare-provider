package com.aait.alphacareprovider.Models;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    private String key;
    private String msg;
    private String user_status;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }
    public boolean issucessfull(){
        if (key.equals("1")){
            return true;
        }else {
            return false;
        }
    }
}
