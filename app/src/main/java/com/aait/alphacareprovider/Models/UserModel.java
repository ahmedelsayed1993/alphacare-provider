package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class UserModel implements Serializable {
    private int id;
    private String name;
    private String phone;
    private String email;
    private String provider_type;
    private String provider_type_name;
    private String gender;
    private String gender_name;
    private String experience;
    private String experience_name;
    private String section_id;
    private String section;
    private String city_id;
    private String city_name;
    private String desc;
    private String desc_ar;
    private String desc_en;
    private String national_id;
    private String national_copy;
    private String user_type;
    private String is_active;
    private String is_online;
    private String mobile_activation;
    private String code;
    private String lang;
    private String count_rate;
    private String final_rate;
    private String notification_status;
    private String avatar;
    private String token_id;
    private String practice_copy;
    private String card_copy;
    private String degree_copy;
    private String lisence_copy;
    private String papers_pdf;
    private ArrayList<BanksModel> banks;
    private ArrayList<AddressesModel> addresses;
    private int count_services;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender_name() {
        return gender_name;
    }

    public void setGender_name(String gender_name) {
        this.gender_name = gender_name;
    }

    public String getNational_id() {
        return national_id;
    }

    public void setNational_id(String national_id) {
        this.national_id = national_id;
    }

    public String getNational_copy() {
        return national_copy;
    }

    public void setNational_copy(String national_copy) {
        this.national_copy = national_copy;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getIs_online() {
        return is_online;
    }

    public void setIs_online(String is_online) {
        this.is_online = is_online;
    }

    public String getMobile_activation() {
        return mobile_activation;
    }

    public void setMobile_activation(String mobile_activation) {
        this.mobile_activation = mobile_activation;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCount_rate() {
        return count_rate;
    }

    public void setCount_rate(String count_rate) {
        this.count_rate = count_rate;
    }

    public String getFinal_rate() {
        return final_rate;
    }

    public void setFinal_rate(String final_rate) {
        this.final_rate = final_rate;
    }

    public String getNotification_status() {
        return notification_status;
    }

    public void setNotification_status(String notification_status) {
        this.notification_status = notification_status;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getProvider_type() {
        return provider_type;
    }

    public void setProvider_type(String provider_type) {
        this.provider_type = provider_type;
    }

    public String getProvider_type_name() {
        return provider_type_name;
    }

    public void setProvider_type_name(String provider_type_name) {
        this.provider_type_name = provider_type_name;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getExperience_name() {
        return experience_name;
    }

    public void setExperience_name(String experience_name) {
        this.experience_name = experience_name;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc_ar() {
        return desc_ar;
    }

    public void setDesc_ar(String desc_ar) {
        this.desc_ar = desc_ar;
    }

    public String getDesc_en() {
        return desc_en;
    }

    public void setDesc_en(String desc_en) {
        this.desc_en = desc_en;
    }

    public String getPractice_copy() {
        return practice_copy;
    }

    public void setPractice_copy(String practice_copy) {
        this.practice_copy = practice_copy;
    }

    public String getCard_copy() {
        return card_copy;
    }

    public void setCard_copy(String card_copy) {
        this.card_copy = card_copy;
    }

    public String getDegree_copy() {
        return degree_copy;
    }

    public void setDegree_copy(String degree_copy) {
        this.degree_copy = degree_copy;
    }

    public String getLisence_copy() {
        return lisence_copy;
    }

    public void setLisence_copy(String lisence_copy) {
        this.lisence_copy = lisence_copy;
    }

    public String getPapers_pdf() {
        return papers_pdf;
    }

    public void setPapers_pdf(String papers_pdf) {
        this.papers_pdf = papers_pdf;
    }

    public ArrayList<BanksModel> getBanks() {
        return banks;
    }

    public void setBanks(ArrayList<BanksModel> banks) {
        this.banks = banks;
    }

    public ArrayList<AddressesModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<AddressesModel> addresses) {
        this.addresses = addresses;
    }

    public int getCount_services() {
        return count_services;
    }

    public void setCount_services(int count_services) {
        this.count_services = count_services;
    }


}
