package com.aait.alphacareprovider.Models;

public class MessagesResponse extends BaseResponse {
  private MessagesModel data;

    public MessagesModel getData() {
        return data;
    }

    public void setData(MessagesModel data) {
        this.data = data;
    }
}
