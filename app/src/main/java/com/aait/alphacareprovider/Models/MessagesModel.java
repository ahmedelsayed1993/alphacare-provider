package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class MessagesModel implements Serializable {
    private ArrayList<RoomModel> data;
    private PaginationModel pagination;

    public ArrayList<RoomModel> getData() {
        return data;
    }

    public void setData(ArrayList<RoomModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
