package com.aait.alphacareprovider.Models;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class SettlementModel implements Serializable {
    private int total;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
