package com.aait.alphacareprovider.Models;

import java.io.Serializable;

public class BanksModel implements Serializable {
    private int id;
    private String bank_name;
    private String bank_num;
    private String bank_iban;
    private String bank_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_num() {
        return bank_num;
    }

    public void setBank_num(String bank_num) {
        this.bank_num = bank_num;
    }

    public String getBank_iban() {
        return bank_iban;
    }

    public void setBank_iban(String bank_iban) {
        this.bank_iban = bank_iban;
    }

    public String getBank_image() {
        return bank_image;
    }

    public void setBank_image(String bank_image) {
        this.bank_image = bank_image;
    }
}
