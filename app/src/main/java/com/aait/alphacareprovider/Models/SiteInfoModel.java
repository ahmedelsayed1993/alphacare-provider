package com.aait.alphacareprovider.Models;

import java.io.Serializable;

public class SiteInfoModel implements Serializable {
    private String site_email;
    private String site_phone;
    private String site_condition;
    private String site_about;
    private String contact_image;

    public String getSite_email() {
        return site_email;
    }

    public void setSite_email(String site_email) {
        this.site_email = site_email;
    }

    public String getSite_phone() {
        return site_phone;
    }

    public void setSite_phone(String site_phone) {
        this.site_phone = site_phone;
    }

    public String getSite_condition() {
        return site_condition;
    }

    public void setSite_condition(String site_condition) {
        this.site_condition = site_condition;
    }

    public String getSite_about() {
        return site_about;
    }

    public void setSite_about(String site_about) {
        this.site_about = site_about;
    }

    public String getContact_image() {
        return contact_image;
    }

    public void setContact_image(String contact_image) {
        this.contact_image = contact_image;
    }
}
