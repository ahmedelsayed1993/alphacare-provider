package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderResponse implements Serializable {
    private ArrayList<OrderModel> data;
    private PaginationModel pagination;

    public ArrayList<OrderModel> getData() {
        return data;
    }

    public void setData(ArrayList<OrderModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
