package com.aait.alphacareprovider.Models;

import java.util.ArrayList;

public class ListResponse extends BaseResponse {
    private ArrayList<ListModel> data;

    public ArrayList<ListModel> getData() {
        return data;
    }

    public void setData(ArrayList<ListModel> data) {
        this.data = data;
    }
}
