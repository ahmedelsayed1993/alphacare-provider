package com.aait.alphacareprovider.Models;

import java.io.Serializable;

public class OrderModel implements Serializable {
    private int order_id;
    private int user_id;
    private String user_name;
    private String user_avatar;
    private String provider_name;
    private int provider_id;
    private String provider_avatar;
    private String order_status;
    private String order_total_price;
    private String order_date;
    private String order_time;
    private String order_lat;
    private String order_lng;
    private String order_latlng_address;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }

    public String getProvider_avatar() {
        return provider_avatar;
    }

    public void setProvider_avatar(String provider_avatar) {
        this.provider_avatar = provider_avatar;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_total_price() {
        return order_total_price;
    }

    public void setOrder_total_price(String order_total_price) {
        this.order_total_price = order_total_price;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getOrder_lat() {
        return order_lat;
    }

    public void setOrder_lat(String order_lat) {
        this.order_lat = order_lat;
    }

    public String getOrder_lng() {
        return order_lng;
    }

    public void setOrder_lng(String order_lng) {
        this.order_lng = order_lng;
    }

    public String getOrder_latlng_address() {
        return order_latlng_address;
    }

    public void setOrder_latlng_address(String order_latlng_address) {
        this.order_latlng_address = order_latlng_address;
    }
}
