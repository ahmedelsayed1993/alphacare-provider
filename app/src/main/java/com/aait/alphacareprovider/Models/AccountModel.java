package com.aait.alphacareprovider.Models;

import java.io.Serializable;

public class AccountModel implements Serializable {
    private String bank_name;
    private String account_number;
    private String iban_number;
    private Double total;

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getIban_number() {
        return iban_number;
    }

    public void setIban_number(String iban_number) {
        this.iban_number = iban_number;
    }
}
