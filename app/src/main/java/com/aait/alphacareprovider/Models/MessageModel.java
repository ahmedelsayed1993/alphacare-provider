package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class MessageModel implements Serializable {
    private int id;
    private String type;
    private String body_type;
    private String message_body;
    private int room_id;
    private int user_id;
    private String is_seen;
    private String is_sender;
    private String flagged;
    private String is_delete;
    private String created_at;
    private ArrayList<Integer> other_users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody_type() {
        return body_type;
    }

    public void setBody_type(String body_type) {
        this.body_type = body_type;
    }

    public String getMessage_body() {
        return message_body;
    }

    public void setMessage_body(String message_body) {
        this.message_body = message_body;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getIs_seen() {
        return is_seen;
    }

    public void setIs_seen(String is_seen) {
        this.is_seen = is_seen;
    }

    public String getIs_sender() {
        return is_sender;
    }

    public void setIs_sender(String is_sender) {
        this.is_sender = is_sender;
    }

    public String getFlagged() {
        return flagged;
    }

    public void setFlagged(String flagged) {
        this.flagged = flagged;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public ArrayList<Integer> getOther_users() {
        return other_users;
    }

    public void setOther_users(ArrayList<Integer> other_users) {
        this.other_users = other_users;
    }
}
