package com.aait.alphacareprovider.Models;

public class Singltone {


   public RegisterModel registerModel;

    private static Singltone singletonInstance=null;


    private Singltone(){
        registerModel =new RegisterModel();
    }
    public static Singltone getInstance()
    {
        if (singletonInstance == null)
            singletonInstance = new Singltone();

        return singletonInstance;
    }

    public static void setSingletonInstance(Singltone singletonInstance) {
        Singltone.singletonInstance = singletonInstance;
    }
}
