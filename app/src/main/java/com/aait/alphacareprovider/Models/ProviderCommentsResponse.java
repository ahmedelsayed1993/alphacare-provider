package com.aait.alphacareprovider.Models;

public class ProviderCommentsResponse extends BaseResponse{
    private ProviderCommentsModel data;

    public ProviderCommentsModel getData() {
        return data;
    }

    public void setData(ProviderCommentsModel data) {
        this.data = data;
    }
}
