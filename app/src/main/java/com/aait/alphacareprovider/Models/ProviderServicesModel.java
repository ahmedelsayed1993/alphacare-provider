package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProviderServicesModel implements Serializable {
    private ProviderModel provider;
    private ArrayList<ServiceModel> tab_data;

    public ProviderModel getProvider() {
        return provider;
    }

    public void setProvider(ProviderModel provider) {
        this.provider = provider;
    }

    public ArrayList<ServiceModel> getTab_data() {
        return tab_data;
    }

    public void setTab_data(ArrayList<ServiceModel> tab_data) {
        this.tab_data = tab_data;
    }
}
