package com.aait.alphacareprovider.Models;

import java.util.ArrayList;

public class AddressesResponse extends BaseResponse{
    private ArrayList<AddressesModel> data;

    public ArrayList<AddressesModel> getData() {
        return data;
    }

    public void setData(ArrayList<AddressesModel> data) {
        this.data = data;
    }
}
