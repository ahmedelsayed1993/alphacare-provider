package com.aait.alphacareprovider.Models;

import okhttp3.MultipartBody;

public class RegisterModel {

    int id;
    String  avatar="" ;
    String name = "";
    String phone = "";
    String email = "";
    String city_id = "";
    String city_name = "";
    int gender = 1;
    String section_id = "";
    String section_name = "";
    int provider_type = 1;
    String national_id = "";
    String national_copy ="";
    String practice_copy = "";
    String card_copy = "";
    String degree_copy = "";
    int experience = 1;
    String lisence_copy = "";
    String papers_pdf = "";
    String bank_iban = "";
    String iban_copy = "";
    String desc_ar = "";
    String desc_en = "";
    String password = "";
    String lat ="";
    String lng = "";
    String latlng_address_ar = "";
    String latlng_address_en = "";
    boolean technical = false;


    public String getCity_name() {
        return city_name;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public boolean isTechnical() {
        return technical;
    }

    public void setTechnical(boolean technical) {
        this.technical = technical;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public int getProvider_type() {
        return provider_type;
    }

    public void setProvider_type(int provider_type) {
        this.provider_type = provider_type;
    }

    public String getNational_id() {
        return national_id;
    }

    public void setNational_id(String national_id) {
        this.national_id = national_id;
    }

    public String getNational_copy() {
        return national_copy;
    }

    public void setNational_copy(String national_copy) {
        this.national_copy = national_copy;
    }

    public String getPractice_copy() {
        return practice_copy;
    }

    public void setPractice_copy(String practice_copy) {
        this.practice_copy = practice_copy;
    }

    public String getCard_copy() {
        return card_copy;
    }

    public void setCard_copy(String card_copy) {
        this.card_copy = card_copy;
    }

    public String getDegree_copy() {
        return degree_copy;
    }

    public void setDegree_copy(String degree_copy) {
        this.degree_copy = degree_copy;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getLisence_copy() {
        return lisence_copy;
    }

    public void setLisence_copy(String lisence_copy) {
        this.lisence_copy = lisence_copy;
    }

    public String getPapers_pdf() {
        return papers_pdf;
    }

    public void setPapers_pdf(String papers_pdf) {
        this.papers_pdf = papers_pdf;
    }

    public String getBank_iban() {
        return bank_iban;
    }

    public void setBank_iban(String bank_iban) {
        this.bank_iban = bank_iban;
    }

    public String getIban_copy() {
        return iban_copy;
    }

    public void setIban_copy(String iban_copy) {
        this.iban_copy = iban_copy;
    }

    public String getDesc_ar() {
        return desc_ar;
    }

    public void setDesc_ar(String desc_ar) {
        this.desc_ar = desc_ar;
    }

    public String getDesc_en() {
        return desc_en;
    }

    public void setDesc_en(String desc_en) {
        this.desc_en = desc_en;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLatlng_address_ar() {
        return latlng_address_ar;
    }

    public void setLatlng_address_ar(String latlng_address_ar) {
        this.latlng_address_ar = latlng_address_ar;
    }

    public String getLatlng_address_en() {
        return latlng_address_en;
    }

    public void setLatlng_address_en(String latlng_address_en) {
        this.latlng_address_en = latlng_address_en;
    }
}
