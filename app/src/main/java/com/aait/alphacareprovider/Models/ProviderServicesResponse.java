package com.aait.alphacareprovider.Models;

public class ProviderServicesResponse extends BaseResponse {
    private ProviderServicesModel data;

    public ProviderServicesModel getData() {
        return data;
    }

    public void setData(ProviderServicesModel data) {
        this.data = data;
    }
}
