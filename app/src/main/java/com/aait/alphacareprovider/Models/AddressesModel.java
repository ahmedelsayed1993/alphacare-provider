package com.aait.alphacareprovider.Models;

import java.io.Serializable;

public class AddressesModel implements Serializable {
    private int id;
    private int user_id;
    private String name;
    private String lat;
    private String lng;
    private String latlng_address_ar;
    private String latlng_address_en;
    private String latlng_address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLatlng_address_ar() {
        return latlng_address_ar;
    }

    public void setLatlng_address_ar(String latlng_address_ar) {
        this.latlng_address_ar = latlng_address_ar;
    }

    public String getLatlng_address_en() {
        return latlng_address_en;
    }

    public void setLatlng_address_en(String latlng_address_en) {
        this.latlng_address_en = latlng_address_en;
    }

    public String getLatlng_address() {
        return latlng_address;
    }

    public void setLatlng_address(String latlng_address) {
        this.latlng_address = latlng_address;
    }
}
