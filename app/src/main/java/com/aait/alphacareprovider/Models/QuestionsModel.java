package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class QuestionsModel implements Serializable {
    private ArrayList<QuestionModel> data;
    private PaginationModel pagination;

    public ArrayList<QuestionModel> getData() {
        return data;
    }

    public void setData(ArrayList<QuestionModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
