package com.aait.alphacareprovider.Models;

import java.util.ArrayList;

public class ReasonsResponse extends BaseResponse{
    private ArrayList<ReasonsModel> data;

    public ArrayList<ReasonsModel> getData() {
        return data;
    }

    public void setData(ArrayList<ReasonsModel> data) {
        this.data = data;
    }
}
