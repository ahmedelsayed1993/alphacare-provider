package com.aait.alphacareprovider.Models;

import java.util.ArrayList;

public class ListResonse extends BaseResponse {
    private ArrayList<ListModle> data;

    public ArrayList<ListModle> getData() {
        return data;
    }

    public void setData(ArrayList<ListModle> data) {
        this.data = data;
    }
}
