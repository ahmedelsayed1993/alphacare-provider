package com.aait.alphacareprovider.Models;

public class ServiceResponse extends BaseResponse{
    private ServicesModel data;

    public ServicesModel getData() {
        return data;
    }

    public void setData(ServicesModel data) {
        this.data = data;
    }
}
