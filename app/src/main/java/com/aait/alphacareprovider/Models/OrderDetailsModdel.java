package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDetailsModdel implements Serializable {
    private int order_id;
    private int room_id;
    private int user_id;
    private String user_name;
    private String user_count_rate;
    private String user_final_rate;
    private String user_online;
    private String user_avatar;
    private int provider_id;
    private String provider_name;
    private String provider_section_id;
    private String provider_section;
    private String provider_branch;
    private String provider_count_rate;
    private String provider_final_rate;
    private String provider_online;
    private String provider_avatar;
    private String provider_distance;
    private String order_status;
    private String order_refuse_reason;
    private String order_is_blood_prob;
    private String order_blood_prob_desc;
    private String order_total_price;
    private String order_date;
    private String order_time;
    private String order_lat;
    private String order_lng;
    private String order_latlng_address;
    private ArrayList<ServiceModel> normal_services;
    private ArrayList<ServiceModel> offer_services;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_count_rate() {
        return user_count_rate;
    }

    public void setUser_count_rate(String user_count_rate) {
        this.user_count_rate = user_count_rate;
    }

    public String getUser_final_rate() {
        return user_final_rate;
    }

    public void setUser_final_rate(String user_final_rate) {
        this.user_final_rate = user_final_rate;
    }

    public String getUser_online() {
        return user_online;
    }

    public void setUser_online(String user_online) {
        this.user_online = user_online;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getProvider_section_id() {
        return provider_section_id;
    }

    public void setProvider_section_id(String provider_section_id) {
        this.provider_section_id = provider_section_id;
    }

    public String getProvider_section() {
        return provider_section;
    }

    public void setProvider_section(String provider_section) {
        this.provider_section = provider_section;
    }

    public String getProvider_branch() {
        return provider_branch;
    }

    public void setProvider_branch(String provider_branch) {
        this.provider_branch = provider_branch;
    }

    public String getProvider_count_rate() {
        return provider_count_rate;
    }

    public void setProvider_count_rate(String provider_count_rate) {
        this.provider_count_rate = provider_count_rate;
    }

    public String getProvider_final_rate() {
        return provider_final_rate;
    }

    public void setProvider_final_rate(String provider_final_rate) {
        this.provider_final_rate = provider_final_rate;
    }

    public String getProvider_online() {
        return provider_online;
    }

    public void setProvider_online(String provider_online) {
        this.provider_online = provider_online;
    }

    public String getProvider_avatar() {
        return provider_avatar;
    }

    public void setProvider_avatar(String provider_avatar) {
        this.provider_avatar = provider_avatar;
    }

    public String getProvider_distance() {
        return provider_distance;
    }

    public void setProvider_distance(String provider_distance) {
        this.provider_distance = provider_distance;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_refuse_reason() {
        return order_refuse_reason;
    }

    public void setOrder_refuse_reason(String order_refuse_reason) {
        this.order_refuse_reason = order_refuse_reason;
    }

    public String getOrder_is_blood_prob() {
        return order_is_blood_prob;
    }

    public void setOrder_is_blood_prob(String order_is_blood_prob) {
        this.order_is_blood_prob = order_is_blood_prob;
    }

    public String getOrder_blood_prob_desc() {
        return order_blood_prob_desc;
    }

    public void setOrder_blood_prob_desc(String order_blood_prob_desc) {
        this.order_blood_prob_desc = order_blood_prob_desc;
    }

    public String getOrder_total_price() {
        return order_total_price;
    }

    public void setOrder_total_price(String order_total_price) {
        this.order_total_price = order_total_price;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getOrder_lat() {
        return order_lat;
    }

    public void setOrder_lat(String order_lat) {
        this.order_lat = order_lat;
    }

    public String getOrder_lng() {
        return order_lng;
    }

    public void setOrder_lng(String order_lng) {
        this.order_lng = order_lng;
    }

    public String getOrder_latlng_address() {
        return order_latlng_address;
    }

    public void setOrder_latlng_address(String order_latlng_address) {
        this.order_latlng_address = order_latlng_address;
    }

    public ArrayList<ServiceModel> getNormal_services() {
        return normal_services;
    }

    public void setNormal_services(ArrayList<ServiceModel> normal_services) {
        this.normal_services = normal_services;
    }

    public ArrayList<ServiceModel> getOffer_services() {
        return offer_services;
    }

    public void setOffer_services(ArrayList<ServiceModel> offer_services) {
        this.offer_services = offer_services;
    }
}
