package com.aait.alphacareprovider.Models;

public class CommissionResponse extends BaseResponse {
    private CommissionsModel data;

    public CommissionsModel getData() {
        return data;
    }

    public void setData(CommissionsModel data) {
        this.data = data;
    }
}
