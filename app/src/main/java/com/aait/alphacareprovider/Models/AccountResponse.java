package com.aait.alphacareprovider.Models;

public class AccountResponse extends BaseResponse {
    private AccountModel account;

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }
}
