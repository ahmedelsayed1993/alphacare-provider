package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class CommissionsModel implements Serializable {
    private UserCommission user_commissions;
    private double user_balance;
    private int site_commission;

    public UserCommission getUser_commissions() {
        return user_commissions;
    }

    public void setUser_commissions(UserCommission user_commissions) {
        this.user_commissions = user_commissions;
    }

    public double getUser_balance() {
        return user_balance;
    }

    public void setUser_balance(double user_balance) {
        this.user_balance = user_balance;
    }

    public int getSite_commission() {
        return site_commission;
    }

    public void setSite_commission(int site_commission) {
        this.site_commission = site_commission;
    }
}
