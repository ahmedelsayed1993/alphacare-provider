package com.aait.alphacareprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ChatModel implements Serializable {
    private ArrayList<MessageModel> data;
    private PaginationModel pagination;

    public ArrayList<MessageModel> getData() {
        return data;
    }

    public void setData(ArrayList<MessageModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
