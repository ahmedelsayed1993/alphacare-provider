package com.aait.alphacareprovider.Base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.Uitls.DialogUtil;

import butterknife.ButterKnife;


/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */
public abstract class BaseFragment extends Fragment {

    public SharedPrefManager mSharedPrefManager;

    protected LanguagePrefManager mLanguagePrefManager;

    protected Context mContext;

    public Bundle mSavedInstanceState;

    private ProgressDialog mProgressDialog;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        mContext = getActivity();
        mSharedPrefManager = new SharedPrefManager(mContext);
        mLanguagePrefManager = new LanguagePrefManager(mContext);
        this.mSavedInstanceState = savedInstanceState;
        ButterKnife.bind(this, view);

        initializeComponents(view);
        return view;
    }


    protected abstract int getLayoutResource();

    protected abstract void initializeComponents(View view);

    /**
     * it the current activity is a recycle
     */

    protected void showProgressDialog(String message) {
        mProgressDialog = DialogUtil.showProgressDialog(mContext, message, false);
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }



}
