package com.aait.alphacareprovider.FCM;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aait.alphacareprovider.Pereferences.LanguagePrefManager;
import com.aait.alphacareprovider.Pereferences.SharedPrefManager;
import com.aait.alphacareprovider.R;
//import com.aait.alphacareprovider.UI.Activities.MainActivity;
//import com.aait.alphacareprovider.UI.Activities.NotificationActivity;
import com.aait.alphacareprovider.UI.Activities.MainActivity;
import com.aait.alphacareprovider.UI.Activities.NotificationActivity;
import com.aait.alphacareprovider.UI.Activities.SplashActivity;
import com.aait.alphacareprovider.Uitls.CommonUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    SharedPreferences sharedPreferences;
    private static final String TAG = "FCM Messaging";

    String notificationTitle;

    String notificationMessage;

    String notificationData;

    SharedPrefManager mSharedPrefManager;

    String notificationType;

    // OrderModel mOrderModel;

    LanguagePrefManager mLanguagePrefManager;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sharedPreferences = getSharedPreferences("home", MODE_PRIVATE);
        CommonUtil.onPrintLog(remoteMessage.getNotification());
        CommonUtil.onPrintLog(remoteMessage.getData());
        mSharedPrefManager = new SharedPrefManager(getApplicationContext());
        mLanguagePrefManager = new LanguagePrefManager(getApplicationContext());

//        if (!mSharedPrefManager.getUserData().getNotification_status().equals("")) {
//            return;
//        }


//        notificationType = remoteMessage.getData().get("type");
        notificationTitle = remoteMessage.getData().get("notification_title");
        notificationMessage = remoteMessage.getData().get("notification_body");
        notificationType = remoteMessage.getData().get("notification_type");
        // if the notification contains data payload
        if (remoteMessage == null) {
            return;
        }

        // if the user not logged in never do any thing
        if (!mSharedPrefManager.getLoginStatus()) {
            return;
        } else {
            if (remoteMessage.getData().get("notification_type").equals("notification")){
                Intent intent = new Intent(this, NotificationActivity.class);
                intent.putExtra("notification", "notification");
                showNotification(remoteMessage, intent);
            }else  if(remoteMessage.getData().get("notification_type").equals("blocked")){
                mSharedPrefManager.Logout();
                mSharedPrefManager.setLoginStatus(false);
                startActivity(new Intent(getApplicationContext(), SplashActivity.class));

            }
            else  if(remoteMessage.getData().get("notification_type").equals("deleted")){
                mSharedPrefManager.Logout();
                mSharedPrefManager.setLoginStatus(false);
                startActivity(new Intent(getApplicationContext(),SplashActivity.class));

            }
            else if (remoteMessage.getData().get("notification_type").equals("chat")){
            if (sharedPreferences.getString("sender_id", "0").equals(remoteMessage.getData().get("room_id"))) {
                Intent intent = new Intent("new_message");
                intent.putExtra("msg", remoteMessage.getData().get("message_body"));
                Log.e("msg",remoteMessage.getData().get("message_body"));

                intent.putExtra("reciver_id", remoteMessage.getData().get("user_id"));

                intent.putExtra("sender_id", remoteMessage.getData().get("room_id"));

                intent.putExtra("time", remoteMessage.getData().get("created_at"));

                intent.putExtra("id", remoteMessage.getData().get("is_sender"));
                intent.putExtra("message",remoteMessage.getData().get("id"));

                intent.putExtra("type", remoteMessage.getData().get("type"));


                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            }else {

                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("notification", "notification");
                intent.putExtra("type","normal");
                showNotification(remoteMessage, intent);
            }
            }else {
                Intent intent = new Intent(this, NotificationActivity.class);
                intent.putExtra("notification", "notification");
                showNotification(remoteMessage, intent);
            }
            }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showNotification(RemoteMessage message, Intent intent) {

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getData().get("notification_title"))
                .setContentText(message.getData().get("notification_body"))
                .setTicker("Notification!")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setColor(getColor(R.color.colorAccent))
                .setSmallIcon(R.drawable.logo);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.DKGRAY);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            myNotification.setChannelId("10001");
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build());
    }

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
