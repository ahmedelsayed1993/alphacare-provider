package com.aait.alphacareprovider.Network;

import androidx.cardview.widget.CardView;

import com.aait.alphacareprovider.Models.AccountResponse;
import com.aait.alphacareprovider.Models.AddressesResponse;
import com.aait.alphacareprovider.Models.BaseResponse;
import com.aait.alphacareprovider.Models.ChatResponse;
import com.aait.alphacareprovider.Models.CommissionResponse;
import com.aait.alphacareprovider.Models.ListResonse;
import com.aait.alphacareprovider.Models.ListResponse;
import com.aait.alphacareprovider.Models.MessageResponse;
import com.aait.alphacareprovider.Models.MessagesResponse;
import com.aait.alphacareprovider.Models.NotificationCountRespons;
import com.aait.alphacareprovider.Models.NotificationResponse;
import com.aait.alphacareprovider.Models.OrderDetailsResponse;
import com.aait.alphacareprovider.Models.OrdersResponse;
import com.aait.alphacareprovider.Models.ProviderCommentsResponse;
import com.aait.alphacareprovider.Models.ProviderServicesResponse;
import com.aait.alphacareprovider.Models.QuestionsResponse;
import com.aait.alphacareprovider.Models.ReasonsResponse;
import com.aait.alphacareprovider.Models.ServiceResponse;
import com.aait.alphacareprovider.Models.SettlementResponse;
import com.aait.alphacareprovider.Models.SiteInfoResponse;
import com.aait.alphacareprovider.Models.SiteReportsModel;
import com.aait.alphacareprovider.Models.SiteReportsResponse;
import com.aait.alphacareprovider.Models.UserModel;
import com.aait.alphacareprovider.Models.UserResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ServiceApi {

    @POST(Urls.Cities)
    Call<ListResponse> getCities(@Query("lang") String lang);

    @POST(Urls.Sections)
    Call<ListResponse> getSections(@Query("lang") String lang);

    @Multipart
    @POST(Urls.SignUp)
    Call<UserResponse> registerTechnical(@Query("lang") String lang,
                                         @Part MultipartBody.Part avatar,
                                         @Query("name") String name,
                                         @Query("phone") String phone,
                                         @Query("email") String email,
                                         @Query("city_id") String city_id,
                                         @Query("gender") int gender,
                                         @Query("section_id") String section_id,
                                         @Query("provider_type") int provider_type,
                                         @Query("national_id") String national_id,
                                         @Part MultipartBody.Part national_copy,
                                         @Query("lat") String lat,
                                         @Query("lng") String lng,
                                         @Query("latlng_address_ar") String latlng_address_ar,
                                         @Query("latlng_address_en") String latlng_address_en,
                                         @Part MultipartBody.Part practice_copy,
                                         @Part MultipartBody.Part card_copy,
                                         @Part MultipartBody.Part degree_copy,
                                         @Query("experience") int experience,
                                         @Query("desc_ar") String desc_ar,
                                         @Query("desc_en") String desc_en,
                                         @Query("password") String password,
                                         @Query("app_type") int app_type,
                                         @Query("device_id") String device_id,
                                         @Query("device_type") String device_type);

    @Multipart
    @POST(Urls.SignUp)
    Call<UserResponse> registerLab(@Query("lang") String lang,
                                   @Part MultipartBody.Part avatar,
                                   @Query("name") String name,
                                   @Query("phone") String phone,
                                   @Query("email") String email,
                                   @Query("city_id") String city_id,
                                   @Query("gender") int gender,
                                   @Query("section_id") String section_id,
                                   @Query("provider_type") int provider_type,
                                   @Query("national_id") String national_id,
                                   @Part MultipartBody.Part national_copy,
                                   @Part MultipartBody.Part lisence_copy,
                                   @Part MultipartBody.Part papers_pdf,
                                   @Query("bank_iban") String bank_iban,
                                   @Part MultipartBody.Part iban_copy,
                                   @Query("desc_ar") String desc_ar,
                                   @Query("desc_en") String desc_en,
                                   @Query("password") String password,
                                   @Query("app_type") int app_type,
                                   @Query("device_id") String device_id,
                                   @Query("device_type") String device_type);
    @Multipart
    @POST(Urls.SignUp)
    Call<UserResponse> registerLabwithiban(@Query("lang") String lang,
                                   @Part MultipartBody.Part avatar,
                                   @Query("name") String name,
                                   @Query("phone") String phone,
                                   @Query("email") String email,
                                   @Query("city_id") String city_id,
                                   @Query("gender") int gender,
                                   @Query("section_id") String section_id,
                                   @Query("provider_type") int provider_type,
                                   @Query("national_id") String national_id,

                                   @Part MultipartBody.Part lisence_copy,
                                   @Part MultipartBody.Part papers_pdf,
                                   @Query("bank_iban") String bank_iban,
                                   @Part MultipartBody.Part iban_copy,
                                   @Query("desc_ar") String desc_ar,
                                   @Query("desc_en") String desc_en,
                                   @Query("password") String password,
                                   @Query("app_type") int app_type,
                                   @Query("device_id") String device_id,
                                   @Query("device_type") String device_type);
    @Multipart
    @POST(Urls.SignUp)
    Call<UserResponse> registerLabwithcopy(@Query("lang") String lang,
                                   @Part MultipartBody.Part avatar,
                                   @Query("name") String name,
                                   @Query("phone") String phone,
                                   @Query("email") String email,
                                   @Query("city_id") String city_id,
                                   @Query("gender") int gender,
                                   @Query("section_id") String section_id,
                                   @Query("provider_type") int provider_type,
                                   @Query("national_id") String national_id,
                                   @Part MultipartBody.Part national_copy,
                                   @Part MultipartBody.Part lisence_copy,
                                   @Part MultipartBody.Part papers_pdf,
                                   @Query("bank_iban") String bank_iban,

                                   @Query("desc_ar") String desc_ar,
                                   @Query("desc_en") String desc_en,
                                   @Query("password") String password,
                                   @Query("app_type") int app_type,
                                   @Query("device_id") String device_id,
                                   @Query("device_type") String device_type);
    @Multipart
    @POST(Urls.SignUp)
    Call<UserResponse> registerLabwitout(@Query("lang") String lang,
                                   @Part MultipartBody.Part avatar,
                                   @Query("name") String name,
                                   @Query("phone") String phone,
                                   @Query("email") String email,
                                   @Query("city_id") String city_id,
                                   @Query("gender") int gender,
                                   @Query("section_id") String section_id,
                                   @Query("provider_type") int provider_type,
                                   @Query("national_id") String national_id,
                                   @Part MultipartBody.Part lisence_copy,
                                   @Part MultipartBody.Part papers_pdf,
                                         @Query("bank_iban") String bank_iban,
                                   @Query("desc_ar") String desc_ar,
                                   @Query("desc_en") String desc_en,
                                   @Query("password") String password,
                                   @Query("app_type") int app_type,
                                   @Query("device_id") String device_id,
                                   @Query("device_type") String device_type);
    @POST(Urls.SiteReports)
    Call<SiteReportsResponse> getReports(@Query("lang") String lang);
    @POST(Urls.Activate)
    Call<UserResponse> activate(@Query("lang") String lang,
                                @Query("user_id") int user_id,
                                @Query("code") String code);
    @POST(Urls.Resend)
    Call<UserResponse> resend(@Query("lang") String lang,
                              @Query("user_id") int user_id);
    @POST(Urls.LogOut)
    Call<BaseResponse> LogOut(@Query("app_type") int app_type,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id);
    @POST(Urls.SignIn)
    Call<UserResponse> Login(@Query("lang") String lang,
                             @Query("phoneOrEmail") String phoneOrEmail,
                             @Query("password") String password,
                             @Query("app_type") int app_type,
                             @Query("device_id") String device_id,
                             @Query("device_type") String device_type);
    @POST(Urls.FprgotPassword)
    Call<UserResponse> forgotPass(@Query("lang") String lang,
                                  @Query("phone") String phone);
    @POST(Urls.NewPassword)
    Call<UserResponse> newPass(@Query("lang") String lang,
                               @Query("code") String code,
                               @Query("new_password") String new_password,
                               @Query("phone") String phone);
    @POST(Urls.ChangeLang)
    Call<BaseResponse> changeLang(@Query("app_type") int app_type,
                                  @Query("token_id") String token_id,
                                  @Query("device_id") String device_id,
                                  @Query("lang") String lang);

    @POST(Urls.SiteInfo)
    Call<SiteInfoResponse> getInfo(@Query("lang") String lang);

    @POST(Urls.ContactTypes)
    Call<ListResonse> getTypes(@Query("lang") String lang);
    @POST(Urls.SendMessage)
    Call<BaseResponse> sendMessage(@Query("lang") String lang,
                                   @Query("app_type") int app_type,
                                   @Query("token_id") String token_id,
                                   @Query("device_id") String device_id,
                                   @Query("name") String name,
                                   @Query("phone") String phone,
                                   @Query("type") int type,
                                   @Query("message") String message);

    @POST(Urls.MostQuestions)
    Call<QuestionsResponse> getQuestions(@Query("lang") String lang,
                                         @Query("page") int page);
    @POST(Urls.Status)
    Call<UserResponse> status(@Query("app_type") int app_type,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id,
                              @Query("status") int status,
                              @Query("lang") String lang);
    @POST(Urls.NotificationCount)
    Call<NotificationCountRespons> getCount(@Query("lang") String lang,
                                            @Query("app_type") int app_type,
                                            @Query("token_id") String token_id,
                                            @Query("device_id") String device_id);
    @POST(Urls.Addresses)
    Call<AddressesResponse> getAddress(@Query("lang") String lang,
                                       @Query("token_id") String token_id,
                                       @Query("device_id") String device_id,
                                       @Query("app_type") int app_type);

    @POST(Urls.AddAddress)
    Call<AddressesResponse> addAddrress(@Query("lang") String lang,
                                        @Query("app_type") int app_type,
                                        @Query("token_id") String token_id,
                                        @Query("device_id") String device_id,
                                        @Query("name") String name,
                                        @Query("lat") String lat,
                                        @Query("lng") String lng,
                                        @Query("latlng_address_ar") String latlng_address_ar,
                                        @Query("latlng_address_en") String latlng_address_en);

    @POST(Urls.EditAddress)
    Call<AddressesResponse> editAddrress(@Query("lang") String lang,
                                         @Query("app_type") int app_type,
                                         @Query("token_id") String token_id,
                                         @Query("device_id") String device_id,
                                         @Query("name") String name,
                                         @Query("lat") String lat,
                                         @Query("lng") String lng,
                                         @Query("address_id") int address_id,
                                         @Query("latlng_address_ar") String latlng_address_ar,
                                         @Query("latlng_address_en") String latlng_address_en);
    @POST(Urls.DeleteAddress)
    Call<AddressesResponse> deleteAddress(@Query("lang") String lang,
                                          @Query("app_type") int app_type,
                                          @Query("token_id") String token_id,
                                          @Query("device_id") String device_id,
                                          @Query("address_id") int address_id);
    @POST(Urls.Notification)
    Call<NotificationResponse> getNotification(@Query("lang") String lang,
                                               @Query("app_type") int app_type,
                                               @Query("token_id") String token_id,
                                               @Query("device_id") String device_id,
                                               @Query("page") int page);
    @POST(Urls.Delete)
    Call<BaseResponse> remove(@Query("app_type") int app_type,
                              @Query("lang") String lang,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id,
                              @Query("notification_id") int notification_id);

    @POST(Urls.Rooms)
    Call<MessagesResponse> getMessages(@Query("app_type") int app_type,
                                       @Query("lang") String lang,
                                       @Query("token_id") String token_id,
                                       @Query("device_id") String device_id,
                                       @Query("page") int page);

    @POST(Urls.ShowProvider)
    Call<ProviderServicesResponse> getProvider(@Query("app_type") int app_type,
                                               @Query("token_id") String token_id,
                                               @Query("device_id") String device_id,
                                               @Query("tab_num") String tab_num,
                                               @Query("lang") String lang);
    @POST(Urls.ServiceDetails)
    Call<ServiceResponse> getService(@Query("lang") String lang,
                                     @Query("product_id") String product_id);
    @POST(Urls.ShowProvider)
    Call<ProviderCommentsResponse> getComments(@Query("app_type") int app_type,
                                               @Query("token_id") String token_id,
                                               @Query("device_id") String device_id,
                                               @Query("tab_num") String tab_num,
                                               @Query("lang") String lang);
    @POST(Urls.Profile)
    Call<UserResponse> getProfile(@Query("lang") String lang,
                                  @Query("token_id") String token_id,
                                  @Query("device_id") String device_id,
                                  @Query("app_type") int app_type);

    @Multipart
    @POST(Urls.AddService)
    Call<BaseResponse> addService(@Query("app_type") int app_type,
                                  @Query("token_id") String token,
                                  @Query("device_id") String device_id,
                                  @Query("title_ar") String title_ar,
                                  @Query("title_en") String title_en,
                                  @Query("price") String price,
                                  @Part MultipartBody.Part image,
                                  @Query("desc_ar") String desc_ar,
                                  @Query("desc_en") String desc_en,
                                  @Query("lang") String lang,
                                  @Query("offer") int offer,
                                  @Query("adv_ar") String adv_ar,
                                  @Query("adv_en") String adv_en);

    @POST(Urls.NewOrders)
    Call<OrdersResponse> getNew(@Query("lang") String lang,
                                @Query("app_type") int app_type,
                                @Query("token_id") String token_id,
                                @Query("device_id") String device_id,
                                @Query("page") int page);
    @POST(Urls.CurrentOrders)
    Call<OrdersResponse> getCurrent(@Query("lang") String lang,
                                    @Query("app_type") int app_type,
                                    @Query("token_id") String token_id,
                                    @Query("device_id") String device_id,
                                    @Query("page") int page);
    @POST(Urls.FinishedOrders)
    Call<OrdersResponse> getFinished(@Query("lang") String lang,
                                     @Query("app_type") int app_type,
                                     @Query("token_id") String token_id,
                                     @Query("device_id") String device_id,
                                     @Query("page") int page);
    @POST(Urls.OrderDetails)
    Call<OrderDetailsResponse> getOrder(@Query("lang") String lang,
                                        @Query("app_type") int app_type,
                                        @Query("token_id") String token_id,
                                        @Query("device_id") String device_id,
                                        @Query("order_id") String order_id);
    @POST(Urls.AnswerOrder)
    Call<BaseResponse> answerOrder(@Query("lang") String lang,
                                   @Query("app_type") int app_type,
                                   @Query("token_id") String token_id,
                                   @Query("device_id") String device_id,
                                   @Query("order_id") String order_id,
                                   @Query("status") int status,
                                   @Query("refuse_reason") String refuse_reason);
    @POST(Urls.FinishOrder)
    Call<BaseResponse> finishOrder(@Query("lang") String lang,
                                   @Query("app_type") int app_type,
                                   @Query("token_id") String token_id,
                                   @Query("device_id") String device_id,
                                   @Query("order_id") String order_id);
    @POST(Urls.Rate)
    Call<BaseResponse> rate(@Query("lang") String lang,
                            @Query("app_type") int app_type,
                            @Query("token_id") String token_id,
                            @Query("device_id") String device_id,
                            @Query("rate") int rate,
                            @Query("comment") String comment,
                            @Query("rated_id") int rated_id,
                            @Query("order_id") String order_id);

    @POST(Urls.Send)
    Call<MessageResponse> Send(@Query("app_type") int app_type,
                               @Query("token_id") String token_id,
                               @Query("device_id") String device_id,
                               @Query("room_id") String room_id,
                               @Query("message") String message);

    @POST(Urls.Chat)
    Call<ChatResponse> getChat(@Query("app_type") int app_type,
                               @Query("token_id") String token_id,
                               @Query("device_id") String device_id,
                               @Query("room_id") String room_id,
                               @Query("lang") String lang,
                               @Query("page") int page);

    @Multipart
    @POST(Urls.EditProduct)
    Call<ServiceResponse> editServicewithImageAr(@Query("lang") String lang,
                                                 @Query("app_type") int app_type,
                                                 @Query("token_id") String token_id,
                                                 @Query("device_id") String device_id,
                                                 @Query("product_id") String product_id,
                                                 @Query("title_ar") String title_ar,
                                                 @Query("desc_ar") String desc_ar,
                                                 @Query("price") String price,
                                                 @Query("adv_ar") String adv_ar,
                                                 @Part MultipartBody.Part image);
    @Multipart
    @POST(Urls.EditProduct)
    Call<ServiceResponse> editServicewithImageEn(@Query("lang") String lang,
                                                 @Query("app_type") int app_type,
                                                 @Query("token_id") String token_id,
                                                 @Query("device_id") String device_id,
                                                 @Query("product_id") String product_id,
                                                 @Query("title_en") String title_ar,
                                                 @Query("desc_en") String desc_ar,
                                                 @Query("price") String price,
                                                 @Query("adv_en") String adv_ar,
                                                 @Part MultipartBody.Part image);


    @POST(Urls.EditProduct)
    Call<ServiceResponse> editServicewithoutImageAr(@Query("lang") String lang,
                                                 @Query("app_type") int app_type,
                                                 @Query("token_id") String token_id,
                                                 @Query("device_id") String device_id,
                                                 @Query("product_id") String product_id,
                                                 @Query("title_ar") String title_ar,
                                                 @Query("desc_ar") String desc_ar,
                                                 @Query("price") String price,
                                                 @Query("adv_ar") String adv_ar);

    @POST(Urls.EditProduct)
    Call<ServiceResponse> editServicewithoutImageEn(@Query("lang") String lang,
                                                 @Query("app_type") int app_type,
                                                 @Query("token_id") String token_id,
                                                 @Query("device_id") String device_id,
                                                 @Query("product_id") String product_id,
                                                 @Query("title_ar") String title_ar,
                                                 @Query("desc_ar") String desc_ar,
                                                 @Query("price") String price,
                                                 @Query("adv_ar") String adv_ar);
    @POST(Urls.FlagComment)
    Call<BaseResponse> report(@Query("lang") String lang,
                              @Query("app_type") int app_type,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id,
                              @Query("comment_id") int comment_id,
                              @Query("flage_reason") String flage_reason);
    @POST(Urls.DeleteProduct)
    Call<BaseResponse> deleteProduct(@Query("app_type") int app_type,
                                     @Query("token_id") String token_id,
                                     @Query("device_id") String device_id,
                                     @Query("product_id") int product_id,
                                     @Query("lang") String lang);

    @Multipart
    @POST(Urls.UpdateProfile)
    Call<UserResponse> update(@Query("lang") String lang,
                              @Query("app_type") int app_type,
                              @Query("token_id") String token_id,
                              @Query("device_id") String device_id,
                              @Part MultipartBody.Part avatar);

    @POST(Urls.UpdateProfile)
    Call<UserResponse> updateWithoutPassAvatar(@Query("lang") String lang,
                                               @Query("app_type") int app_type,
                                               @Query("token_id") String token_id,
                                               @Query("device_id") String device_id,
                                               @Query("name") String name,
                                               @Query("phone") String phone,
                                               @Query("email") String email,
                                               @Query("gender") int gender,
                                               @Query("national_id") String national_id);

    @Multipart
    @POST(Urls.UpdateProfile)
    Call<UserResponse> updateProfessional_lab(@Query("lang") String lang,
                                          @Query("app_type") int app_type,
                                          @Query("token_id") String token_id,
                                          @Query("device_id") String device_id,
                                          @Query("name") String name,
                                          @Query("phone") String phone,
                                          @Query("email") String email,
                                          @Query("gender") int gender,
                                          @Query("national_id") String national_id,
                                          @Query("section_id") String section_id,
                                          @Query("desc_ar") String desc_ar,
                                          @Query("desc_en") String desc_en,
                                          @Part MultipartBody.Part lisence_copy);
    @Multipart
    @POST(Urls.UpdateProfile)
    Call<UserResponse> updateProfessional_pro(@Query("lang") String lang,
                                              @Query("app_type") int app_type,
                                              @Query("token_id") String token_id,
                                              @Query("device_id") String device_id,
                                              @Query("name") String name,
                                              @Query("phone") String phone,
                                              @Query("email") String email,
                                              @Query("gender") int gender,
                                              @Query("national_id") String national_id,
                                              @Query("section_id") String section_id,
                                              @Query("desc_ar") String desc_ar,
                                              @Query("desc_en") String desc_en,
                                              @Part MultipartBody.Part practice_copy);
    @Multipart
    @POST(Urls.UpdateProfile)
    Call<UserResponse> updateProfessional_lab_with(@Query("lang") String lang,
                                              @Query("app_type") int app_type,
                                              @Query("token_id") String token_id,
                                              @Query("device_id") String device_id,
                                              @Query("name") String name,
                                              @Query("phone") String phone,
                                              @Query("email") String email,
                                              @Query("gender") int gender,
                                              @Query("national_id") String national_id,
                                              @Query("section_id") String section_id,
                                              @Query("desc_ar") String desc_ar,
                                              @Query("desc_en") String desc_en,
                                              @Part MultipartBody.Part lisence_copy,
                                              @Query("old_password") String old_password,
                                              @Query("password") String password);
    @Multipart
    @POST(Urls.UpdateProfile)
    Call<UserResponse> updateProfessional_pro_with(@Query("lang") String lang,
                                              @Query("app_type") int app_type,
                                              @Query("token_id") String token_id,
                                              @Query("device_id") String device_id,
                                              @Query("name") String name,
                                              @Query("phone") String phone,
                                              @Query("email") String email,
                                              @Query("gender") int gender,
                                              @Query("national_id") String national_id,
                                              @Query("section_id") String section_id,
                                              @Query("desc_ar") String desc_ar,
                                              @Query("desc_en") String desc_en,
                                              @Part MultipartBody.Part practice_copy,
                                              @Query("old_password") String old_password,
                                              @Query("password") String password);
    @POST(Urls.UpdateProfile)
    Call<UserResponse> update_with(@Query("lang") String lang,
                                                   @Query("app_type") int app_type,
                                                   @Query("token_id") String token_id,
                                                   @Query("device_id") String device_id,
                                                   @Query("name") String name,
                                                   @Query("phone") String phone,
                                                   @Query("email") String email,
                                                   @Query("gender") int gender,
                                                   @Query("national_id") String national_id,
                                                   @Query("old_password") String old_password,
                                                   @Query("password") String password);
    @POST(Urls.Reasons)
    Call<ReasonsResponse> getReasons(@Query("lang") String lang);

    @POST(Urls.ProviderCommission)
    Call<CommissionResponse> getCommission(@Query("lang") String lang,
                                           @Query("app_type") int app_type,
                                           @Query("token_id") String token_id,
                                           @Query("device_id") String device_id,
                                           @Query("page") int page);
    @Multipart
    @POST(Urls.Transfer)
    Call<BaseResponse> Transfer(@Query("lang") String lang,
                                @Query("app_type") int app_type,
                                @Query("token_id") String token_id,
                                @Query("device_id") String device_id,
                                @Query("user_name") String user_name,
                                @Query("bank_num") String bank_num,
                                @Query("amount") String amount,
                                @Part MultipartBody.Part image);

    @POST(Urls.Transfer)
    Call<AccountResponse> Accounts(@Query("lang") String lang,
                                   @Query("app_type") int app_type,
                                   @Query("token_id") String token_id,
                                   @Query("device_id") String device_id);
    @POST(Urls.Pay)
    Call<SettlementResponse> pay(@Query("lang") String lang,
                                 @Query("app_type") int app_type,
                                 @Query("token_id") String token_id,
                                 @Query("device_id") String device_id,
                                 @Query("commissions_ids") String commissions_ids);

    @POST(Urls.SeeMessage)
    Call<BaseResponse> see(@Query("app_type") int app_type,
                           @Query("token_id") String token_id,
                           @Query("device_id") String device_id,
                           @Query("message_id") String message_id,
                           @Query("lang") String lang);
    @POST(Urls.Active)
    Call<UserResponse> active(@Query("app_type") int app_type,
                           @Query("token_id") String token_id,
                           @Query("device_id") String device_id,
                           @Query("status") String message_id,
                           @Query("lang") String lang);

}

