package com.aait.alphacareprovider.Network;

public class Urls {
    public final static String baseUrl = "https://alphacare.aait-sa.com/api/";

    public final static String SignUp = "provider/sign-up";
    public final static String Activate = "active_code";
    public final static String Resend = "resend_code";
    public final static String LogOut = "logout";
    public final static String SignIn = "sign-in";
    public final static String FprgotPassword = "forget-password";
    public final static String NewPassword = "forget-password/update";
    public final static String ChangeLang = "change_lang";
    public final static String SiteInfo = "site-data";
    public final static String ContactTypes = "contact/types";
    public final static String SendMessage = "contact/send";
    public final static String MostQuestions = "most-ask";
    public final static String NotificationCount = "unread_notifications";
    public final static String Addresses = "user_addresses";
    public final static String AddAddress = "user_add/address";
    public final static String EditAddress = "user_edit/address";
    public final static String DeleteAddress = "user_delete/address";
    public final static String Profile = "profile";
    public final static String UpdateProfile = "profile/update";
    public final static String MainPage = "main-page";
    public final static String ShowProvider = "provider/show-provider";
    public final static String ToogleFav = "toogle-fav";
    public final static String Sections = "sections";
    public final static String Fav = "get-fav";
    public final static String ServiceDetails = "show-product";
    public final static String Rate = "rate";
    public final static String AddOrder = "new-order";
    public final static String NewOrders = "new-orders";
    public final static String CancelOrder = "cancel-order";
    public final static String CurrentOrders = "current-orders";
    public final static String FinishedOrders = "finished-orders";
    public final static String Notification = "notifications";
    public final static String OrderDetails = "show-order";
    public final static String EditOrder = "edit-order";
    public final static String Send = "send-message";
    public final static String Chat = "room-messages";
    public final static String Status = "notification_status";
    public final static String Delete = "delete-notification";
    public final static String Cities = "cities";
    public final static String SiteReports = "site-reports";
    public final static String Rooms = "my-rooms";
    public final static String AddService = "provider/add-product";
    public final static String AnswerOrder = "provider/answer-order";
    public final static String FinishOrder = "provider/finish-order";
    public final static String EditProduct = "provider/edit-product";
    public final static String FlagComment = "provider/flage-comment";
    public final static String DeleteProduct = "provider/delete-product";
    public final static String Reasons = "refuse-reasons";
    public final static String ProviderCommission = "provider/commissions";
    public final static String Transfer = "send-transfer";
    public final static String Pay = "provider/pay-commissions";
    public final static String SeeMessage = "see-message";
    public final static String Active = "switch-active";


}
